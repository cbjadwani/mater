#!/usr/bin/env python
"""Simple socket server that forwards the incoming data to local API server
and forwards the response.
"""
from gevent.server import StreamServer
import logging
import urllib
import os


logformat = '[%(asctime)s] %(name)s:%(levelname)s: %(message)s'
logging.basicConfig(level='DEBUG', format=logformat)
log = logging.getLogger('Socket Proxy')


MATER_API_URL = os.getenv('MATER_API_URL', default='http://127.0.0.1:8000/')
if not MATER_API_URL.endswith('/'):
    MATER_API_URL += '/'
MATER_SOCKET_HOST = os.getenv('MATER_SOCKET_HOST', '0.0.0.0')
MATER_SOCKET_PORT = os.getenv('MATER_SOCKET_PORT', '80')
try:
    MATER_SOCKET_PORT = int(MATER_SOCKET_PORT)
except Exception:
    MATER_SOCKET_PORT = 80


# this handler will be run for each incoming connection in a dedicated greenlet
def apiforwarder(socket, address):
    addr_str = '%s:%s' % address
    log.info('New connection from %s' % addr_str)
    # using a makefile because we want to use readline()
    fileobj = socket.makefile()
    try:
        while True:
            line = fileobj.readline()
            if not line:
                break
            if not line.strip():
                continue
            log.info("recv %s: %r" % (addr_str, line))
            response = forward_request(line)
            fileobj.write(response)
            fileobj.flush()
            log.info("send %s: %r" % (addr_str, response))
        log.info('Client disconnected %s' % addr_str)
    except Exception as e:
        log.error('Exception (%s): %s' % (addr_str, e))
    finally:
        fileobj.close()


def forward_request(data):
    data = data.strip()
    qs = urllib.quote(data)
    url = '%s?q=%s' % (MATER_API_URL, qs)
    log.info('Forwarding to: %s' % url)

    f = urllib.urlopen(url)
    try:
        response = f.read()
    except Exception as e:
        log.error('Django Error: %s') % e
        return ':;'

    response = response.rstrip() + '\r\n'
    return response


if __name__ == '__main__':
    # to make the server use SSL, pass certfile
    # and keyfile arguments to the constructor
    server = StreamServer((MATER_SOCKET_HOST, MATER_SOCKET_PORT), apiforwarder)
    # to start the server asynchronously, use its start() method;
    # we use blocking serve_forever() here because we have no other jobs
    msg = 'Starting apiforwarder server on %s:%s'
    log.info(msg % (MATER_SOCKET_HOST, MATER_SOCKET_PORT))
    server.serve_forever()
