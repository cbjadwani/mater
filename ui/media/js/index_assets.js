function AssetsCtrl($scope, $timeout, $http) {
  $scope.map_maximized = false;
  $scope.user_assets = {};
  $scope.selected_pk = null;
  $scope.latest_trip_stats = {
    from: '',
    to: '',
    distance: '0',
    curr_speed: '?',
    avg_speed: '?',
    month_km: '0'
  };
  $scope.pane = 'livemap';
  $scope.gaugeWidget;

  function getAssetHeading(asset) {
    var logs, lat_diff, lng_diff, angle = 0;

    if (asset.track && asset.track.length >= 2) {
      logs = asset.track.slice(-2);
      lat_diff = logs[1][0] - logs[0][0];
      lng_diff = logs[1][1] - logs[0][1];
      angle = Math.atan2(lng_diff, lat_diff) * 180 / Math.PI;
    }
    return angle;
  };
  function getAssetMarkerCaption(asset) {
    var status_;
    if (asset.is_trip_data) {
      if (asset.loaded) {
        status_ = 'Decanted at ' + asset.end_place;
      } else {
        status_ = 'Loaded at ' + asset.end_place;
      }
      status_ += '<br>' + moment.unix(asset.end_ts).format('DD/MM/YY hh:mmA');
      return status_
    }
    status_ = asset.status == 'On Road' ? (asset.curr_speed + ' km/h') : asset.status;
    if (asset.status === 'On Road') {
      status_ = asset.curr_speed + ' km/h';
    } else if (asset.curr_place) {
      status_ = asset.status + ' at ' + asset.curr_place;
    } else {
      status_ = asset.status;
    }
    return '<b>' + asset.fields.name + '</b><br>(' + status_ + ', ' + (asset.loaded ? 'Loaded' : 'Unloaded') + ')';
  };

  $scope.init = function (user_assets, selected_pk) {
    var selected_pk, asset, rotation, caption;
    $scope.user_assets = user_assets;
    for (var pk in $scope.user_assets) {
      asset = $scope.user_assets[pk];
      asset.pk = pk;
      rotation = getAssetHeading(asset);
      caption = getAssetMarkerCaption(asset);
      mater.livemap.add_marker(pk, asset.y, asset.x, caption, rotation);
    }
    if (!(selected_pk in $scope.user_assets)) {
      selected_pk = Object.keys($scope.user_assets)[0];
    }
    if (selected_pk) {
      $scope.selected_pk = selected_pk;
      mater.livemap.show_tracks(user_assets[selected_pk].track, user_assets[selected_pk].loaded);
    }
  };
  $scope.get_current_asset = function () {
    if ($scope.selected_pk) {
      return $scope.user_assets[$scope.selected_pk];
    }
    return null;
  };
  $scope.setMapCenterAndExtent = function () {
    var curr_asset = $scope.get_trip_asset() || {},
        extent = {}, mapTypeId;
    if (curr_asset && map) {
      if(['Stopped', 'Parked'].indexOf(curr_asset.status) >= 0 &&
          !curr_asset.is_trip_data) {
        mapTypeId = google.maps.MapTypeId.HYBRID;
        extent.zoom = 18;
        extent.center = [curr_asset.y, curr_asset.x];
      } else if (curr_asset.status === 'On Road' || curr_asset.is_trip_data) {
        mapTypeId = google.maps.MapTypeId.ROADMAP;
        var minLat, maxLat, minLng, maxLng;
        if (curr_asset.track) {
          for (var i=0; i < curr_asset.track.length; i++) {
            if (typeof minLat === 'undefined' || curr_asset.track[i][0] < minLat) {
              minLat = curr_asset.track[i][0];
            }
            if (typeof maxLat === 'undefined' || curr_asset.track[i][0] > maxLat) {
              maxLat = curr_asset.track[i][0];
            }
            if (typeof minLng === 'undefined' || curr_asset.track[i][1] < minLng) {
              minLng = curr_asset.track[i][1];
            }
            if (typeof maxLng === 'undefined' || curr_asset.track[i][1] > maxLng) {
              maxLng = curr_asset.track[i][1];
            }
          }
          if (typeof minLat != 'undefined' && typeof maxLat != 'undefined') {
            var halfWay = minLat + (maxLat - minLat) / 2;
            if (curr_asset.y > halfWay) {
              maxLat = curr_asset.y + (curr_asset.y - minLat);
            } else {
              minLat = curr_asset.y - (maxLat - curr_asset.y);
            }
          }
          if (typeof minLng != 'undefined' && typeof maxLng != 'undefined') {
            var halfWay = minLng + (maxLng - minLng) / 2;
            if (curr_asset.x > halfWay) {
              maxLng = curr_asset.x + (curr_asset.x - minLng);
            } else {
              minLng = curr_asset.x - (maxLng - curr_asset.x);
            }
          }
          extent.bounds = {
            minLat: minLat,
            maxLat: maxLat,
            minLng: minLng,
            maxLng: maxLng
          };
        }
      } else {
        extent.center = [curr_asset.y, curr_asset.x];
      }
      mater.livemap.setMapCenterAndExtent(extent, mapTypeId);
    }
  };
  $scope.$watch('selected_pk', function (new_pk, old_pk, $scope) {
    if (new_pk !== old_pk) {
      mater.livemap.hide_marker(old_pk);
    }
    mater.livemap.clear_violations();
    $scope.latest_trip_stats.curr_trip = undefined;
    $scope.latest_trip_stats.curr_loaded_state = undefined;
    $scope.update_trip_stats();
    $scope.update_last_time();
    $scope.setMapCenterAndExtent();
    mater.livemap.show_marker(new_pk);

    // Update header dropdown
    $('#assets-submenu a i').removeClass('icon-ok').addClass('icon-spacer');
    $('#assets-submenu a[data-pk="' + new_pk + '"] i').removeClass('icon-spacer').addClass('icon-ok');
  });
  $scope.select_asset = function (selected_pk) {
    if (selected_pk in $scope.user_assets) {
      $scope.selected_pk = selected_pk;
      $.ajax(mater.urls.select_asset(selected_pk));
    }
  };
  $scope.showingPastTrip = function () {
    var asset = $scope.get_current_asset(),
        stats = $scope.latest_trip_stats;
    return asset && (stats.curr_trip != asset.num_trips || stats.curr_loaded_state != asset.loaded);
  };
  $scope.get_trip_asset = function () {
    var asset = $scope.get_current_asset(),
      stats = $scope.latest_trip_stats,
      trip_data,
      new_asset;
    if (asset &&
        (stats.curr_trip != asset.num_trips || stats.curr_loaded_state != asset.loaded)) {
      trip_data = $scope.getCachedTrip(asset, stats.curr_trip, stats.curr_loaded_state);
      if (trip_data) {
        var new_asset = {};
        angular.extend(new_asset, asset);
        angular.extend(new_asset, trip_data);
        asset = new_asset;
        asset.x = asset.track[asset.track.length - 1][1];
        asset.y = asset.track[asset.track.length - 1][0];
        asset.is_trip_data = true;
        console.log('asset.x ' + asset.x + ', asset.y ' + asset.y);
      }
    } else {
      console.log('No cached trip');
    }
    return asset;
  };
  $scope._update_asset_stats = function (asset) {
    var stats = $scope.latest_trip_stats;
    var asset, rotation, caption;
    if (asset.pk == $scope.selected_pk) {
      asset = $scope.get_trip_asset();
    }
    asset.x = parseFloat(asset.x);
    asset.y = parseFloat(asset.y);
    asset.formatted_ts = moment(asset.curr_ts * 1000).format('hh:mm:ssA DD/MM/YY');
    asset.woeid = '2295085';
    asset.fuel_kmpl = 4.75;
    rotation = getAssetHeading(asset);
    caption = getAssetMarkerCaption(asset);
    mater.livemap.update_marker(asset.pk, asset.y, asset.x, caption, rotation);
  };
  $scope.update_asset_stats = function (asset_stats) {
    var stats = $scope.latest_trip_stats;
    var asset, rotation, caption;
    for (var pk in asset_stats) {
      asset = $scope.user_assets[pk] || {};
      asset.pk = pk;
      angular.extend(asset, asset_stats[pk]);
      $scope._update_asset_stats(asset);
    }
    $scope.update_trip_stats();
    $scope.setMapCenterAndExtent();
  };
  $scope.getCachedTrip = function (asset, trip_num, loaded) {
    asset.cached_trips = asset.cached_trips || {1: {}, 0: {}};
    loaded = loaded ? 1 : 0;
    return asset.cached_trips[loaded][trip_num];
  };
  $scope.getMaxTripNum = function (asset_pk, loaded) {
    var asset = $scope.user_assets[asset_pk];
    if (typeof asset == "undefined")
      return undefined;
    return (asset.loaded && !loaded) ? asset.num_trips - 1 : asset.num_trips;
  };
  $scope.update_trip_stats = function () {
    var stats = $scope.latest_trip_stats;
    var curr_asset = $scope.get_current_asset() || {};
    var now = moment();
    var trip_data;

    if (typeof stats.curr_trip == 'undefined' ||
        1 > stats.curr_trip || stats.curr_trip > curr_asset.num_trips) {
      stats.curr_trip = curr_asset.num_trips;
      stats.curr_loaded_state = curr_asset.loaded;
    }
    if (typeof stats.curr_loaded_state == "undefined") {
      stats.curr_loaded_state = curr_asset.loaded;
    }
    stats.curr_loaded_state = stats.curr_loaded_state ? 1 : 0;
    stats.efficiency = formatSeconds(curr_asset.efficiency || (60 * 60 * 4.5), 0);
    if (stats.curr_trip != curr_asset.num_trips ||
        stats.curr_loaded_state != curr_asset.loaded) {
      trip_data = $scope.getCachedTrip(curr_asset, stats.curr_trip, stats.curr_loaded_state);
      if (!trip_data) {
        console.log('no trip data');
        return;
      }
      stats.from = trip_data.start_place || '(unknown)';
      stats.to = trip_data.end_place || '(unknown)';
      stats.num_trips = trip_data.num_trips;
      stats.dist = ((trip_data.trip_dist || 0) / 1000).toFixed(3);
      stats.curr_speed = trip_data.curr_speed || 0;
      var travel_duration = trip_data.end_ts - trip_data.start_ts - trip_data.stoppage;
      var avg_speed = trip_data.trip_dist * 3.6 / travel_duration;
      $scope.gaugeWidget.set(avg_speed);
      stats.month_km = (trip_data.aggregate_meters / 1000.0).toFixed() || 0;
      stats.month_name = 'Trip ' + stats.curr_trip + (stats.curr_loaded_state ? " (Loaded)" : " (Unloaded)");
      now = moment.unix(trip_data.start_ts)
      stats.now_time = {};
      stats.now_time.hours = now.format('hh');
      stats.now_time.mins = now.format('mm');
      stats.now_time.ampm = now.format('a');
      stats.now_date = {};
      stats.now_date.day = now.format('D');
      stats.now_date.month = now.format('MMMM');
      stats.ignition = trip_data.ignition;
      stats.loaded = trip_data.loaded;
      var minutes = now.minutes()
        , hours = ((now.hours() + minutes / 60) % 12);
      $('#analog-clock-minutes').rotate(minutes * 360 / 60);
      $('#analog-clock-hours').rotate(hours * 360 / 12);

      if (trip_data.track) {
        mater.livemap.show_tracks(trip_data.track, trip_data.loaded);

        var violations = trip_data.violations;
        if (trip_data.track.length) {
          violations.push([
              trip_data.track[0],
              trip_data.start_ts,
              trip_data.start_place,
              3
          ]);
        }
        mater.livemap.update_violations(violations);
        mater.livemap.update_places(trip_data.places);
      }
    } else {
      stats.from = curr_asset.start_place || '(unknown)';
      stats.to = '(unknown)';
      stats.num_trips = curr_asset.num_trips;
      stats.dist = ((curr_asset.trip_dist || 0) / 1000).toFixed(3);
      stats.curr_speed = curr_asset.curr_speed || 0;
      $scope.gaugeWidget.set(stats.curr_speed);
      stats.month_km = (curr_asset.aggregate_meters / 1000.0).toFixed() || 0;
      stats.month_name = now.format('MMM YYYY');
      stats.now_time = {};
      stats.now_time.hours = now.format('hh');
      stats.now_time.mins = now.format('mm');
      stats.now_time.ampm = now.format('a');
      stats.now_date = {};
      stats.now_date.day = now.format('D');
      stats.now_date.month = now.format('MMMM');
      stats.ignition = curr_asset.ignition;
      stats.loaded = curr_asset.loaded;
      var minutes = now.minutes()
        , hours = ((now.hours() + minutes / 60) % 12);
      $('#analog-clock-minutes').rotate(minutes * 360 / 60);
      $('#analog-clock-hours').rotate(hours * 360 / 12);

      if (curr_asset.track) {
        mater.livemap.show_tracks(curr_asset.track, curr_asset.loaded);

        var violations = curr_asset.violations;
        if (curr_asset.track.length) {
          violations.push([
              curr_asset.track[0],
              curr_asset.start_ts,
              curr_asset.start_place,
              3
          ]);
        }
        mater.livemap.update_violations(violations);
        mater.livemap.update_places(curr_asset.places);
      }
    }

    if (typeof stats.weather === 'undefined') {
      stats.weather = {temp: '-', text: '', iconName: 'Sunny'};
    }
    mater.weather.get(curr_asset.woeid || '2295085', function (data) {
      var result, wind;
      try {
        result = {};
        angular.extend(result, data.query.results.channel.item.condition);
        result.iconName = mater.weather.iconNames[result.code] || 'Sunny';
        wind = data.query.results.channel.wind;
        var direction = parseInt(wind.direction);
        direction = direction + 22.5;
        if (direction > 360) { direction -= 360;}
        direction = Math.floor(direction / 45);
        direction = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'][direction];
        result.wind = Math.round(wind.speed) + ' km/h ' + (direction || '');
      } catch (e) {
        result = {};
        console.log(e.toString());
        console.log(JSON.stringify(data));
      }
      result.temp = result.temp || '-';
      result.text = result.text || '';
      result.iconName = result.iconName || 'Sunny';
      $scope.latest_trip_stats.weather = result;  // JSONP is sync so $apply is already in progress
    });
  };
  $scope.update_last_time = function () {
    var stats = $scope.latest_trip_stats;
    var curr_asset = $scope.get_current_asset() || {};
    if (typeof curr_asset.curr_ts !== 'undefined') {
      var curr_moment = moment.unix(curr_asset.curr_ts);
      var hours = moment().diff(curr_moment, 'hours');
      var format = (hours >= 12) ? 'DD/MM/YY LT' : 'hh:mm:ss';
      var pretty_diff = curr_moment.fromNow();
      stats.last_time = curr_moment.format(format) + ' (' + pretty_diff + ')';
    } else {
      stats.last_time = '(not available)';
    }
  };
  $scope.fetchTrip = function (asset, trip_num, loaded_state, then_func) {
    var url, trip_data;
    trip_data = $scope.getCachedTrip(asset, trip_num, loaded_state);
    if (trip_data) {
      then_func();
      return;
    }
    url = mater.urls.get_initial_logs;
    url += '?asset=' + asset.pk;
    url += '&trip=' + trip_num;
    url += '&loaded=' + (loaded_state ? 1 : 0);
    $http({method: 'GET', url: url}).success(function (data, status, headers, config) {
      console.log('Fetched trip: ' + url);
      loaded_state = loaded_state ? 1 : 0;
      asset.cached_trips[loaded_state][trip_num] = data.assets;
      then_func();
    });
  };

  $scope.selectTrip = function (diff, toggleLoaded, $event) {
    var stats = $scope.latest_trip_stats,
        curr_asset = $scope.get_current_asset() || {},
        trip_num = stats.curr_trip + diff,
        loaded = toggleLoaded ? (1 - stats.curr_loaded_state) : stats.curr_loaded_state;
        total_trips = $scope.getMaxTripNum(curr_asset.pk, loaded);
    var onTripChange = function () {
      var trip_data;
      $scope._update_asset_stats(curr_asset);
      $scope.update_trip_stats();
      $scope.setMapCenterAndExtent();
      trip_data = $scope.get_trip_asset();
      var travel_duration = trip_data.end_ts - trip_data.start_ts - trip_data.stoppage;
      var avg_speed = trip_data.trip_dist * 3.6 / travel_duration;
      var parking_violations = [0, 0],
          speed_violations = [0, 0];
      for (pk in trip_data.violations) {
        if (trip_data.violations[pk][3] == 1) {
          // parking violation
          parking_violations[0] = parking_violations[0] + 1;
          parking_violations[1] = parking_violations[1] + trip_data.violations[pk][2];
        } else if (trip_data.violations[pk][3] == 2) {
          // speed violation
          speed_violations[0] += 1;
          speed_violations[1] += trip_data.violations[pk][2];
        }
      }
      if (parking_violations[0]) {
        parking_violations =  parking_violations[0] + ', Total Time: ' + formatSeconds(parking_violations[1]);
      } else {
        parking_violations = '-';
      }
      if (speed_violations[0]) {
        speed_violations =  speed_violations[0] + ', Average: ' + (speed_violations[1] / speed_violations[0]).toFixed() + ' km/h';
      } else {
        speed_violations = '-';
      }
      $scope.trip_data = {
        startWhen: moment.unix(trip_data.start_ts).format('DD/MM/YY hh:mma'),
        startWhere: trip_data.start_place,
        reachedWhen: moment.unix(trip_data.end_ts).format('DD/MM/YY hh:mma'),
        reachedWhere: trip_data.end_place,
        travelledDist: trip_data.trip_dist / 1000.0,
        travelledDuration: formatSeconds(trip_data.end_ts - trip_data.start_ts, trip_data.stoppage),
        parkedDuration: formatSeconds(trip_data.stoppage, 0),
        averageSpeed: avg_speed,
        parkingViolations: parking_violations,
        speedViolations: speed_violations
      };
    };
    $event.stopPropagation();
    if (toggleLoaded && trip_num == curr_asset.num_trips && curr_asset.loaded && trip_num > 1) {
      trip_num -= 1;
    }
    else if (trip_num == curr_asset.num_trips && curr_asset.loaded && !loaded) {
      total_trips = curr_asset.num_trips;
      loaded = 1;
    }
    if (!curr_asset) {
      console.log('no curr_asset');
      return;
    }
    if (trip_num == total_trips && loaded == curr_asset.loaded) {
      stats.curr_trip = undefined;
      stats.curr_loaded_state = loaded;
      onTripChange();
      return;
    }
    if ((trip_num < total_trips ||
         (trip_num == total_trips && curr_asset.loaded != loaded))
        && trip_num > 0) {
      stats.curr_trip = trip_num;
      stats.curr_loaded_state = loaded;
      stats.month_name = 'Trip ' + trip_num;
      $scope.fetchTrip(curr_asset, trip_num, loaded, onTripChange);
      console.log('nav trip');
    } else {
      console.log('no nav ' + stats.curr_trip + ' num_trips: ' + total_trips);
    }
  };
  $scope.toggle_map = function () {
    $scope.map_maximized = !$scope.map_maximized;
    $timeout(function () {
      mater.livemap.resize_map();
      for (var pk in $scope.user_assets) {
        if (pk !== $scope.selected_pk) {
          if ($scope.map_maximized) {
            mater.livemap.show_marker(pk);
          } else {
            mater.livemap.hide_marker(pk);
          }
        }
      }
      $scope.setMapCenterAndExtent();
    });
  }
  $scope.getNextMarqueeMessage = function () {
    var asset = $scope.get_current_asset() || {},
        status_, stats;
    if (!asset || !asset.fields) {
      return '';
    }
    stats = $scope.latest_trip_stats;
    var nearest_dist = parseFloat(asset.nearest_dist);
    if (nearest_dist) {
      nearest_dist = (nearest_dist / 1000).toFixed(1);
    }
    if (asset.status === 'On Road') {
      if (asset.from === asset.curr_place) {
        status_ = ' is moving within ' + asset.from;
      } else {
        if (asset.nearest_place) {
          if (nearest_dist > 0) {
            status_ = ' has travelled ' + nearest_dist + ' km from ' + asset.nearest_place;
          } else {
            status_ = ' is ' + (0 - nearest_dist) + ' km from ' + asset.nearest_place;
          }
        } else {
          status_ = ' has travelled ' + parseFloat(stats.dist).toFixed(1) + ' km';
          if (stats.from && stats.from !== '(unknown)') {
            status_ += ' from ' + stats.from;
          }
        }
      }
    } else if (!asset.status) {
      status_ = ' does not have any logs yet';
    } else {
      status_ = ' is ' + asset.status;
      if (asset.curr_place) {
        status_ += ' at ' + asset.curr_place;
      } else {
        if (asset.nearest_place) {
          if (nearest_dist > 0) {
            status_ += ' ' + nearest_dist + ' km ' + ' ahead of ' + asset.nearest_place;
          } else {
            status_ += ' ' + (0 - nearest_dist) + ' km ' + ' before ' + asset.nearest_place;
          }
        } else {
          if (stats.from && stats.from !== '(unknown)') {
            status_ += ' ' + parseFloat(stats.dist).toFixed(1) + ' km from ' + stats.from;
          }
        }
      }
    }
    return asset.fields.name + (stats.ignition ? '*' : '') + ' ' + status_;
  };
  $scope.showLiveMap = function () {
    $scope.pane = 'livemap'
    setTimeout('mater.livemap.resize_map()', 0);
  };
  $scope.showTripReports = function () {
    var tripScope = $('#trip-ctrl').scope();
    if (tripScope.data == null) {
      tripScope.loadData();
    }
    $scope.pane = 'report-trips'
    setTimeout(fixReportHeight, 0)
  };
  $scope.showPlaceReports = function () {
    var placeScope = $('#place-ctrl').scope();
    if (placeScope.data == null) {
      placeScope.loadData();
    }
    $scope.pane = 'report-places'
    setTimeout(fixReportHeight, 0)
  };
  $scope.showLifelogReports = function () {
    var lifelogScope = $('#lifelog-ctrl').scope();
    if (lifelogScope.data == null) {
      lifelogScope.loadData();
    }
    $scope.pane = 'report-lifelog'
    setTimeout(fixReportHeight, 0)
  };
  $scope.showSupplyEfficiencyReports = function () {
    var reportScope = $('#supply-efficiency-ctrl').scope();
    if (reportScope.data == null) {
      reportScope.loadData();
    }
    reportScope.selected_asset = $scope.selected_pk;
    $scope.pane = 'report-supply-efficiency';
    setTimeout(fixReportHeight, 0)
  };
  GaugeTextRenderer = (function() {
    function GaugeTextRenderer(el) {
      this.el = el;
    }
    GaugeTextRenderer.prototype = new TextRenderer(null);

    GaugeTextRenderer.prototype.render = function(gauge) {
      var speed = gauge.displayedValue.toFixed();
      return this.el.innerHTML =   '<small style="visibility: hidden;">&emsp;km/h</small>' + speed + ' <small> km/h</small>';
    };

    return GaugeTextRenderer;
  })();
  var initGauge = function () {
    var gaugeElement = document.getElementById('speed-gauge'),
        gaugeTextElement = document.getElementById('speed-gauge-text'),
        gauge = new Gauge(gaugeElement);
    gauge.setTextField(new GaugeTextRenderer(gaugeTextElement));
    gauge.setOptions({
      lines: 20, // The number of lines to draw
      angle: 0.1, // The length of each line
      lineWidth: 0.25, // The line thickness
      pointer: {
        length: 0.75, // The radius of the inner circle
        strokeWidth: 0.05, // The rotation offset
        color: '#000000' // Fill color
      },
      colorStart: '#6FADCF',   // Colors
      colorStop: '#8FC0DA',    // just experiment with them
      strokeColor: '#FFFFFF',   // to see which ones work best for you
      generateGradient: true
    });
    gauge.maxValue = 80;
    gauge.ctx.clearRect(0, 0, gauge.canvas.width, gauge.canvas.height);
    gauge.render();
    gauge.set(0);
    return gauge;
  }
  $scope.gaugeWidget = initGauge();
};

function TripReportCtrl($scope, $http) {
  $scope.data = null;
  $scope.selected_route = null;
  $scope.averageTravelTime = null;
  $scope.loadData = function () {
    $http({method: 'GET', url: '/ajax/trip-report/'}).success(function (data, status, headers, config) {
      $scope.data = data;
      var routes = data.routes;
      if (routes) {
        $scope.selected_route = 0;
        setTimeout(function () {
          $('#trip-report-select-trip').select2('val', 0);
        }, 0);
      }
    })
  };
  $scope.dataFilter = function(line) {
    if (line[0] != $scope.selected_route) {
      return false;
    }
    if ($scope.selected_asset != null && line[1] != $scope.selected_asset) {
      return false;
    }
    if ($scope.dateRangeStart != null && moment(line[2] * 1000) < $scope.dateRangeStart.sod()) {
      return false;
    }
    if ($scope.dateRangeEnd != null && moment(line[2] * 1000) > $scope.dateRangeEnd.eod()) {
      return false;
    }
    return true;
  }
  $scope.formatTS = function (ts) {
    return moment(ts * 1000).format('hh:mmA DD/MM/YY')
  };
  $scope.formatTD = function (t1, t2) {
    return formatSeconds(t2 - t1);
  };
  $scope.updateAverageStay = function () {
    var sum_stay = 0,
        count_stay = 0;
    if (!$scope.data)
      return;
    for (var i = 0; i < $scope.data.report.length; i++) {
      var line = $scope.data.report[i];
      if ($scope.dataFilter(line)) {
        // console.log(JSON.stringify(line))
        sum_stay = sum_stay + line[3] - line[2];
        count_stay = count_stay + 1;
      }
    }
    if (!count_stay) {
      $scope.averageTravelTime = '--';
    } else {
      var seconds = Math.floor(sum_stay/count_stay);
      $scope.averageTravelTime = formatSeconds(seconds);
    }
  };
  $scope.$watch('data', $scope.updateAverageStay);
  $scope.$watch('selected_route', $scope.updateAverageStay);
  $scope.$watch('selected_asset', $scope.updateAverageStay);
  $scope.$watch('dateRangeStart', $scope.updateAverageStay);
  $scope.$watch('dateRangeEnd', $scope.updateAverageStay);
  $scope.updateReportHeader = function () {
    setTimeout(function () {
      updateReportHeader();
    }, 0);
  }
}

function PlaceReportCtrl($scope, $http) {
  $scope.data = null;
  $scope.selected_place = null;
  $scope.selected_asset = null;
  $scope.dateRangeStart = null;
  $scope.dateRangeEnd = null;
  $scope.averageStay = 0;
  $scope.loadData = function () {
    $http({method: 'GET', url: '/ajax/place-report/'}).success(function (data, status, headers, config) {
      $scope.data = data;
      var places = data.places;
      if (places) {
        $scope.selected_place = Object.keys(places)[0];
        setTimeout(function () {
          $('#place-report-select-place').select2('val', $scope.selected_place);
        }, 0);
      }
    })
  };
  $scope.dataFilter = function(line) {
    if (line[0] != $scope.selected_place) {
      return false;
    }
    if ($scope.selected_asset != null && line[1] != $scope.selected_asset) {
      return false;
    }
    if ($scope.dateRangeStart != null && moment(line[2] * 1000) < $scope.dateRangeStart.sod()) {
      return false;
    }
    if ($scope.dateRangeEnd != null && moment(line[2] * 1000) > $scope.dateRangeEnd.eod()) {
      return false;
    }
    return true;
  }
  $scope.formatTS = function (ts) {
    return moment(ts * 1000).format('hh:mmA DD/MM/YY')
  };
  $scope.formatTD = function (t1, t2) {
    return formatSeconds(t2 - t1);
  };
  $scope.updateAverageStay = function () {
    var sum_stay = 0,
        count_stay = 0;
    if (!$scope.data)
      return;
    for (var i = 0; i < $scope.data.report.length; i++) {
      var line = $scope.data.report[i];
      if ($scope.dataFilter(line)) {
        // console.log(JSON.stringify(line))
        sum_stay = sum_stay + line[3] - line[2];
        count_stay = count_stay + 1;
      }
    }
    // console.log(JSON.stringify([sum_stay, count_stay]))
    if (count_stay) {
      var seconds = Math.floor(sum_stay/count_stay);
      $scope.averageStay = formatSeconds(seconds);
    } else {
      $scope.averageStay = '--';
    }
  };
  $scope.$watch('data', $scope.updateAverageStay);
  $scope.$watch('selected_place', $scope.updateAverageStay);
  $scope.$watch('selected_asset', $scope.updateAverageStay);
  $scope.$watch('dateRangeStart', $scope.updateAverageStay);
  $scope.$watch('dateRangeEnd', $scope.updateAverageStay);
  $scope.updateReportHeader = function () {
    setTimeout(function () {
      updateReportHeader();
    }, 0);
  }
}

function LifelogReportCtrl($scope, $http) {
  $scope.data = null;
  $scope.selected_asset = null;
  $scope.dateRangeStart = null;
  $scope.dateRangeEnd = null;
  $scope.filteredReport = [];
  $scope.loadData = function () {
    $http({method: 'GET', url: '/ajax/lifelog-report/'})
      .success(function (data, status, headers, config) {
        var asset_ids = Object.keys(data)
          , report;
        $scope.data = data;
        if (asset_ids) {
          $scope.selected_asset = asset_ids[0];
          setTimeout(function () {
            $('#lifelog-report-select-asset').select2('val', $scope.selected_asset);
          }, 0);
        }
        asset_ids.map(function (id) {
          report = data[id].report;
          for (var i = 0; i < report.length; i++) {
            report[i][0] = moment(report[i][0] * 1000);
            report[i][1] = moment(report[i][1] * 1000);
          }
        });
      });
  };
  var updateFilter = function () {
    var report, i, line, prev_sod = null, curr_sod, date,
        dateFilterStart = $scope.dateRangeStart && $scope.dateRangeStart.sod(),
        dateFilterEnd = $scope.dateRangeStart && $scope.dateRangeEnd.eod();
    $scope.filteredReport = [];
    if (!$scope.data) {
      return;
    }
    i = 0;
    report = $scope.data[$scope.selected_asset].report;
    while(i < report.length &&
          (dateFilterStart != null && report[i][0] < dateFilterStart)) {
      i++;
    }
    for(; i < report.length && (dateFilterEnd == null || report[i][0] <= dateFilterEnd); i++) {
        line = report[i]
        curr_sod = line[1].sod().local();
        if (prev_sod === null || prev_sod.diff(curr_sod) != 0) {
          date = curr_sod.format('DD/MM/YY');
        } else {
          date = '';
        }
        prev_sod = curr_sod;
        $scope.filteredReport.push([
            date,
            line[0].format('hh:mmA'),
            line[1].format('hh:mmA'),
            line[2],
            line[3],
            line[4].toFixed(1),
            formatSeconds(line[1].diff(line[0], 'seconds'))
        ]);
    }
  };
  $scope.$watch('dateRangeStart', updateFilter);
  $scope.$watch('dateRangeEnd', updateFilter);
  $scope.$watch('selected_asset', updateFilter);
  $scope.formatTS = function (ts) {
    return ts.format('hh:mmA')
  };
  $scope.formatTD = function (t1, t2) {
    return formatSeconds(t2.diff(t1, 'seconds'));
  };
  $scope.getDate = function (i) {
    var startTS = $scope.filteredReport[i][1].sod().local(),
        prevTS;
    if (i > 0) {
      prevTS = $scope.filteredReport[i - 1][1].sod().local();
      if (prevTS.diff(startTS) == 0) {
        return '';
      }
    }
    return startTS.format('DD/MM/YY');
  };
  $scope.updateReportHeader = function () {
    setTimeout(function () {
      updateReportHeader();
    }, 0);
  }
}

function SupplyEfficiencyReportCtrl($scope, $http) {
  $scope.data = null;
  $scope.selected_asset = null;
  $scope.dateRangeStart = null;
  $scope.dateRangeEnd = null;
  $scope.averageEfficiency = '';
  $scope.filteredReport = [];
  $scope.loadData = function () {
    $http({method: 'GET', url: '/ajax/supply-efficiency-report/'})
      .success(function (data, status, headers, config) {
        var asset_ids = Object.keys(data)
          , report;
        $scope.data = data;
        if (data.assets) {
          setTimeout(function () {
            $('#supply-efficiency-report-select-asset').select2('val', $scope.selected_asset);
          }, 0);
        }
        report = data.rows;
        report.map(function (row) {
          var place_duration = row.exit - row.enter,
              total_time = 0.0,
              diff;
          // Use only parking time for token and add travelling to `internalTravel`
          row.internalTravel += row.token[0] - row.token[1];

          total_time += row.parked[0];
          total_time += row.token[1];
          total_time += row.gantry[0];
          total_time += row.billing[0];
          total_time += row.internalTravel;

          diff = place_duration - total_time;
          if (diff < 0) {
            console.log('Bad diff: ' + diff);
          }
          row.internalTravel += place_duration - total_time;

          row.efficiency = place_duration - row.parked[0];
          row.enter = moment.unix(row.enter);
          row.exit = moment.unix(row.exit);
          row.date = row.enter.format('DD/MM/YY');
        });
        updateFilter();
      });
  };
  var updateFilter = function () {
    var dateFilterStart = $scope.dateRangeStart && $scope.dateRangeStart.sod()
      , dateFilterEnd = $scope.dateRangeStart && $scope.dateRangeEnd.eod(),
      total_efficiency = 0;
    $scope.filteredReport = [];
    if (!$scope.data) {
      return;
    }
    $scope.filteredReport = $scope.data.rows.filter(function (row) {
      if (row.asset == $scope.data.assets[$scope.selected_asset] &&
          (dateFilterStart == null || row.enter >= dateFilterStart) &&
          (dateFilterEnd == null || row.exit <= dateFilterEnd)) {
          total_efficiency += row.efficiency;
        return true;
      }
      return false;
    });
    if (!$scope.filteredReport.length) {
      $scope.averageEfficiency = 60 * 60 * 4.5;
    } else {
      $scope.averageEfficiency = (total_efficiency / $scope.filteredReport.length);
    }
  };
  $scope.$watch('dateRangeStart', updateFilter);
  $scope.$watch('dateRangeEnd', updateFilter);
  $scope.$watch('selected_asset', updateFilter);
  $scope.formatTS = function (ts) {
    return ts.format('hh:mmA')
  };
  $scope.formatSeconds = function (td) {
    return formatSeconds(td);
  };
  $scope.getDate = function (i) {
    var startTS = $scope.filteredReport[i][1].sod().local(),
        prevTS;
    if (i > 0) {
      prevTS = $scope.filteredReport[i - 1][1].sod().local();
      if (prevTS.diff(startTS) == 0) {
        return '';
      }
    }
    return startTS.format('DD/MM/YY');
  };
  $scope.updateReportHeader = function () {
    setTimeout(function () {
      updateReportHeader();
    }, 0);
  }
}
