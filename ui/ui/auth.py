from django.contrib.auth.backends import ModelBackend


class SingleSessionModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        user = super(SingleSessionModelBackend, self).authenticate(
            username, password)
        if user is None:
            return None
        already_logged_in = user.profile.is_logged_in
        return user if not already_logged_in or user.username == 'demo' \
            else None
