function AssetsCtrl($scope) {
  $scope.assets = [];

  $scope.load_data = function () {
    $.getJSON(
      mater.urls.get_assets,
      function (data) {
        if (data.ok) {
          $scope.$apply(function () {
            $scope.assets = data.data;
          });
        }
      }
    )
  }
  $scope.save_data = function () {
    var ajax_data = {};
    ajax_data.command = 'modify';
    assets = [];
    for (var i=0; i < $scope.assets.length; i++) {
      assets[i] = {
        pk: $scope.assets[i].pk,
        name: $scope.assets[i].name,
        speed_threshold: $scope.assets[i].speed_threshold
      }
    }
    ajax_data.json = JSON.stringify(assets);
    var jqxhr = $.getJSON(
        mater.urls.put_assets,
        ajax_data,
        function (data) {
          if (!data.ok) {
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            $.bootstrapGrowl('Data saved',
              {type: 'success', align: 'center'});
            var users_scope = angular.element('#users').scope()
            users_scope.$apply('load_data()');
          }
        }
    ).error(function () {
      $.bootstrapGrowl('Error saving data, please try again',
        {type: 'error', align: 'center'});
    });
  }

  $scope.load_data();
}
