from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

from ui.forms import SingleSessionAuthForm

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$',
        'ui.views.html.home', name='home'),
    url(r'^livemap/$',
        'ui.views.html.livemap', name='livemap'),
    url(r'^settings/$',
        'ui.views.html.settings', name='settings'),

    # Ajax view
    url(r'^ajax/update-assets/$',
        'ui.views.ajax.update_assets', name='update_assets'),
    url(r'^ajax/get-initial-logs/$',
        'ui.views.ajax.get_initial_logs', name='get_initial_logs'),
    url(r'^ajax/locations/get/$',
        'ui.views.ajax.get_locations', name='get_locations'),
    url(r'^ajax/locations/put/$',
        'ui.views.ajax.put_locations', name='put_locations'),
    url(r'^ajax/users/get/$',
        'ui.views.ajax.get_users', name='get_users'),
    url(r'^ajax/users/put/$',
        'ui.views.ajax.put_users', name='put_users'),
    url(r'^ajax/assets/get/$',
        'ui.views.ajax.get_assets', name='get_assets'),
    url(r'^ajax/assets/put/$',
        'ui.views.ajax.put_assets', name='put_assets'),
    url(r'^ajax/routes/get/$',
        'ui.views.ajax.get_routes', name='get_routes'),
    url(r'^ajax/routes/put/$',
        'ui.views.ajax.put_routes', name='put_routes'),
    url(r'^ajax/suggest-routes/$',
        'ui.views.ajax.suggest_routes', name='suggest_routes'),
    url(r'^ajax/suggest-places/$',
        'ui.views.ajax.suggest_places', name='suggest_places'),
    url(r'^ajax/trip-report/$',
        'ui.views.ajax.trip_report_data', name='trip_report_data'),
    url(r'^ajax/place-report/$',
        'ui.views.ajax.place_report_data', name='place_report_data'),
    url(r'^ajax/lifelog-report/$',
        'ui.views.ajax.lifelog_report_data', name='lifelog_report_data'),
    url(r'^ajax/supply-efficiency-report/$',
        'ui.views.ajax.supply_efficiency_report_data',
        name='supply_efficiency_report_data'),
    url(r'^ajax/select-asset/(?P<asset_id>\d+)/$',
        'ui.views.ajax.select_asset', name='select_asset'),

    url(settings.LOGIN_URL.lstrip('/'),
        'ui.views.auth.login'),
    url(settings.LOGOUT_URL.lstrip('/'),
        'django.contrib.auth.views.logout', {'template_name': 'logout.html'}),
)


admin.autodiscover()


urlpatterns += (
    url(r'^admin/', include(admin.site.urls)),
)
