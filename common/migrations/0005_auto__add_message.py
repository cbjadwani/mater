# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Message'
        db.create_table('common_message', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.DevLog'])),
            ('delivered', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('common', ['Message'])


    def backwards(self, orm):
        # Deleting model 'Message'
        db.delete_table('common_message')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'common.asset': {
            'Meta': {'object_name': 'Asset'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'assets'", 'symmetrical': 'False', 'to': "orm['auth.User']"})
        },
        'common.device': {
            'Meta': {'object_name': 'Device'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '19', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'common.deviceinstallationlog': {
            'Meta': {'object_name': 'DeviceInstallationLog'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': "orm['common.Asset']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': "orm['common.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        'common.devlog': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'DevLog'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dev_logs'", 'to': "orm['common.Asset']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'logs'", 'to': "orm['common.Device']"}),
            'event': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'first_ts': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Place']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            'speed': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        'common.message': {
            'Meta': {'object_name': 'Message'},
            'delivered': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.DevLog']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'common.path': {
            'Meta': {'object_name': 'Path'},
            'default_stop_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'default_travel_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_route_json': ('django.db.models.fields.TextField', [], {}),
            'place1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paths'", 'to': "orm['common.Place']"}),
            'place2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paths_reverse'", 'to': "orm['common.Place']"}),
            'total_distance': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        'common.place': {
            'Meta': {'object_name': 'Place'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'max_latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5', 'db_index': 'True'}),
            'max_longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5', 'db_index': 'True'}),
            'min_latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5', 'db_index': 'True'}),
            'min_longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5', 'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'polygon_json': ('django.db.models.fields.TextField', [], {})
        },
        'common.route': {
            'Meta': {'object_name': 'Route'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'total_distance': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_duration': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'common.routeleg': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'RouteLeg'},
            '_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Path']"}),
            'path_order': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'route': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'legs'", 'to': "orm['common.Route']"}),
            'stop_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'travel_duration': ('django.db.models.fields.DateTimeField', [], {})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']