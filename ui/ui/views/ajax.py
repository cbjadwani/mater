import simplejson as json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.serializers import serialize, deserialize

from annoying.decorators import ajax_request

from common.models import DevLog, Place, Path, UserProfile, Asset
from ui.views.main import get_asset_stats, get_messages, \
    get_suggested_places, get_suggested_routes, get_trip_report_data, \
    get_place_report_data, get_asset_lifelog_report_data, select_user_asset, \
    supply_efficiency_report


COMMAND_ADD = 'add'
COMMAND_MODIFY = 'modify'
COMMAND_DELETE = 'delete'


@login_required
@ajax_request
def get_initial_logs(request):
    asset_stats = get_asset_stats(request.user, request.GET)
    messages = get_messages(request.user)
    return {'assets': asset_stats, 'messages': messages}


def _ajax_put(data, command, model):
    result = {}
    not_modified = []
    modified = []

    try:
        objs = deserialize('json', data)
    except Exception:
        result['error'] = 'DeserializationError'
        return result

    if command == COMMAND_ADD:
        not_modified = []
        for obj in objs:
            pk = obj.object.pk
            if isinstance(obj.object, model) and \
                    not model.objects.filter(pk=pk).exists():
                obj.save()
                modified.append(obj.object.pk)
            else:
                not_modified.append(pk)
        if not_modified:
            result['error'] = 'Not added'
            result['not_modified'] = not_modified
        else:
            result['ok'] = True
            result['added'] = modified

    elif command == COMMAND_MODIFY:
        not_modified = []
        for obj in objs:
            pk = obj.object.pk
            if isinstance(obj.object, model) and \
                    model.objects.filter(pk=pk).exists():
                obj.save()
            else:
                not_modified.append(pk)
        if not_modified:
            result['error'] = 'Not modified'
            result['not_modified'] = not_modified
        else:
            result['ok'] = True

    elif command == COMMAND_DELETE:
        not_modified = []
        for obj in objs:
            pk = obj.object.pk
            if isinstance(obj.object, model) and \
                    model.objects.filter(pk=pk).exists():
                obj.object.delete()
            else:
                not_modified.append(pk)
        if not_modified:
            result['error'] = 'Not deleted'
            result['not_modified'] = not_modified
        else:
            result['ok'] = True

    else:
        result['error'] = 'Bad command'

    return result


def _ajax_get(_data, model):
    result = {}

    try:
        objs = serialize('json', model.objects.all())
    except Exception:
        result['error'] = 'SerializationError'
    else:
        result['ok'] = True
        result['data'] = json.loads(objs)
    return result


@login_required
@ajax_request
def get_locations(request):
    data = request.GET.get('json')

    result = _ajax_get(data, Place)

    return result


@login_required
@ajax_request
def get_routes(request):
    data = request.GET.get('json')

    result = _ajax_get(data, Path)

    return result


@login_required
@ajax_request
def get_users(request):
    user = request.user
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    if profile.role != UserProfile.ROLE_OWNER:
        return {'error': 'Not Allowed'}
    users = User.objects.filter(profile__customer=profile.customer)
    result = []
    for user in users:
        try:
            profile = user.profile
        except UserProfile.DoesNotExist:
            profile = UserProfile.objects.create(user=user)
        result.append({
            'pk': user.pk,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'phone': user.profile.phone,
            'assets': map(str, user.assets.values_list('pk', flat=True))
        })

    all_assets = {a.pk: a.name
                  for a in Asset.objects.filter(customer=profile.customer)}
    return {'ok': True, 'data': result, 'all_assets': all_assets}


@login_required
@ajax_request
def get_assets(request):
    user = request.user
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    if profile.role != UserProfile.ROLE_OWNER:
        return {'error': 'Not Allowed'}
    result = []
    assets = Asset.objects.filter(customer=profile.customer)
    for asset in assets:
        result.append({
            'pk': asset.pk,
            'name': asset.name,
            'speed_threshold': asset.speed_threshold,
        })

    return {'ok': True, 'data': result}


@login_required
@ajax_request
def put_locations(request):
    command = request.GET.get('command')
    data = request.GET.get('json')

    result = _ajax_put(data, command, Place)

    return result


@login_required
@ajax_request
def put_routes(request):
    command = request.GET.get('command')
    data = request.GET.get('json')

    result = _ajax_put(data, command, Path)

    return result


@login_required
@ajax_request
def put_users(request):
    user = request.user
    command = request.GET.get('command')
    data = request.REQUEST.get('json', '[]')
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    if profile.role != UserProfile.ROLE_OWNER:
        return {'error': 'Not Allowed'}
    users = User.objects.filter(profile__customer=profile.customer)
    try:
        data = json.loads(data)
    except:
        return {'error': 'Data error'}
    for user_json in data:
        if command == COMMAND_ADD:
            user = User(username=user_json['username'])
            user.profile = UserProfile(user=user, customer=profile.customer)
        else:
            user = users.get(pk=user_json['pk'])
        user.first_name = user_json['first_name']
        user.last_name = user_json['last_name']
        user.email = user_json['email']
        user.profile.phone = user_json['phone']
        if 'password' in user_json:
            user.set_password(user_json['password'])
        user.save()
        user.profile.user = user
        user.profile.save()
        user_assets = set(user.assets.values_list('pk', flat=True))
        user_json_assets = set(user_json['assets'])
        to_remove = user_assets - user_json_assets
        to_add = user_json_assets - user_assets
        user.assets.remove(*Asset.objects.filter(pk__in=to_remove))
        user.assets.add(*Asset.objects.filter(pk__in=to_add))

    return {'ok': True}


@login_required
@ajax_request
def put_assets(request):
    user = request.user
    command = request.GET.get('command')
    data = request.REQUEST.get('json', '[]')
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    if profile.role != UserProfile.ROLE_OWNER:
        return {'error': 'Not Allowed'}
    if command != COMMAND_MODIFY:
        return {'error': 'Not Allowed'}
    assets = Asset.objects.filter(customer=profile.customer)
    try:
        data = json.loads(data)
    except:
        return {'error': 'Data error'}
    for asset_json in data:
        asset = assets.get(pk=asset_json['pk'])
        asset.name = asset_json['name']
        asset.speed_threshold = asset_json['speed_threshold']
        asset.save()

    return {'ok': True}


@login_required
@ajax_request
def update_assets(request):
    user = request.user
    #assets = user.assets.all()
    # changing to one asset per user (probably temporarily)
    asset = user.assets.all().distinct()[0]

    #logs = []
    #for asset in assets:
    device = asset.current_device
    log = {'name': asset.name}
    if device is not None:
        try:
            dev_log = asset.dev_logs.filter(device=device).latest()
        except DevLog.DoesNotExist:
            log['status'] = 'No Logs'
        else:
            log.update({
                'ts': dev_log.get_ts(),
                'x': dev_log.longitude.to_eng_string(),
                'y': dev_log.latitude.to_eng_string(),
                'speed': dev_log.speed,
            })
    else:
        log['status'] = 'No Device'
    #logs.append(log)
    #return {'logs': logs}
    return log


@login_required
@ajax_request
def suggest_places(request):
    user = request.user
    return get_suggested_places(user)


@login_required
@ajax_request
def suggest_routes(request):
    user = request.user
    return get_suggested_routes(user)


@login_required
@ajax_request
def trip_report_data(request):
    user = request.user
    data = get_trip_report_data(user)
    return data


@login_required
@ajax_request
def place_report_data(request):
    user = request.user
    data = get_place_report_data(user)
    return data


@login_required
@ajax_request
def lifelog_report_data(request):
    user = request.user
    data = get_asset_lifelog_report_data(user)
    return data


@login_required
@ajax_request
def select_asset(request, asset_id):
    res = {}
    user = request.user
    try:
        select_user_asset(user, asset_id)
        res['ok'] = True
    except Exception:
        res['error'] = True
    return res


@login_required
@ajax_request
def supply_efficiency_report_data(request):
    user = request.user
    data = supply_efficiency_report(user)
    return data
