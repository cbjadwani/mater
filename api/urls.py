from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^device/(?P<dev_id>[A-F0-9]{5})/log/$',
        'api.views.log', name='log'),
    url(r'^device/(?P<dev_id>[A-F0-9]{5})/stopped/$',
        'api.views.stopped', name='stopped'),
    url(r'^opengts/$',
        'api.views.opengts', name='opengts'),
    url(r'$',
        'api.views.root', name='root'),
    # Examples:
    # url(r'^$', 'api.views.home', name='home'),
    # url(r'^api/', include('api.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
