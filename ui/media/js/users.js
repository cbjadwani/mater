function UsersCtrl($scope) {
  $scope.users = [];
  $scope.all_assets = {};
  $scope.modalUserAddOpen = false;
  $scope.addFormData = {
    username: null, password: null, password2: null, email: '', phone: '', assets: []
  };
  $scope.showPasswordConfirm = false;

  $scope.load_data = function () {
    $.getJSON(
      mater.urls.get_users,
      function (data) {
        if (data.ok) {
          $scope.$apply(function () {
            $scope.users = data.data;
            $scope.all_assets = data.all_assets;
          });
        }
      }
    )
  }
  $scope.save_data = function () {
    var users = [];
    for (var i = 0; i < $scope.users.length; i++) {
      users[i] = {
        pk: $scope.users[i].pk,
        first_name: $scope.users[i].first_name,
        last_name: $scope.users[i].last_name,
        assets: $scope.users[i].assets,
        email: $scope.users[i].email,
        phone: $scope.users[i].phone
      }
    }
    sendToServer('modify', users);
  }
  var sendToServer = function (command, users) {
    var ajax_data = {command: command};
    ajax_data.json = JSON.stringify(users);
    var jqxhr = $.getJSON(
        mater.urls.put_users,
        ajax_data,
        function (data) {
          if (!data.ok) {
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            $.bootstrapGrowl('Data saved', {type: 'success', align: 'center'});
            if (command == 'add') {
              $scope.load_data();
            }
          }
        }
    ).error(function () {
      $.bootstrapGrowl('Error saving data, please try again',
        {type: 'error', align: 'center'});
    });
  }
  $scope.get_asset_names = function (pks) {
    var names = [];
    for (var i=0; i < pks.length; i++) {
      names.push($scope.all_assets[pks[i]])
    }
    return names.join(', ')
  }
  $scope.modalUserAddCancel = function() {
    $scope.modalUserAddOpen = false;
  };
  $scope.modalUserAddSave = function() {
    $scope.modalUserAddOpen = false;
    var formData = $scope.addFormData;
    var user = {
      username: formData.username,
      password: formData.password,
      first_name: formData.first_name,
      last_name: formData.last_name,
      email: formData.email || '',
      phone: formData.phone || '',
      assets: formData.assets
    };
    sendToServer('add', [user]);
  };

  $scope.load_data();
}
