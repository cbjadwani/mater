var mater = window.mater || {};

mater.PanLockControl = (function () {
  "use strict";

  function PanLockControl(map) {
    var wrapperDiv, uiDiv, controlDiv;
    var control = this;
    control.locked = true;
    control.dont_unlock = false;

    wrapperDiv = document.createElement('DIV');
    uiDiv = document.createElement('DIV');
    controlDiv = document.createElement('IMG');

    wrapperDiv.style.padding = '5px 0';
    uiDiv.style.padding = '0 4px 2px';
    uiDiv.style.textAlign = 'center';
    uiDiv.style.backgroundColor = 'white';
    uiDiv.style.border = '1px solid #717B87';
    uiDiv.style.boxShadow = '0 2px 4px rgba(0, 0, 0, 0.4)';
    uiDiv.style.cursor = 'pointer';
    controlDiv.src = '/media/img/lock.png';
    wrapperDiv.appendChild(uiDiv);
    uiDiv.appendChild(controlDiv);

    control.wrapperDiv = wrapperDiv;
    control.controlDiv = controlDiv;

    google.maps.event.addDomListener(wrapperDiv, 'click', function () {
      if (!control.locked) {
        control.recenter();
        controlDiv.src = '/media/img/lock.png';
        control.locked = true;
      } else {
        controlDiv.src = '/media/img/lock_open.png';
        control.locked = false;
      }
    });

    google.maps.event.addDomListener(map, 'center_changed', function () {
      control.unlock();
    });
  }

  PanLockControl.prototype.unlock = function () {
    var control = this;
    if (!control.dont_unlock) {
      control.locked = false;
      control.controlDiv.src = '/media/img/lock_open.png';
    }
  }

  PanLockControl.prototype.recenter = function () {
    var control = this, center, zoom, bounds;
    control.dont_unlock = true;
    map.setMapTypeId(control.mapTypeId);
    control.extent = control.extent || {};
    if (typeof control.extent.center != 'undefined') {
      center = new google.maps.LatLng(control.extent.center[0], control.extent.center[1]);
      map.panTo(center);
      map.setZoom(control.extent.zoom || 14);
    } else if (typeof control.extent.bounds != 'undefined') {
      bounds = {
        sw: new google.maps.LatLng(control.extent.bounds.minLat, control.extent.bounds.minLng),
        ne: new google.maps.LatLng(control.extent.bounds.maxLat, control.extent.bounds.maxLng)
      };
      bounds = new google.maps.LatLngBounds(bounds.sw, bounds.ne);
      map.fitBounds(bounds);
    }
    control.dont_unlock = false;
  }

  PanLockControl.prototype.setCenterAndExtent = function (extent, mapTypeId) {
    this.extent = extent;
    this.mapTypeId = mapTypeId;
    if (this.locked) {
      this.recenter();
    }
  }
  return PanLockControl;
}());
