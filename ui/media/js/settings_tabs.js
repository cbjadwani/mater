function SettingsTabsCtrl($scope) {
  var active_tab = angular.element('.tab-pane.active').attr('id');
  $scope.show_map = (active_tab !== 'users') && (active_tab !== 'assets');
}
