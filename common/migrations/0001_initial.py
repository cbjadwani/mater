# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Device'
        db.create_table('common_device', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('imei', self.gf('django.db.models.fields.CharField')(max_length=19, blank=True)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
        ))
        db.send_create_signal('common', ['Device'])

        # Adding model 'Asset'
        db.create_table('common_asset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal('common', ['Asset'])

        # Adding M2M table for field users on 'Asset'
        db.create_table('common_asset_users', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('asset', models.ForeignKey(orm['common.asset'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('common_asset_users', ['asset_id', 'user_id'])

        # Adding model 'DeviceInstallationLog'
        db.create_table('common_deviceinstallationlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(related_name='installations', to=orm['common.Device'])),
            ('asset', self.gf('django.db.models.fields.related.ForeignKey')(related_name='installations', to=orm['common.Asset'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, db_index=True, blank=True)),
        ))
        db.send_create_signal('common', ['DeviceInstallationLog'])

        # Adding model 'DevLog'
        db.create_table('common_devlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(related_name='logs', to=orm['common.Device'])),
            ('asset', self.gf('django.db.models.fields.related.ForeignKey')(related_name='dev_logs', to=orm['common.Asset'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(max_digits=7, decimal_places=5)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(max_digits=8, decimal_places=5)),
            ('speed', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('common', ['DevLog'])

        # Adding model 'Place'
        db.create_table('common_place', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50, db_index=True)),
            ('map_name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('polygon_json', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('common', ['Place'])

        # Adding model 'Path'
        db.create_table('common_path', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='paths', to=orm['common.Place'])),
            ('place2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='paths_reverse', to=orm['common.Place'])),
            ('map_route_json', self.gf('django.db.models.fields.TextField')()),
            ('total_distance', self.gf('django.db.models.fields.PositiveIntegerField')(null=True)),
            ('default_travel_duration', self.gf('django.db.models.fields.DateTimeField')()),
            ('default_stop_duration', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('common', ['Path'])

        # Adding model 'Route'
        db.create_table('common_route', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('total_distance', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('total_duration', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('common', ['Route'])

        # Adding model 'RouteLeg'
        db.create_table('common_routeleg', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('route', self.gf('django.db.models.fields.related.ForeignKey')(related_name='legs', to=orm['common.Route'])),
            ('path', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Path'])),
            ('path_order', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('travel_duration', self.gf('django.db.models.fields.DateTimeField')()),
            ('stop_duration', self.gf('django.db.models.fields.DateTimeField')()),
            ('_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('common', ['RouteLeg'])


    def backwards(self, orm):
        # Deleting model 'Device'
        db.delete_table('common_device')

        # Deleting model 'Asset'
        db.delete_table('common_asset')

        # Removing M2M table for field users on 'Asset'
        db.delete_table('common_asset_users')

        # Deleting model 'DeviceInstallationLog'
        db.delete_table('common_deviceinstallationlog')

        # Deleting model 'DevLog'
        db.delete_table('common_devlog')

        # Deleting model 'Place'
        db.delete_table('common_place')

        # Deleting model 'Path'
        db.delete_table('common_path')

        # Deleting model 'Route'
        db.delete_table('common_route')

        # Deleting model 'RouteLeg'
        db.delete_table('common_routeleg')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'common.asset': {
            'Meta': {'object_name': 'Asset'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'assets'", 'symmetrical': 'False', 'to': "orm['auth.User']"})
        },
        'common.device': {
            'Meta': {'object_name': 'Device'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imei': ('django.db.models.fields.CharField', [], {'max_length': '19', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        'common.deviceinstallationlog': {
            'Meta': {'object_name': 'DeviceInstallationLog'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': "orm['common.Asset']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'installations'", 'to': "orm['common.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        'common.devlog': {
            'Meta': {'ordering': "['-timestamp']", 'object_name': 'DevLog'},
            'asset': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dev_logs'", 'to': "orm['common.Asset']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'logs'", 'to': "orm['common.Device']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'}),
            'speed': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        'common.path': {
            'Meta': {'object_name': 'Path'},
            'default_stop_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'default_travel_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_route_json': ('django.db.models.fields.TextField', [], {}),
            'place1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paths'", 'to': "orm['common.Place']"}),
            'place2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'paths_reverse'", 'to': "orm['common.Place']"}),
            'total_distance': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True'})
        },
        'common.place': {
            'Meta': {'object_name': 'Place'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'polygon_json': ('django.db.models.fields.TextField', [], {})
        },
        'common.route': {
            'Meta': {'object_name': 'Route'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'total_distance': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'total_duration': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'common.routeleg': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'RouteLeg'},
            '_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Path']"}),
            'path_order': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'route': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'legs'", 'to': "orm['common.Route']"}),
            'stop_duration': ('django.db.models.fields.DateTimeField', [], {}),
            'travel_duration': ('django.db.models.fields.DateTimeField', [], {})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']