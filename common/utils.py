from collections import namedtuple
import datetime
from decimal import Decimal
import json
import logging
import math
import sys
import time

from django.core.cache import cache
from django.db.models import F, Q

from common.models import DevLog, Place, Path


logformat = '%(asctime)s %(name)s [%(levelname)s] %(message)s'
logging.basicConfig(level=logging.INFO, format=logformat)
logger = logging.getLogger('common.utils')


EARTH_RADIUS_MS = 6378137.0


def _calc_latlng_diff(lat1, lon1, lat2, lon2):
    lat1 = float(lat1)
    lat2 = float(lat2)
    lon1 = float(lon1)
    lon2 = float(lon2)

    lat1_rad = math.radians(lat1)
    lat2_rad = math.radians(lat2)
    d_lat_rad = math.radians(lat1 - lat2)
    d_lon_rad = math.radians(lon1 - lon2)

    a = (math.sin(d_lat_rad / 2) ** 2 +
         math.cos(lat1_rad) *
         math.cos(lat2_rad) *
         math.sin(d_lon_rad / 2) ** 2)
    #c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    c = 2 * math.asin(math.sqrt(a))
    return c


def distance_from_latlng(lat1, lon1, lat2, lon2):
    """
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
        c = 2 * atan2(sqrt(a), sqrt(1-a))
        d = R * c
    """
    c = _calc_latlng_diff(lat1, lon1, lat2, lon2)
    return c * EARTH_RADIUS_MS


LATLNG_DIFF_TO_METERS_APPROX = 107158.0


def distance_from_latlng_list(latlngs):
    diff_sum = sum(_calc_latlng_diff(*(p1 + p2))
                   for p1, p2 in zip(latlngs[:-1], latlngs[1:]))
    return diff_sum * EARTH_RADIUS_MS


def point_inside_polygon(x, y, poly):
    """determine if a point is inside a given polygon or not
        Polygon is a list of (x, y) pairs.
    """
    n = len(poly)
    inside = False

    p1x, p1y = poly[0]
    for i in range(n + 1):
        p2x, p2y = poly[i % n]
        if min(p1y, p2y) < y <= max(p1y, p2y):
            if x <= max(p1x, p2x):
                if p1y != p2y:
                    xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                if p1x == p2x or x <= xinters:
                    inside = not inside
        p1x, p1y = p2x, p2y

    return inside


def dist2(v, w):
    return (v[0] - w[0]) ** 2 + (v[1] - w[1]) ** 2


def point_nearest_line_segment_point(point, line_point1, line_point2):
    px, py = point
    vx, vy = line_point1
    wx, wy = line_point2
    l2 = dist2(line_point1, line_point2)
    if l2 == 0:
        return line_point1
    t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2
    if t < 0:
        return line_point1
    if t > 1:
        return line_point2
    return (vx + t * (wx - vx), vy + t * (wy - vy))


def point_normal_base(point, line_point1, line_point2):
    px, py = point
    vx, vy = line_point1
    wx, wy = line_point2
    l2 = dist2(line_point1, line_point2)
    if l2 == 0.0:
        return line_point1
    t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2
    if t < 0:
        return None
    if t > 1:
        return None
    return (vx + t * (wx - vx), vy + t * (wy - vy))


def _point_dist_from_line_segment_sqr(point, line_point1, line_point2):
    px, py = point
    vx, vy = line_point1
    wx, wy = line_point2
    l2 = dist2(line_point1, line_point2)
    if l2 == 0:
        return dist2(point, line_point1)
    t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2
    if t < 0:
        return dist2(point, line_point1)
    if t > 1:
        return dist2(point, line_point2)
    return dist2(point,  (vx + t * (wx - vx), vy + t * (wy - vy)))


def point_dist_from_line_segment(point, line_point1, line_point2):
    dist_2 = _point_dist_from_line_segment_sqr(point, line_point1, line_point2)
    return math.sqrt(dist_2)


def point_on_line_segment(point, line_point1, line_point2):
    dist_2 = _point_dist_from_line_segment_sqr(point, line_point1, line_point2)
    return dist_2 == 0.0


def point_inside_polygon_inclusive(x, y, poly):
    if point_inside_polygon(x, y, poly):
        return True

    for p1, p2 in zip(poly[:-1], poly[1:]) + [[poly[-1], poly[0]]]:
        if point_on_line_segment((x, y), p1, p2):
            return True


def point_dist_from_polygon(x, y, poly):
    """ Calculate the distance of a point from the nearest edge of a polygon.
        Requires the point to be outside of the polygon
    """
    min_dist2 = None
    if len(poly) < 3:
        msg = 'Invalid `poly`, number of points is less that three.'
        raise ValueError(msg)
    for p1, p2 in zip(poly, poly[1:] + [poly[0]]):
        dist2 = _point_dist_from_line_segment_sqr((x, y), p1, p2)
        if min_dist2 is None or dist2 < min_dist2:
            min_dist2 = dist2
    assert min_dist2 is not None
    return math.sqrt(min_dist2)


def get_place(dev_log):
    if dev_log.place is not None and dev_log.event != DevLog.EVENT_EXIT:
        return dev_log.place, dev_log.subplace
    return _get_place(dev_log.float_point)


def get_place_description(place, subplace, asset_state):
    if place is None:
        return ''
    place_name = place.get_stateful_name(asset_state)
    if subplace is not None:
        subplace_name = subplace.get_stateful_name(asset_state)
        return '%s, %s' % (subplace_name, place_name)
    return place_name


def get_all_places(_user):
    places = cache.get('all-places')
    if places is None:
        places = Place.objects.all()
        places = list(places)
        [p.polygon for p in places]     # load polygon json
        cache.set('all-places', places, 15)
    return places


def _get_place(point):
    lat, lng = point
    places = get_all_places(None)
    entered_places = []
    for place in places:
        if place.min_latitude <= lat <= place.max_latitude and \
                place.min_longitude <= lng <= place.max_longitude and \
                point_inside_polygon_inclusive(lat, lng, place.polygon):
            entered_places.append(place)
    if not entered_places:
        return None, None
    if len(entered_places) == 1:
        return entered_places[0], None

    smallest_area = None
    smallest_place = None
    largest_area = None
    largest_place = None
    for place in entered_places:
        lat_diff = place.max_latitude - place.min_latitude
        lng_diff = place.max_longitude - place.min_longitude
        area = lat_diff * lng_diff
        if smallest_area is None or area < smallest_area:
            smallest_place = place
            smallest_area = area
        if largest_area is None or area > largest_area:
            largest_place = place
            largest_area = area
    if largest_place == smallest_place:
        return smallest_place, None
    return largest_place, smallest_place


def get_parked_logs(logs_query, max_logs=0,
                    long_parking=30,
                    ignition_off_parking=2):
    long_parking = F('first_ts') + \
        datetime.timedelta(seconds=long_parking * 60)
    ignition_off_parking = F('first_ts') + \
        datetime.timedelta(seconds=ignition_off_parking * 60)
    criteria = (Q(timestamp__gte=long_parking) |
                Q(ignition=False,
                  timestamp__gte=ignition_off_parking,
                  place__isnull=False))
    parkings = logs_query.filter(criteria)
    if max_logs:
        parkings = parkings[:max_logs]
    return parkings


def group_points(points):
    points_dict = {p: set() for p in points}

    for point, neighbours in points_dict.items():
        neighbours.add(point)
        for p in points_dict.keys():
            if p != point:
                distance = distance_from_latlng(*(point + p))
                if distance < 50:
                    neighbours.add(p)

    groups = points_dict.values()

    i = 0
    ref_i = 0
    while ref_i < len(groups):
        if i != ref_i:
            ref_group = groups[ref_i]
            if ref_group & groups[i]:
                ref_group.update(groups.pop(i))
                i = ref_i
        i += 1
        if i >= len(groups):
            ref_i += 1
            i = ref_i

    return groups


def get_bounding_box(points, inflate=0):
    lats = [p[0] for p in points]
    lngs = [p[1] for p in points]
    min_lat = min(lats)
    max_lat = max(lats)
    min_lng = min(lngs)
    max_lng = max(lngs)
    lat_diff = (max_lat - min_lat)
    min_delta = Decimal('0.00010')
    if lat_diff < min_delta:
        min_lat -= (min_delta - lat_diff) / 2
        max_lat += (min_delta - lat_diff) / 2
    lng_diff = (max_lng - min_lng)
    if lng_diff < min_delta:
        min_lng -= (min_delta - lng_diff) / 2
        max_lng += (min_delta - lng_diff) / 2
    if inflate:
        min_lat -= Decimal(inflate)
        max_lat += Decimal(inflate)
        min_lng -= Decimal(inflate)
        max_lng += Decimal(inflate)
    return [(min_lat, min_lng), (min_lat, max_lng),
            (max_lat, max_lng), (max_lat, min_lng)]


def guess_route(start_place, curr_lat, curr_lng):
    ''' UNUSED '''
    if not start_place:
        return None

    paths = set()
    paths.update(start_place.paths.all())
    paths.update(start_place.paths_reverse.all())
    all_dest = {}
    for p in paths:
        if p.place1 != start_place:
            all_dest[p.place1] = p
        if p.place2 != start_place:
            all_dest[p.place2] = p

    best_dest = None
    best_dist = None

    for dest in all_dest.keys():
        dest_points = json.loads(dest.polygon_json)
        if dest_points:
            distance = distance_from_latlng(
                curr_lat, curr_lng, *dest_points[0])
            if best_dist is None or distance < best_dist:
                best_dist = distance
                best_dest = dest
    return (best_dest, all_dest[best_dest] if best_dest else None)


def get_path_length(path):
    route_data = json.loads(path.map_route_json)
    points = route_data['points'] if route_data else []
    return distance_from_latlng_list(points)


def get_ts(datetime_val):
    return time.mktime(datetime_val.timetuple()) + \
        datetime_val.microsecond / 1000000.0


strftimedelta = lambda format, delta: \
    time.strftime(format, time.gmtime(delta.total_seconds()))


def get_enter_log(log):
    enter_log = None
    if log.event == DevLog.EVENT_SUB_EXIT:
        enter_log = log.asset.dev_logs.filter(
            timestamp__lt=log.timestamp, event=DevLog.EVENT_SUB_ENTER,
            place=log.place, subplace=log.subplace)[:1]
    elif log.event == DevLog.EVENT_EXIT:
        enter_log = log.asset.dev_logs.filter(
            timestamp__lt=log.timestamp, event=DevLog.EVENT_ENTER,
            place=log.place)[:1]
    if enter_log:
        enter_log = enter_log[0]
    return enter_log


def short_notification(log, enter_log=None):
    event_descr = {
        DevLog.EVENT_EXIT: 'Truck %s has left %s',
        DevLog.EVENT_ENTER: 'Truck %s has arrived at %s',
        DevLog.EVENT_SUB_EXIT: 'Truck %s has moved out of %s',
        DevLog.EVENT_SUB_ENTER: 'Truck %s has entered %s',
    }
    if not log.event in event_descr:
        return None
    place_descr = log.place_description
    notification = event_descr[log.event] % (log.asset.name, place_descr)
    if log.event in [DevLog.EVENT_EXIT, DevLog.EVENT_SUB_EXIT]:
        if enter_log is None:
            enter_log = get_enter_log(log)
        if enter_log:
            enter_ts = (enter_log.first_ts or enter_log.timestamp)
            exit_ts = (log.first_ts or log.timestamp)
            delta = exit_ts - enter_ts
            format_str = '%H hours %M minutes'
            if delta.total_seconds() < 3600:
                format_str = '%M minutes %S seconds'
            notification += ' after %s' % strftimedelta(format_str, delta)
    return notification


def fmean(values):
    return math.fsum(values) / len(values)


def fstdevp(values):
    mean = fmean(values)
    variance = math.fsum((x - mean) ** 2 for x in values) / len(values)
    return variance ** 0.5


def ftrunc_mean(values):
    mean = fmean(values)
    stdevp = fstdevp(values)
    trimhi = mean + stdevp
    trimlo = mean - stdevp
    sample = [max(min(x, trimhi), trimlo) for x in values]
    return fmean(sample)


def get_min_point(route, p):
    ds = [dist2(x, p) for x in route]
    min_dist2 = min(ds)
    min_i = ds.index(min_dist2)
    min_p = route[min_i]
    dist2_prev = None
    dist2_next = None
    if min_i == 0:
        np = point_nearest_line_segment_point(p, min_p, route[min_i + 1])
        d2 = dist2(p, np)
        return 1, np, d2
    if min_i + 1 >= len(route):
        np = point_nearest_line_segment_point(p, min_p, route[min_i - 1])
        d2 = dist2(p, np)
        return (len(route) - 1, np, d2)
    np_prev = point_nearest_line_segment_point(p, min_p, route[min_i - 1])
    np_next = point_nearest_line_segment_point(p, min_p, route[min_i + 1])
    dist2_prev = dist2(p, np_prev)
    dist2_next = dist2(p, np_next)
    if dist2_prev < dist2_next:
        d2 = dist2_prev
        np = np_prev
    else:
        d2 = dist2_next
        np = np_next
    return min_i, np, d2


def _get_path_data(place):
    paths = Path.objects.filter(Q(place1_id=place.pk) | Q(place2_id=place.pk))
    route_data = {path.pk: json.loads(path.map_route_json) for path in paths}
    points = {}
    branches = {}
    for path in paths:
        data = route_data[path.pk]
        points[path.pk] = data['points']
        if path.place2_id == place.pk:
            points[path.pk].reverse()
        if path.place1_id == place.pk:
            branches[path.pk] = data['place1_branches']
        else:
            branches[path.pk] = data['place2_branches']
    return paths, points, branches


CachedTraceData = namedtuple(
    'CachedTraceData', ('last_i, track, current_pk, start_ts, end_ts,'
                        ' origin_place_pk, start_place violations'))


def get_trace(asset, latlngs, logs, start_place=None, trip_num=None):
    #return latlngs, None, None
    if start_place is None:
        raise NotImplementedError
    if not logs:
        track = latlngs
        route = dist = None
        place = None, None
        violations = []
        return track, route, violations, place, dist
    origin_place = start_place
    start_ts = logs[0].timestamp
    paths = None
    points = None
    violations = []
    #branches = None

    cached = cache.get('TRACE:%d:%s' % (asset.pk, trip_num))
    if cached:
        cached = CachedTraceData(*cached)
        # (last_i, track, current_pk, start_ts, end_ts,
        #  origin_place_pk, start_place, violations)
        if cached.origin_place_pk == origin_place.pk and \
                cached.start_ts == logs[0].timestamp:
            _logs = list(logs)
            for i, log in enumerate(_logs):
                if log.timestamp == cached.end_ts:
                    logs = _logs[i + 1:]
                    break
            else:
                cached = None
        else:
            cached = None
        if cached is not None:
            start_place = cached.start_place
            last_i = cached.last_i
            track = cached.track
            current_pk = cached.current_pk
            violations = cached.violations
            logger.info('Cached Hit: asset:%d' % asset.pk)

    logger.debug('Path data fetch start')
    paths, points, branches = _get_path_data(start_place)
    logger.debug('Path data fetched')
    if not paths:
        track = latlngs
        route = dist = None
        place = None, None
        return track, route, violations, place, dist

    offtrack_threshold = (300 / LATLNG_DIFF_TO_METERS_APPROX) ** 2
    proximity_threshold = (100 / LATLNG_DIFF_TO_METERS_APPROX) ** 2
    min_i = None
    end_ts = None
    if cached is None:
        track = []
        last_i = None
        current_pk = paths[0].pk

    def fix_last_point(p, min_i, ps, track):
        min_p = ps[min_i]
        prev_seg_dist2 = next_seg_dist2 = sys.maxint
        prev_p = next_p = None
        if min_i > 0:
            prev_p = ps[min_i - 1]
            prev_seg_dist2 = _point_dist_from_line_segment_sqr(
                p, min_p, prev_p)
        if min_i + 1 < len(ps):
            next_p = ps[min_i + 1]
            next_seg_dist2 = _point_dist_from_line_segment_sqr(
                p, min_p, next_p)
        other_p = next_p if prev_seg_dist2 > next_seg_dist2 else prev_p
        if other_p is not None:
            p = point_nearest_line_segment_point(p, min_p, other_p)
            if not track:
                logger.error('len(track) == 0')
            elif track[-1] != min_p:
                logger.error('track[-1] != min_p')
            elif len(track) >= 2 and track[-2] == other_p:
                track[-1] = p
            else:
                track.append(p)

    for l in logs:
        p = l.float_point
        if last_i == len(points[current_pk]) - 1:
            #path = paths[current_pk]
            #if path.place1 != start_place:
            #    start_place = path.place1
            #else:
            #    start_place = path.place2
            paths, points, branches = _get_path_data(start_place)
        min_i, min_p, min_dist2 = get_min_point(points[current_pk], p)
        if min_dist2 > proximity_threshold:
            new_pk = None
            for pk in points:
                if pk == current_pk:
                    continue
                i, mp, d2 = get_min_point(points[pk], p)
                if d2 < min_dist2:
                    min_i = i
                    min_p = mp
                    min_dist2 = d2
                    new_pk = pk
            if min_i > 0:
                last_i = min_i - 1
            else:
                last_i = None
            if new_pk is not None:
                current_pk = new_pk
                if len(track) > 2:
                    p1, p2 = track[-2:]
                    if point_normal_base(min_p, p1, p2):
                        last_i = min_i
        if l.violations and l.num_end_events > 0:
            log_violations = l.violations.split(',')
            if str(DevLog.VIOLAT_PARK) in log_violations:
                violations.append([
                    min_p if min_dist2 <= offtrack_threshold else p,
                    get_ts(l.first_ts or l.timestamp),
                    l.duration,
                    DevLog.VIOLAT_PARK,
                ])
            elif str(DevLog.VIOLAT_OVERSPEED) in log_violations:
                violations.append([
                    min_p if min_dist2 <= offtrack_threshold else p,
                    get_ts(l.first_ts or l.timestamp),
                    l.speed,
                    DevLog.VIOLAT_OVERSPEED,
                ])
        if min_dist2 > offtrack_threshold:
            track.append(p)
            last_i = None
        else:
            if last_i == min_i:
                continue
            if last_i is None:
                last_i = min_i - 1
            ps = points[current_pk]
            if min_i < last_i:
                e = ps[min_i:last_i]
                e.reverse()
            else:
                e = ps[last_i + 1:min_i + 1]
            track += e
            last_i = min_i
        end_ts = l.timestamp
    if last_i is not None and min_i is not None and latlngs:
        ps = points[current_pk]
        fix_last_point(latlngs[-1], min_i, ps, track)

    # add to cached
    # (last_i, track, current_pk, start_ts, end_ts,
    #  origin_place_pk, start_place)
    if end_ts is None:
        if logs:
            end_ts = logs[-1].timestamp
        elif track and isinstance(cached, CachedTraceData):
            end_ts = cached.end_ts
    if end_ts:
        cached = CachedTraceData(
            last_i, track, current_pk, start_ts, end_ts,
            origin_place.pk, start_place, violations)
        cache.set('TRACE:%d' % asset.pk, tuple(cached))

    def first_place(trip_part):
        for i, p in enumerate(trip_part):
            place = _get_place(p)
            if place[0] is not None:
                return place, distance_from_latlng_list(trip_part[:i + 1])
    if last_i is not None:
        route = points[current_pk]
        branch = len(route) - 1
        for b in sorted(branches[current_pk], reverse=True):
            if b < last_i:
                break
            branch = b
        last_place_data = first_place(route[:last_i][::-1])
        next_place_data = first_place(route[last_i:branch + 1])
        if last_place_data is None:
            if next_place_data is not None:
                place, dist = next_place_data
                dist = 0 - dist
            else:
                place, dist = (None, None), None
        elif next_place_data is None:
            if last_place_data is not None:
                place, dist = last_place_data
            else:
                place, dist = (None, None), None
        else:
            last_place, last_place_dist = last_place_data
            next_place, next_place_dist = next_place_data
            if next_place_dist > last_place_dist:
                place = last_place
                dist = last_place_dist
            else:
                place = next_place
                dist = 0 - next_place_dist
        #place = place_description(*place)
    else:
        route = dist = None
        place = (None, None)
    return track, route, violations, place, dist
