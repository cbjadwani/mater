function PlacesCtrl($scope) {
  $scope.places = [];
  $scope.editing = false;
  $scope.place_editing = null;
  $scope.editing_on_map = false;
  $scope.load_data = function (zommToAll) {
    $.getJSON(
        mater.urls.get_locations,
        function (data) {
          var i;
          if (data.ok) {
            $scope.$apply(function () {
              $scope.places.map(function(place) {
                mater.settings_map.deletePolygon(place.pk)
              });
              $scope.places = data.data;
              $scope.places.map(function(place) {
                mater.settings_map.addPolygon(place.pk, place.fields.polygon_json, zommToAll && true);
              });
            });
          }
        }
    );
  };
  $scope.generateSuggested = function () {
    var jqxhr = $.getJSON(
      mater.urls.suggest_places,
      function (data) {
        var pk, pks, i;
        if (data.ok) {
          var added = data.added;
          added.map(function(place) {
            mater.settings_map.addPolygon(place.pk, place.fields.polygon_json);
          });
          pks = Object.keys(data.parkings);
          for (var i = 0; i < pks.length; i++) {
            pk = pks[i];
            data.parkings[pk].map(function (p) {
              p.when = moment.unix(p.when).format('DD/MM/YY hh:mmA');
              p.duration = formatSeconds(p.duration);
            });
          }
          if (added.length) {
            $scope.$apply(function () {
              $scope.places = $scope.places.concat(added);
              $scope.parkings = data.parkings;
              $scope.show_suggested = true;
              alert('Places generated.')
            });
          } else {
            alert('There are no new suggested places.');
          }
        } else {
          generate_error_alert();
        }
      }
    )
    .error(generate_error_alert);
  };
  $scope.hoverTimeOutID = null;

  $scope.load_data();

  $scope.isClean = function () {
    return ($scope.place_editing &&
        !$scope.editing_on_map &&
        $scope.form_place_name == $scope.place_editing.fields.name &&
        $scope.form_place_category == $scope.place_editing.fields.category);
  };
  $scope.form_submit = function () {
    if ($scope.places_form.$invalid) return false;
    var place = $scope.place_editing;
    place.fields.name = $scope.form_place_name;
    place.fields.category = $scope.form_place_category;
    place.fields.icon_name = $scope.form_place_icon || null;
    place.fields.polygon_json = mater.settings_map.getPolygonJSON(place.pk || 0);

    var ajax_data = {}, orig_pk = place.pk;
    if (place.pk < 0) {
      place.pk = null;
    }
    ajax_data.command = place.pk ? 'modify' : 'add';
    ajax_data.json = JSON.stringify([place]);
    var jqxhr = $.getJSON(
        mater.urls.put_locations,
        ajax_data,
        function (data) {
          if (!data.ok) {
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            $scope.$apply(function () {
              if ($scope.editing_on_map) {
                mater.settings_map.stopEditingPolygon(orig_pk);
                $scope.editing_on_map = false;
              }
              if (ajax_data.command == 'add') {
                if (!(orig_pk < 0)) {
                  $scope.places.push(place);
                }
                place.pk = data.added[0];
                mater.settings_map.deletePolygon(orig_pk);
                mater.settings_map.addPolygon(place.pk, place.fields.polygon_json, false);
              }
              $scope.place_editing = null;
              $scope.editing = false;
            });
            $.bootstrapGrowl('Data saved', {type: 'success', align: 'center'});
          }
        }
    ).error(function () {
      $.bootstrapGrowl('Error saving data, please try again',
        {type: 'error', align: 'center'});
      if (!place.pk) {
        place.pk = orig_pk;
      }
    });
  };
  $scope.delete_place = function (place) {
    var place_descr = place.fields.name + ' (' + $scope.get_place_category_description(place.fields.category) + ')';
    var msg = 'Delete "' + place_descr + '"?\n\nAll routes connecting this place will also get deleted.';
    if (!confirm(msg)) { return false; }

    if (place.pk < 0) {
      var places = $scope.places.filter(function (p) {
        return (p.pk != place.pk);
      });
      delete $scope.parkings[place.pk];
      $scope.places = places;
      return;
    }
    var ajax_data = {};
    ajax_data.command = 'delete';
    ajax_data.json = JSON.stringify([place]);
    var jqxhr = $.getJSON(
        mater.urls.put_locations,
        ajax_data,
        function (data) {
          if (!data.ok) {
            alert('Error saving data');
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            mater.settings_map.deletePolygon(place.pk);
            $scope.$apply('load_data(false)');
            var routes_scope = angular.element('#routes').scope();
            routes_scope.$apply('load_data()');
            $.bootstrapGrowl(place_descr + ' deleted',
              {type: 'success', align: 'center'});
          }
        }
    ).error(function () {
      $.bootstrapGrowl('Error saving data, please try again',
        {type: 'error', align: 'center'});
    });
  };
  $scope.get_place_category_description = get_place_category_description;
  $scope.start_editing = function (place) {
    $scope.form_place_name = place.fields.name;
    $scope.form_place_category = place.fields.category;
    $scope.form_place_icon = place.fields.icon_name || 'None';
    $scope.editing = true;
    $scope.place_editing = place;
    mater.settings_map.zoomPolygon(place.pk || 0);
  };
  $scope.create_new = function () {
    var place = {pk: null, model: 'common.place', fields: {}};
    place.fields.name = '';
    place.fields.category = '';
    place.fields.icon_name = 'None';
    place.fields.polygon_json = mater.settings_map.addStartingPolygon();
    mater.settings_map.highlightPolygon(0);
    $scope.start_editing(place);
  };
  $scope.stop_editing = function () {
    var pk = $scope.place_editing.pk || 0;
    mater.settings_map.unhighlightPolygon(pk);
    $scope.editing = false;
    $scope.place_editing = null;
    if ($scope.editing_on_map) {
      mater.settings_map.cancelEditingPolygon();
      $scope.editing_on_map = false;
    }
    if (pk == 0) {
      mater.settings_map.deletePolygon(0);
    }
  };
  $scope.highlightPolygon = function (place) {
    clearTimeout($scope.hoverTimeOutID);
    $scope.hoverTimeOutID = setTimeout(function () {
        mater.settings_map.highlightPolygon(place.pk);
      },
      333
    );
  };
  $scope.unhighlightPolygon = function (place) {
    clearTimeout($scope.hoverTimeOutID);
    if (!angular.equals(place, $scope.place_editing)) {
      mater.settings_map.unhighlightPolygon(place.pk);
    }
  };
  $scope.start_editing_map = function () {
    $scope.editing_on_map = true;
    mater.settings_map.startEditingPolygon($scope.place_editing.pk || 0);
  };
  $scope.suggestedFilter = function (place) {
    if (!$scope.show_suggested ||
        typeof $scope.parkings[place.pk] != 'undefined') {
      return true;
    }
    return false;
  };
  $scope.closeSuggestedPlaces = function () {
    var places;
    $scope.show_suggested = false;
    places = $scope.places.filter(function (place) {
      if (typeof $scope.parkings[place.pk] == 'undefined') {
        return true;
      }
      return false;
    });
    delete $scope.parkings;
    $scope.places = places;
  };
};
