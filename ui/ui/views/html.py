import simplejson as json

from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize

from annoying.decorators import render_to

from common.models import Place, UserProfile
from ui.views.main import get_asset_stats, get_messages


@login_required
@render_to('index.html')
def home(request):
    user = request.user
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    user_assets = serialize('json', user.assets.all())
    asset_stats = get_asset_stats(user)
    for asset_json in json.loads(user_assets):
        pk = asset_json['pk']
        if pk in asset_stats:
            asset_stats[pk]['fields'] = asset_json['fields']
    messages = json.dumps(get_messages(request.user))
    return {
        'user_assets': dict(user.assets.values_list('pk', 'name')),
        'user_assets_json': json.dumps(asset_stats),
        'selected_asset_id': profile.selected_asset_id,
        'messages': messages,
        'role': profile.role
    }


@login_required
@render_to('livemap.html')
def livemap(request):
    return {}


@login_required
@render_to('settings.html')
def settings(request):
    user = request.user
    try:
        profile = user.profile
    except UserProfile.DoesNotExist:
        profile = UserProfile.objects.create(user=user)
    return {'Place': Place, 'role': profile.role}
