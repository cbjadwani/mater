import json

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import signals as auth_signals
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.utils.translation import ugettext as _


class Device(models.Model):
    """ The data collection device.
    """
    imei = models.CharField(max_length=len('AA-BBBBBB-CCCCCC-EE'), blank=True)
    token = models.CharField(max_length=32, db_index=True, blank=True)
    command = models.TextField(null=True, blank=True)

    def str_id(self):
        return '%05X' % self.id

    @property
    def current_installation(self):
        if not hasattr(self, '_current_installation_cache'):
            try:
                installation = self.installations.latest()
            except DeviceInstallationLog.DoesNotExist:
                installation = None
            self._current_installation_cache = installation

        return self._current_installation_cache

    @property
    def current_asset(self):
        if self.current_installation:
            return self.current_installation.asset
        return None

    def __str__(self):
        return 'Device(%r, %r)' % (self.str_id(), self.imei)


class Asset(models.Model):
    """ The tracked asset.
    """
    STATE_NONE = ''
    STATE_LOADED = 'LOADED'
    name = models.CharField(
        max_length=50, unique=True, verbose_name=_('Asset Name'))
    customer = models.ForeignKey('Customer', null=True, blank=True)
    users = models.ManyToManyField('auth.User', related_name='assets')
    type = models.ForeignKey('AssetType', null=True)
    speed_threshold = models.PositiveIntegerField('Speed Threshold',
        default=55)

    @property
    def current_installation(self):
        if not hasattr(self, '_current_installation_cache'):
            try:
                installation = self.installations.latest()
            except DeviceInstallationLog.DoesNotExist:
                installation = None
            self._current_installation_cache = installation

        return self._current_installation_cache

    @property
    def current_device(self):
        if self.current_installation:
            return self.current_installation.device
        return None

    def install_device(self, device):
        if self.current_device != device:
            new_installation = DeviceInstallationLog(device=device, asset=self)
            self.installations.add(new_installation)


class DeviceInstallationLog(models.Model):
    """ Log of which asset has which device
    """
    class Meta:
        get_latest_by = 'timestamp'

    device = models.ForeignKey(Device, related_name='installations', null=True)
    asset = models.ForeignKey(Asset, related_name='installations', null=True)
    timestamp = models.DateTimeField(
        db_index=True, auto_now_add=True, editable=False)


class DevLog(models.Model):
    """ The data collected.
    """
    class Meta:
        get_latest_by = 'timestamp'
        ordering = ['-timestamp']

    EVENT_ENTER = 'ENTER'
    EVENT_EXIT = 'EXIT'
    EVENT_SUB_ENTER = 'SUBENTER'
    EVENT_SUB_EXIT = 'SUBEXIT'
    EVENT_CHOICES = (
        (EVENT_ENTER, 'Enter'),
        (EVENT_EXIT, 'Exit'),
        (EVENT_SUB_ENTER, 'SUBENTER'),
        (EVENT_SUB_EXIT, 'SUBEXIT'),
    )
    VIOLAT_PARK = 1
    VIOLAT_OVERSPEED = 2
    #VIOLAT_OFFROUTE = 3
    VIOLAT_CHOICES = (
        (VIOLAT_PARK, 'Parking Violation'),
        (VIOLAT_OVERSPEED, 'Speed Violation'),
    )

    device = models.ForeignKey(Device, related_name='logs')
    asset = models.ForeignKey(Asset, related_name='dev_logs')
    asset_state = models.CharField(max_length=30, null=True, blank=True)
    timestamp = models.DateTimeField(db_index=True, editable=False)
    first_ts = models.DateTimeField(
        db_index=True, editable=False, null=True, blank=True)
    latitude = models.DecimalField(
        max_digits=7, decimal_places=5, editable=False)
    longitude = models.DecimalField(
        max_digits=8, decimal_places=5, editable=False)
    speed = models.PositiveIntegerField()
    diff_time = models.PositiveIntegerField(null=True, blank=True)
    diff_distance = models.PositiveIntegerField(null=True, blank=True)
    place = models.ForeignKey(
        'Place', null=True, blank=True, on_delete=models.SET_NULL)
    subplace = models.ForeignKey(
        'Place', null=True, blank=True, on_delete=models.SET_NULL,
        related_name='subplace_logs')
    event = models.CharField(max_length=10, choices=EVENT_CHOICES, blank=True)
    violations = models.CommaSeparatedIntegerField(
        max_length=20, choices=VIOLAT_CHOICES, null=True, blank=True)
    ignition = models.NullBooleanField(null=True)

    def get_ts(self):
        from common import utils
        return utils.get_ts(self.timestamp)

    @property
    def float_point(self):
        return float(self.latitude), float(self.longitude)

    @property
    def duration(self):
        if not self.first_ts:
            return 0
        return (self.timestamp - self.first_ts).total_seconds()

    def is_inside_place(self, place):
        from common import utils

        if not place:
            return False
        lat, lng = self.float_point
        poly = place.polygon
        return utils.point_inside_polygon_inclusive(lat, lng, poly)

    def approx_dist_from_place(self, place):
        from common import utils
        if self.is_inside_place(place):
            return 0
        poly = place.polygon
        lat, lng = self.float_point
        dist = utils.point_dist_from_polygon(lat, lng, poly)
        return dist * utils.LATLNG_DIFF_TO_METERS_APPROX

    @property
    def place_description(self):
        from common import utils
        return utils.get_place_description(
            self.place, self.subplace, self.asset_state)


class Place(models.Model):
    """ A stop, parking zone or a no-go zone defined by a polygon.
    """
    CAT_STOP = 'STOP'
    CAT_DEPOT = 'DEPOT'
    CAT_NOGO = 'NOGO'
    CAT_LM = 'LM'
    CAT_SF = 'SAFE'
    CAT_DEPOT_PARK = 'DEPOT_PARK'
    CAT_DEPOT_BILL = 'DEPOT_BILL'
    CAT_CITY = 'CITY'
    CATEGORY_CHOICES = (
        (CAT_STOP, 'Retail Outlet'),
        (CAT_DEPOT, 'Supply Point'),
        (CAT_NOGO, 'Black Listed Area'),
        (CAT_LM, 'Land Mark'),
        (CAT_SF, 'Safe Area'),
        (CAT_DEPOT_PARK, 'Supply Point Parking'),
        (CAT_DEPOT_BILL, 'Supply Point Billing'),
        (CAT_CITY, 'City Limits'),
    )
    ATTR_LOADING = 'LOADING'
    ATTR_UNLOADING = 'UNLOADING'
    ATTR_NOPLACE = '*NO PLACE*'
    PLACE_ICON_NONE = None
    PLACE_ICON_ESSAR = 'essar.png'
    PLACE_ICON_HPCL = 'hpcl.png'
    PLACE_ICON_MAINT = 'maintenance.png'
    PLACE_ICON_RESTR = 'restaurant.png'
    PLACE_ICON_CHOICES = (
        (PLACE_ICON_NONE,  '-----'),
        (PLACE_ICON_ESSAR, 'Essar'),
        (PLACE_ICON_HPCL, 'HP Pump'),
        (PLACE_ICON_MAINT, 'Maintenance'),
        (PLACE_ICON_RESTR, 'Restaurant'),
    )

    name = models.CharField(max_length=50, db_index=True, unique=True)
    map_name = models.CharField(max_length=100, blank=True)
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES,
                                db_index=True)
    polygon_json = models.TextField()  # bounding points
    min_latitude = models.DecimalField(
        max_digits=7, decimal_places=5, db_index=True, editable=False)
    max_latitude = models.DecimalField(
        max_digits=7, decimal_places=5, db_index=True, editable=False)
    min_longitude = models.DecimalField(
        max_digits=8, decimal_places=5, db_index=True, editable=False)
    max_longitude = models.DecimalField(
        max_digits=8, decimal_places=5, db_index=True, editable=False)
    attrs = models.TextField(null=True, blank=True)
    stateful_names = models.TextField(null=True, blank=True)
    icon_name = models.CharField(
        max_length=256, choices=PLACE_ICON_CHOICES, null=True, blank=True)
    #customer = models.ForeignKey('Customer')

    def __str__(self):
        return 'Place(%s, %s)' % (self.name, self.get_category_display())

    @property
    def polygon(self):
        if not hasattr(self, '_polygon'):
            self._polygon = json.loads(self.polygon_json)
        return self._polygon

    def get_superplace(self):
        if not hasattr(self, '_superplace'):
            super_place = Place.objects.exclude(pk=self.pk).filter(
                max_latitude__gte=self.max_latitude,
                max_longitude__gte=self.max_longitude,
                min_latitude__lte=self.min_latitude,
                min_longitude__lte=self.min_longitude)
            self._superplace = super_place[0] if super_place else None
        return self._superplace

    def is_subplace(self):
        return self.get_superplace() is not None

    def get_fullname(self):
        superplace = self.get_superplace()
        if superplace is not None:
            return '%s, %s' % (self.name, superplace.name)
        return self.name

    @property
    def state_dict(self):
        if not hasattr(self, '_state_dict'):
            self._state_dict = json.loads(self.stateful_names or '{}')
        assert isinstance(self._state_dict, dict)
        return self._state_dict

    def get_stateful_name(self, asset_state):
        return self.state_dict.get(asset_state, self.name)

    def get_icon_url(self):
        if not self.icon_name or self.icon_name == 'None':
            return None
        return settings.STATIC_URL + ('img/place-icons/%s' % self.icon_name)

    def get_centroid(self):
        points = self.polygon
        lat = sum(p[0] for p in points) / len(points)
        lng = sum(p[1] for p in points) / len(points)
        return lat, lng


class AssetStateTransition(models.Model):
    """  Rules to specify asset state transitions and place names
    """
    asset_type = models.ForeignKey('AssetType', related_name='state_rules')
    place_attr = models.CharField(max_length=30)
    initial_state = models.CharField(max_length=30, null=True, blank=True)
    final_state = models.CharField(max_length=30, null=True, blank=True)
    trigger_duration = models.PositiveIntegerField()


class AssetType(models.Model):
    name = models.CharField(max_length=50)
    prefix = models.CharField(max_length=50)


class Path(models.Model):
    """ A polyline path that assets must use to go from a place to another.
    """
    place1 = models.ForeignKey(Place, related_name='paths')
    place2 = models.ForeignKey(Place, related_name='paths_reverse')
    map_route_json = models.TextField()  # PolyLine array
    total_distance = models.PositiveIntegerField(null=True, editable=False)
    default_travel_duration = models.DateTimeField()
    default_stop_duration = models.DateTimeField()

    def __str__(self):
        return '%s<->%s' % (self.place1, self.place2)


class Route(models.Model):
    """ Sequence of paths with expected travel duration and specifying
        whether there is a stop at each point.
    """
    name = models.CharField(max_length=50)
    total_distance = models.PositiveIntegerField()
    total_duration = models.PositiveIntegerField()


class RouteLeg(models.Model):
    """ A single leg of a Route
    """
    class Meta:
        order_with_respect_to = 'route'

    route = models.ForeignKey(Route, related_name='legs')
    path = models.ForeignKey(Path)
    path_order = models.CharField(
        max_length=1, choices=(('F', 'Forward'), ('R', 'Reverse')))
    travel_duration = models.DateTimeField()
    stop_duration = models.DateTimeField()


class EventLog(models.Model):
    class Meta:
        get_latest_by = 'start_log_ts'
        ordering = ['-start_log_ts']

    EVENT_PARKED = 1
    EVENT_MOBILE = 2
    EVENT_LOADED = 3
    EVENT_UNLOADED = 4
    EVENT_VIOLATION = 5
    EVENT_DICT = {
        EVENT_PARKED: 'Parked',
        EVENT_MOBILE: 'Mobile',
        EVENT_LOADED: 'Supply Point',
        EVENT_UNLOADED: 'Decanting',
        EVENT_VIOLATION: 'Violation',
    }
    asset = models.ForeignKey('Asset')
    start_log_ts = models.DateTimeField(db_index=True)
    start_log = models.ForeignKey('DevLog', related_name='start_event_logs')
    end_log = models.ForeignKey('DevLog', related_name='end_event_logs')
    distance = models.PositiveIntegerField(default=0)

    @property
    def duration(self):
        ts1 = self.start_log.first_ts or self.start_log.timestamp
        ts2 = self.end_log.first_ts or self.end_log.timestamp
        duration = (ts2 - ts1).total_seconds()
        assert duration > 0, str(self.pk)
        return duration

    @property
    def event(self):
        if not hasattr(self, '_event'):
            start_log = self.start_log
            end_log = self.end_log
            if start_log.asset_state == Asset.STATE_NONE and \
                    end_log.asset_state == Asset.STATE_LOADED:
                self._event = self.EVENT_LOADED
            elif start_log.asset_state == Asset.STATE_LOADED and \
                    end_log.asset_state == Asset.STATE_NONE:
                self._event = self.EVENT_UNLOADED
            elif start_log.event == [DevLog.EVENT_EXIT, DevLog.EVENT_SUB_EXIT]:
                self._event = self.EVENT_MOBILE
            elif start_log.violations:
                self._event = self.EVENT_VIOLATION
            elif end_log.place == start_log.place and \
                    end_log.subplace == start_log.subplace and \
                    (self.distance * 3.6 // self.duration <= 5 and
                        self.distance < 1000):
                self._event = self.EVENT_PARKED
            else:
                self._event = self.EVENT_MOBILE
        return self._event

    @property
    def event_description(self):
        return self.EVENT_DICT.get(self.event, '')

    @property
    def event_details(self):
        if self.event == self.EVENT_PARKED:
            return self.start_log.place_description or ''
        elif self.event == self.EVENT_MOBILE:
            descr = []
            if self.start_log.place:
                descr.append('From %s' % self.start_log.place_description)
            if self.end_log.place:
                descr.append('To %s' % self.end_log.place_description)
            return ' '.join(descr)
        elif self.event == self.EVENT_LOADED:
            return self.start_log.place_description
        elif self.event == self.EVENT_UNLOADED:
            return self.start_log.place_description
        elif self.event == self.EVENT_VIOLATION:
            if str(DevLog.VIOLAT_PARK) in self.start_log.violations:
                descr = 'Parking Violation'
                if self.start_log.place:
                    descr += ' (%s)' % self.start_log.place_description
                return descr
            elif str(DevLog.VIOLAT_OVERSPEED) in self.start_log.violations:
                return 'Speed Violation (%s km/h)' % self.start_log.speed
        return '(ERROR)'

    @property
    def description(self):
        start_log = self.start_log
        end_log = self.end_log
        assert start_log.timestamp <= end_log.timestamp, str(self.pk)

        def _en_route():
            descr = 'Travelled'
            if start_log.place is not None:
                descr += ' from %s' % start_log.place_description
            if end_log.place is not None:
                descr += ' to %s' % end_log.place_description
            return descr

        def _moved_within():
            assert start_log.place is not None, str(self.pk)
            assert start_log.place == end_log.place, str(self.pk)
            if start_log.subplace == end_log.subplace:
                return 'Moved within %s' % start_log.place_description
            descr = 'Moved'
            if start_log.subplace is not None:
                sp = start_log.subplace
                descr += ' from %s' % sp.get_stateful_name(
                    start_log.asset_state)
            if end_log.subplace is not None:
                sp = end_log.subplace
                descr += ' to %s' % sp.get_stateful_name(
                    start_log.asset_state)
            place = start_log.place
            descr += ' inside %s' % place.get_stateful_name(
                start_log.asset_state)
            return descr

        if start_log.event == DevLog.EVENT_EXIT:
            return _en_route()
        elif start_log.event == DevLog.EVENT_SUB_EXIT:
            if start_log.place == end_log.place:
                return _moved_within()
            else:
                return _en_route()
        elif start_log.event == DevLog.EVENT_ENTER:
            if end_log.place == start_log.place:
                if self.distance / self.duration <= 5:
                    return 'Parked at %s' % start_log.place_description
                else:
                    return _moved_within()
            else:
                return _en_route()
        elif start_log.event == DevLog.EVENT_SUB_ENTER:
            if end_log.subplace == start_log.subplace and \
                    self.distance / self.duration <= 5:
                return 'Parked at %s' % start_log.place_description
            elif start_log.place == end_log.place:
                return _moved_within()
            else:
                return _en_route()
        elif str(DevLog.VIOLAT_PARK) in start_log.violations:
            return 'Parking Violation'
        elif str(DevLog.VIOLAT_OVERSPEED) in start_log.violations:
            return 'Speed Violation'
        else:
            if start_log.place == end_log.place and \
                    start_log.place is not None:
                return _moved_within()
            else:
                return _en_route()

        return '(ERROR)'


class Message(models.Model):
    """
    User messages
    """
    user = models.ForeignKey('auth.User')
    log = models.ForeignKey(DevLog)
    delivered = models.BooleanField(default=False)


class Customer(models.Model):
    name = models.CharField(max_length=150, unique=True, db_index=True)


class UserProfile(models.Model):
    ROLE_OWNER = 'OWNER'
    ROLE_OTHER = 'OTHER'
    ROLE_CHOICES = [
        (ROLE_OWNER, 'Owner'),
        (ROLE_OTHER, 'Other')
    ]

    user = models.OneToOneField(User, primary_key=True,
                                related_name='profile')
    customer = models.ForeignKey('Customer', related_name='users',
                                 null=True, blank=True)
    role = models.CharField(max_length=10, choices=ROLE_CHOICES,
                            default=ROLE_OTHER)
    phone = models.CharField(max_length=20, blank=True)
    selected_asset = models.ForeignKey(Asset, null=True, default=None)
    current_session = models.TextField(blank=True, null=True)


@receiver(signals.post_save, sender=User)
def create_new_user_profile(instance, **kwargs):
    created = kwargs.get('created', False)
    if created:
        UserProfile.objects.create(user=instance)


@receiver(signals.pre_delete, sender=Place)
def place_pre_delete(instance, **kwargs):
    instance.devlog_set.update(place=None, event='')


@receiver(signals.pre_save, sender=Place)
def update_place_min_max(instance, **kwargs):
    points = json.loads(instance.polygon_json)
    lats = [p[0] for p in points]
    lngs = [p[1] for p in points]
    created = kwargs.get('created', False)
    if created and instance.min_latitude:
        pass
    else:
        instance.min_latitude = min(lats)
        instance.max_latitude = max(lats)
        instance.min_longitude = min(lngs)
        instance.max_longitude = max(lngs)


@receiver(auth_signals.user_logged_in)
def single_session_backend_logged_in(sender, request, user, **kwargs):
    if user is not None:
        session_key = request.session.session_key
        print 'session_key', session_key
        profile = user.profile
        UserProfile.objects.filter(pk=profile.pk).update(
            current_session=session_key)


@receiver(auth_signals.user_logged_out)
def single_session_backend_logged_out(sender, request, user, **kwargs):
    if hasattr(user, 'is_authenticated') and user.is_authenticated():
        profile = user.profile
        UserProfile.objects.filter(pk=profile.pk).update(current_session=None)
