import sys

import settings
this_module = sys.modules[__name__]
for param in dir(settings):
    if not '__' in param:
        setattr(this_module, param, getattr(settings, param))

DEBUG = True
TEMPLATE_DEBUG = DEBUG
SERVE_STATIC_FILES = False
STATIC_ROOT = ""
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
SOUTH_TESTS_MIGRATE = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(DIRNAME, 'sqlite3.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

INTERNAL_IPS = ('127.0.0.1', '192.168.1.101')
DEBUG_TOOLBAR = False
