var settings_map;
window.mater.settings_map = (function () {
  window.polylines = [];
  window.polygons = [];
  var show_map = true;
  var editing_key;
  var cached_path = [];


  var _getBoundFromArray = function (coords) {
    if (coords.length < 2) {
      var c1 = new google.maps.LatLng(22.300-0.1, 70.787-0.1),
        c2 = new google.maps.LatLng(22.300+0.1, 70.787+0.1);
      return new google.maps.LatLngBounds(c1, c2);
    }
    var lats = coords.map(function (c) { return c.lat(); });
    var lngs = coords.map(function (c) { return c.lng(); });
    var min_lat = Math.min.apply(Math, lats),
        max_lat = Math.max.apply(Math, lats),
        min_lng = Math.min.apply(Math, lngs),
        max_lng = Math.max.apply(Math, lngs),
        sw = new google.maps.LatLng(min_lat, min_lng),
        ne = new google.maps.LatLng(max_lat, max_lng);
    var bounds = new google.maps.LatLngBounds(sw, ne);
    return bounds;
  }
  var _getCetroid = function (latlngs) {
    var sum_lats = 0, sum_lngs = 0;
    latlngs.map(function (latlng) {
      sum_lats += latlng.lat();
      sum_lngs += latlng.lng();
    });
    return new google.maps.LatLng(sum_lats/latlngs.length, sum_lngs/latlngs.length)
  }

  var addPolygon = function(key, coords_json, zoomToFit) {
    var coords = JSON.parse(coords_json);
    var path = [];
    for (var i=0; i < coords.length; i++) {
      var latlng = new google.maps.LatLng(coords[i][0], coords[i][1]);
      path.push(latlng)
    }
    var polygon = new google.maps.Polygon({
      paths: path,
        strokeColor: '#666',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#666',
        fillOpacity: 0.35
    });
    polygons[key] = polygon;
    if (settings_map) {
      polygon.setMap(settings_map);
      unhighlightPolygon(key);
      if (zoomToFit != false) {
        settings_map.fitBounds(get_bounds());
      }
    }
  };

  var addPolyline = function(key, coords, zoomToFit) {
    // var coords = JSON.parse(coords_json);
    var path = [];
    for (var i=0; i < coords.length; i++) {
      var latlng = new google.maps.LatLng(coords[i][0], coords[i][1]);
      path.push(latlng)
    }
    var polyline = new google.maps.Polyline({
      path: path,
        strokeColor: '#666',
        strokeOpacity: 0.8,
        strokeWeight: 2
    });
    polylines[key] = polyline;
    if (settings_map) {
      polyline.setMap(settings_map);
      unhighlightPolyline(key);
      if (zoomToFit != false) {
        settings_map.fitBounds(get_bounds());
      }
    }
  };

  var addStartingPolygon = function () {
    var center, bounds;

    bounds = settings_map.getBounds();
    center = bounds.getCenter();

    var bounds_sw = bounds.getSouthWest(),
        bounds_ne = bounds.getNorthEast();
    var lat1, lat2, lon1, lon2, path;
    lat1 = (center.lat() + bounds_sw.lat())/2;
    lon1 = (center.lng() + bounds_sw.lng())/2;
    lat2 = (center.lat() + bounds_ne.lat())/2;
    lon2 = (center.lng() + bounds_ne.lng())/2;

    path = [[lat1, lon1], [lat1, lon2], [lat2, lon2], [lat2, lon1]];

    var path_json = JSON.stringify(path);
    addPolygon(0, path_json, false);
    return path_json;
  };

  var addStartingPolyline = function () {
    var center, bounds;

    bounds = settings_map.getBounds();
    center = bounds.getCenter();

    var bounds_sw = bounds.getSouthWest(),
        bounds_ne = bounds.getNorthEast();
    var lat1, lat2, lon1, lon2, path;
    lat1 = (center.lat() + bounds_sw.lat())/2;
    lon1 = (center.lng() + bounds_sw.lng())/2;
    lat2 = (center.lat() + bounds_ne.lat())/2;
    lon2 = (center.lng() + bounds_ne.lng())/2;

    path = [[lat1, lon1], [lat1, lon2], [lat2, lon2], [lat2, lon1]];

    // var path_json = JSON.stringify(path);
    addPolyline(0, path, false);
    return path_json;
  };

  var getPolygonJSON = function (key) {
    var poly = polygons[key];
    if (!poly) return '{}'
      var latlngs = poly.getPath().getArray();
    var coords = [];

    for (var i=0; i < latlngs.length; i++) {
      var c = latlngs[i];
      coords.push([c.lat(), c.lng()]);
    }
    return JSON.stringify(coords);
  };

  var getPolylineJSON = function (key) {
    var poly = polylines[key];
    if (!poly) return '{}'
      var latlngs = poly.getPath().getArray();
    var coords = [];

    for (var i=0; i < latlngs.length; i++) {
      var c = latlngs[i];
      coords.push([c.lat(), c.lng()]);
    }
    return JSON.stringify(coords);
  };

  var unhighlightPolygon = function (key) {
    var options = {
      strokeColor: '#6f6',
      strokeWeight: 2,
      fillColor: '#26f'
    };
    var polygon = polygons[key];
    if (polygon) polygon.setOptions(options);
  }

  var unhighlightPolyline = function (key) {
    var options = {
      strokeColor: '#6f6',
      strokeWeight: 2
    };
    var polyline = polylines[key];
    if (polyline) polyline.setOptions(options);
  }

  var highlightPolygon = function (key) {
    var options = {
      strokeColor: '#f62',
      strokeWeight: 3,
      fillColor: '#f62'
    };
    var polygon = polygons[key];
    if (polygon) {
      polygon.setOptions(options);
      if (polygon.getMap()) {
        var centroid = _getCetroid(polygon.getPath().getArray());
        settings_map.setCenter(centroid);
      }
    }
  }

  var highlightPolyline = function (key) {
    var options = {
      strokeColor: '#f62',
      strokeWeight: 3
    };
    var polyline = polylines[key];
    if (polyline) {
      polyline.setOptions(options);
      if (polyline.getMap()) {
        var bounds = _getBoundFromArray(polyline.getPath().getArray());
        settings_map.fitBounds(bounds);
      }
    }
  }

  var startEditingPolygon = function (key) {
    var polygon = polygons[key];
    editing_key = key;
    cached_path = polygon.getPath().getArray().concat([]);

    for (var p in polygons) {
      var poly = polygons[p];
      poly.setVisible(p == key);
      poly.setEditable(p == key);
    }
  };
  var zoomPolygon = function (key) {
    var polygon = polygons[key];
    var bounds = _getBoundFromArray(polygon.getPath().getArray());
    settings_map.fitBounds(bounds);
  };

  var startEditingPolyline = function (key) {
    var polyline = polylines[key];
    editing_key = key;
    cached_path = polyline.getPath().getArray().concat([]);

    for (var p in polylines) {
      var poly = polylines[p];
      poly.setVisible(p == key);
      poly.setEditable(p == key);
    }
  };

  var _stopEditingPolygon = function (cancelled) {
    var polygon = polygons[editing_key];
    if (!polygon) return false;  // XXX: Why is this required?
    if (cancelled) {
      polygon.setPath(cached_path);
    }
    cached_path = null;
    editing_key = null;

    for (var p in polygons) {
      var poly = polygons[p];
      poly.setVisible(true);
      poly.setEditable(false);
    }
  }

  var _stopEditingPolyline = function (cancelled) {
    var polyline = polylines[editing_key];
    if (!polyline) return false;  // XXX: Why is this required?
    if (cancelled) {
      polyline.setPath(cached_path);
    }
    cached_path = null;
    editing_key = null;

    for (var p in polylines) {
      var poly = polylines[p];
      poly.setVisible(true);
      poly.setEditable(false);
    }
  }

  var stopEditingPolygon = function (pk) {
    if (typeof pk != 'undefined' && editing_key == 0) {
      polygons[pk] = polygons[0];
      delete polygons[0];
      editing_key = pk;
    }
    _stopEditingPolygon(false);
  }

  var stopEditingPolyline = function (pk) {
    if (typeof pk != 'undefined' && editing_key == 0) {
      polylines[pk] = polylines[0];
      delete polylines[0];
      editing_key = pk;
    }
    _stopEditingPolyline(false);
  }

  var cancelEditingPolygon = function () {
    _stopEditingPolygon(true);
  }

  var cancelEditingPolyline = function () {
    _stopEditingPolyline(true);
  }

  var deletePolygon = function (pk) {
    var polygon = polygons[pk];
    if (typeof polygon != 'undefined') {
      delete polygons[pk];
      polygon.setMap(null);
    }
  }

  var deletePolyline = function (pk) {
    var polyline = polylines[pk];
    if (typeof polyline != 'undefined') {
      delete polylines[pk];
      polyline.setMap(null);
    }
  }


  var get_bounds = function () {
    var coords = [];
    for (var poly_key in polylines) {
      var path = polylines[poly_key].getPath();
      if (path) {
        coords = coords.concat(path.getArray())
      }
    }
    return _getBoundFromArray(coords);
  };

  // Map
  var initialize = function () {
    var bounds = get_bounds();
    var mapOptions = {
      zoom: 16,
      center: bounds.getCenter(),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      //mapTypeControl: false,
      panControl: false,
      rotateControl: false,
      streetViewControl: false
    };
    if (show_map) {
      var $map = $('#map-container')[0];
      settings_map = new google.maps.Map($map, mapOptions);
      for (var key in polylines) {
        var polyline = polylines[key];
        polyline.setMap(settings_map);
        unhighlightPolyline(key);
      }
      for (var key in polygons) {
        var polygon = polygons[key];
        polygon.setMap(settings_map);
        unhighlightPolygon(key);
      }
    }
  }

  var update_map = function () {
    window.setTimeout(function () {
      google.maps.event.trigger(settings_map, 'resize');
      settings_map.fitBounds(get_bounds());
    }, 50);
  };

  return {
    'initialize': function () {
      if (show_map) {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    },
      'get_bounds': get_bounds,
      'addPolygon': addPolygon,
      'addPolyline': addPolyline,
      'addStartingPolygon': addStartingPolygon,
      'addStartingPolyline': addStartingPolyline,
      'getPolygonJSON': getPolygonJSON,
      'getPolylineJSON': getPolylineJSON,
      'startEditingPolygon': startEditingPolygon,
      'startEditingPolyline': startEditingPolyline,
      'stopEditingPolygon': stopEditingPolygon,
      'stopEditingPolyline': stopEditingPolyline,
      'deletePolygon': deletePolygon,
      'deletePolyline': deletePolyline,
      'cancelEditingPolygon': cancelEditingPolygon,
      'cancelEditingPolyline': cancelEditingPolyline,
      'highlightPolygon': highlightPolygon,
      'highlightPolyline': highlightPolyline,
      'unhighlightPolygon': unhighlightPolygon,
      'unhighlightPolyline': unhighlightPolyline,
      'update_map': update_map,
      'zoomPolygon': zoomPolygon
  };
}());
