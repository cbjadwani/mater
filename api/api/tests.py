from decimal import Decimal
import random
import time
import urllib

from django.core.urlresolvers import reverse
from django.test import TestCase

from common.models import Device, Asset, DevLog
from api import auth


class UtilsTest(TestCase):
    def test_gen_token(self):
        token = auth.gen_token()
        self.assertIsInstance(token, basestring)

    def test_create_sig(self):
        device = Device.objects.create(token=auth.gen_token())
        params = {'key1': 'val1', 'key2': 'val2'}
        sig1 = auth._create_sig(device.token, params)
        self.assertIsInstance(sig1, basestring)
        self.assertEqual(len(sig1), 32)
        self.assertEqual(sig1, auth._create_sig(device.token, params))

        params['key3'] = 'val3'
        sig2 = auth._create_sig(device.token, params)
        self.assertIsInstance(sig2, basestring)
        self.assertEqual(len(sig2), 32)
        self.assertNotEqual(sig1, sig2)

    def test_check_ts(self):
        dev_id = random.randint(1, 10 ** 5)
        ts = int(time.time() * 1000)
        auth._check_ts(dev_id, ts)
        self.assertRaises(ValueError, auth._check_ts, dev_id, ts)
        self.assertRaises(ValueError, auth._check_ts, dev_id, ts - 10)
        auth._check_ts(dev_id, ts + 1)

    def test_check_auth(self):
        device = Device.objects.create(token=auth.gen_token())
        ts = int(time.time() * 1000)
        params = {'key1': 'val1', 'key2': 'val2', 'ts': '%X' % ts}
        params['sig'] = auth._create_sig(device.token, params)

        dev_id = device.str_id()
        auth.check_auth(dev_id, params.copy())

        p = params.copy()
        d = dev_id[1:] + 'A'
        self.assertRaises(Device.DoesNotExist, auth.check_auth, d, p)

        p = params.copy()
        d = dev_id[1:] + 'X'
        self.assertRaises(Device.DoesNotExist, auth.check_auth, d, p)

        p = params.copy()
        p['sig'] = 'BADSIG' + p['sig'][6:]
        self.assertRaises(ValueError, auth.check_auth, dev_id, p)


class ViewsTest(TestCase):

    def setUp(self):
        self.device = Device.objects.create(token=auth.gen_token())
        self.asset = Asset.objects.create(name='test-asset1')
        self.asset.install_device(self.device)

    def test_log(self):
        x = round((random.random() * 360) - 180, 5)
        y = round((random.random() * 180) - 90, 5)
        s = random.randint(0, 150)
        ts = '%X' % int(time.time() * 1000)
        params = {'x': '%.5f' % x, 'y': '%.5f' % y, 's': str(s), 'ts': ts}
        params['sig'] = auth._create_sig(self.device.token, params)

        qs = '&'.join('%s=%s' % (k, v) for k, v in params.items())
        url = reverse('log', args=[self.device.str_id()])
        rsp = self.client.get('%s?%s' % (url, qs))
        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, 'OK')

        log = DevLog.objects.get()
        str_ts = time.mktime(log.timestamp.timetuple()) + \
                log.timestamp.microsecond / 1000000.0
        str_ts = '%X' % int(str_ts * 1000)
        self.assertEqual(log.device, self.device)
        self.assertEqual(log.asset, self.asset)
        self.assertEqual(str_ts, params['ts'])
        self.assertEqual(log.latitude.to_eng_string(), params['y'])
        self.assertEqual(log.longitude.to_eng_string(), params['x'])
        self.assertEqual(str(log.speed), params['s'])

    def test_stopped(self):
        # First send a normal log
        x = round((random.random() * 360) - 180, 5)
        y = round((random.random() * 180) - 90, 5)
        s = random.randint(0, 150)
        ts = '%X' % int(time.time() * 1000)
        params = {'x': str(x), 'y': str(y), 's': str(s), 'ts': ts}
        params['sig'] = auth._create_sig(self.device.token, params)

        qs = '&'.join('%s=%s' % (k, v) for k, v in params.items())
        url = reverse('log', args=[self.device.str_id()])
        rsp = self.client.get('%s?%s' % (url, qs))
        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, 'OK')

        ts = '%X' % int(time.time() * 1000)
        params2 = {'ts': ts}
        params2['sig'] = auth._create_sig(self.device.token, params2)

        qs = '&'.join('%s=%s' % (k, v) for k, v in params2.items())
        url = reverse('stopped', args=[self.device.str_id()])
        rsp = self.client.get('%s?%s' % (url, qs))
        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, 'OK')

        log2, log1 = DevLog.objects.all()
        str_ts = time.mktime(log2.timestamp.timetuple()) + \
                log2.timestamp.microsecond / 1000000.0
        str_ts = '%X' % int(str_ts * 1000)
        self.assertLess(log1.timestamp, log2.timestamp)
        self.assertEqual(self.device, log2.device)
        self.assertEqual(self.asset, log2.asset)
        self.assertEqual(str_ts, params2['ts'])
        self.assertEqual(log2.latitude, log1.latitude)
        self.assertEqual(log2.longitude, log1.longitude)
        self.assertEqual(log2.speed, log1.speed)

    def test_root(self):
        device = Device.objects.create(token='012497001940042')
        asset = Asset.objects.create(name='test_asset')
        asset.install_device(device)
        data = '>012497001940042|422|22.272739|70.794968|191112162448|' \
                '0.011594|32|0|16.1987|0.000000<'
        url = '%s?q=%s' % (reverse('root'), urllib.quote(data))

        self.assertEqual(DevLog.objects.count(), 0)
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':AGPRSOKB;')
        self.assertEqual(DevLog.objects.count(), 1)
        devlog = DevLog.objects.get()
        self.assertEqual(devlog.asset, asset)
        self.assertEqual(devlog.device, device)
        self.assertEqual(devlog.latitude, Decimal('22.27274'))
        self.assertEqual(devlog.longitude, Decimal('70.79497'))
        self.assertEqual(devlog.speed, 30)
        self.assertEqual(devlog.ignition, None)

        data = '>012497001940042|422|22.272840|70.794968|191112162449|' \
                '0.011594|32|0|16.1987|0.000000|1007|812|0|0|0|0|0|31|' \
                '186|0|0|-160|0|0|13F1|5900|0619,33,99,404,98,55<'
        url = '%s?q=%s' % (reverse('root'), urllib.quote(data))

        self.assertEqual(DevLog.objects.count(), 1)
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':AGPRSOKB;')
        self.assertEqual(DevLog.objects.count(), 2)
        devlog = DevLog.objects.latest()
        self.assertEqual(devlog.asset, asset)
        self.assertEqual(devlog.device, device)
        self.assertEqual(devlog.latitude, Decimal('22.27284'))
        self.assertEqual(devlog.longitude, Decimal('70.79497'))
        self.assertEqual(devlog.speed, 30)
        self.assertEqual(devlog.ignition, False)

    def test_root_command(self):
        # Test single command
        device = self.device
        device.command = ':>F|07|1|G;'
        device.save()
        data = '>%s|422|22.272739|70.794968|191112162448|' \
                '0.011594|32|0|16.1987|0.000000<' % device.token
        url = '%s?q=%s' % (reverse('root'), urllib.quote(data))
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, device.command)

        device = Device.objects.get(pk=device.pk)
        self.assertEqual(device.command, '')

        # Test multiple commands and updating config
        new_sos_no = '+919078563412'
        device.command = '%s\n%s' % (
            ':>F|07|1|G;', '{"sos_no": "%s"}' % new_sos_no)
        device.save()

        # send the previous data and check for first command in response
        rsp = self.client.get(url)
        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':>F|07|1|G;')

        # send the device config and check for new config in response
        config_string = (
            device.token + '%s|123456|%s|mobile.apn.site|'
            '||||||||||150|100|150|100|150|100|150|100|'
            '255.255.255.255|80|2|300|10|2|15|1|30|120|2|7')
        data = config_string % ('|0|F|V11.1B', '+911234567890')
        data = '>%s<\r\n' % data

        url = '%s?q=%s' % (reverse('root'), urllib.quote(data))
        rsp = self.client.get(url)
        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content,
                         ':>F|07|02|%s|G;' % (config_string % ('', new_sos_no)))

        device = Device.objects.get(pk=device.pk)
        self.assertEqual(device.command, '')

    def test_root_acceleration(self):
        device = self.device
        data = '>%s|422|%s|%s|191112162449|' \
                '0.011594|32|0|%%s|0.000000|1007|812|0|0|0|0|0|31|' \
                '%%s|0|%%s|0|%%s|0|13F1|5900|0619,33,99,404,98,55<' % \
                (device.token, 22.272840, 70.794968)
        url = '%s?q=%s' % (reverse('root'),
                           urllib.quote(data % (10, 1, 1, 1)))
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':AGPRSOKB;')
        self.assertEqual(DevLog.objects.count(), 1)

        data = '>%s|422|%s|%s|191112162459|' \
                '0.011594|32|0|%%s|0.000000|1007|812|0|0|0|0|0|31|' \
                '%%s|0|%%s|0|%%s|0|13F1|5900|0619,33,99,404,98,55<' % \
                (device.token, 22.272990, 70.794968)
        url = '%s?q=%s' % (reverse('root'),
                           urllib.quote(data % (10 / 1.854, 0, 0, 0)))
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':AGPRSOKB;')
        self.assertEqual(DevLog.objects.count(), 1)

        data = '>%s|422|%s|%s|191112162509|' \
                '0.011594|32|0|%%s|0.000000|1007|812|0|0|0|0|0|31|' \
                '%%s|0|%%s|0|%%s|0|13F1|5900|0619,33,99,404,98,55<' % \
                (device.token, 22.272990, 70.794968)
        url = '%s?q=%s' % (reverse('root'),
                           urllib.quote(data % (10 / 1.854, 0, 1, 0)))
        rsp = self.client.get(url)

        self.assertEqual(rsp.status_code, 200)
        self.assertEqual(rsp.content, ':AGPRSOKB;')
        self.assertEqual(DevLog.objects.count(), 2)
