function RoutesCtrl($scope) {
  $scope.routes = [];
  $scope.editing = false;
  $scope.route_editing = null;
  $scope.editing_on_map = false;
  $scope.load_data = function () {
    $.getJSON(
      mater.urls.get_routes,
      function (data) {
        if (data.ok) {
          $scope.$apply(function () {
            $scope.routes = data.data;
            for (var i=0; i < data.data.length; i++) {
              var route = data.data[i];
              var route_data = JSON.parse(route.fields.map_route_json);
              var route_points = route_data.points;
              mater.settings_map.addPolyline(route.pk, route_points);
            }
          });
        }
      }
    );
  };
  $scope.hoverTimeOutID = null;

  $scope.load_data();

  var getPlacesScope = function () {
    return angular.element('#places').scope();
  };
  var getPlaceName = function (pk) {
    var $places_scope = getPlacesScope();
    var place = $places_scope.places.filter(function (p) {return p.pk == pk;})[0];
    if (typeof place !== 'undefined') {
      return place.fields.name;
    }
  };

  $scope.isClean = function () {
    return ($scope.route_editing &&
        !$scope.editing_on_map &&
        $scope.form_route_name == $scope.route_editing.fields.name &&
        $scope.form_route_category == $scope.route_editing.fields.category);
  };
  $scope.form_submit = function () {
    if ($scope.routes_form.$invalid) return false;
    var route = $scope.route_editing;
    route.fields.name = $scope.form_route_name;
    route.fields.category = $scope.form_route_category;
    // route.fields.map_route_json = mater.settings_map.getPolylineJSON(route.pk || 0);

    var ajax_data = {};
    ajax_data.command = route.pk ? 'modify' : 'add';
    ajax_data.json = JSON.stringify([$scope.route_editing]);
    var jqxhr = $.getJSON(
        mater.urls.put_locations,
        ajax_data,
        function (data) {
          if (!data.ok) {
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            $scope.$apply(function ($scope) {
              if (ajax_data.command == 'add') {
                $scope.route_editing.pk = data.added[0];
                $scope.routes.push($scope.route_editing);
              }
              if ($scope.editing_on_map) {
                mater.settings_map.stopEditingPolyline($scope.route_editing.pk);
                $scope.editing_on_map = false;
              }
              $scope.route_editing = null;
              $scope.editing = false;
            });
            $.bootstrapGrowl('Data saved',
              {type: 'success', align: 'center'});
          }
        }
    ).error(function () {
      $.bootstrapGrowl('Error saving data, please try again',
        {type: 'error', align: 'center'});
    });
  };
  $scope.delete_route = function (route) {
    var router_descr = $scope.get_route_description(route);
    var msg = 'Delete Route "' + router_descr + '"?';
    if (!confirm(msg)) { return false; }

    var ajax_data = {};
    ajax_data.command = 'delete';
    ajax_data.json = JSON.stringify([route]);
    var jqxhr = $.getJSON(
        mater.urls.put_routes,
        ajax_data,
        function (data) {
          if (!data.ok) {
            $.bootstrapGrowl('Error saving data, please try again',
              {type: 'error', align: 'center'});
          } else {
            mater.settings_map.deletePolyline(route.pk);
            $scope.$apply('load_data()');
            $.bootstrapGrowl('Route "' + router_descr + '" deleted',
              {type: 'success', align: 'center'});
          }
        }
        ).error(function () {
          $.bootstrapGrowl('Error saving data, please try again',
            {type: 'error', align: 'center'});
        });
  };
  $scope.get_route_description = function (route) {
    var p1_pk = route.fields.place1
      , p2_pk = route.fields.place2;
    if (p1_pk && p2_pk) {
      return getPlaceName(p1_pk) + ' - ' + getPlaceName(p2_pk);
    }
  };
  $scope.start_editing = function (route) {
    $scope.form_route_name = route.fields.name;
    $scope.form_route_category = route.fields.category;
    $scope.editing = true;
    $scope.route_editing = route;
  };
  // $scope.create_new = function () {
  //   var route = {pk: null, model: 'common.route', fields: {}};
  //   route.fields.name = '';
  //   route.fields.category = '';
  //   route.fields.map_route_json = mater.settings_map.addStartingPolyline();
  //   mater.settings_map.highlightPolyline(0);
  //   $scope.start_editing(route);
  // };
  $scope.stop_editing = function () {
    var pk = $scope.route_editing.pk || 0;
    mater.settings_map.unhighlightPolyline(pk);
    $scope.editing = false;
    $scope.route_editing = null;
    if ($scope.editing_on_map) {
      mater.settings_map.cancelEditingPolyline();
      $scope.editing_on_map = false;
    }
    if (pk == 0) {
      mater.settings_map.deletePolyline(0);
    }
  };
  $scope.highlightPolyline = function (route) {
    clearTimeout($scope.hoverTimeOutID);
    $scope.hoverTimeOutID = setTimeout(function () {
      mater.settings_map.highlightPolyline(route.pk);
    },
    333
    );
  };
  $scope.unhighlightPolyline = function (route) {
    clearTimeout($scope.hoverTimeOutID);
    if (!angular.equals(route, $scope.route_editing)) {
      mater.settings_map.unhighlightPolyline(route.pk);
    }
  };
  $scope.start_editing_map = function () {
    $scope.editing_on_map = true;
    mater.settings_map.startEditingPolyline($scope.route_editing.pk || 0);
  };
};
