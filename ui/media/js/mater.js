window.mater = {
  urls: {},
  ajax: {},
  show_messages:  function (messages) {
    for (var i=0; i < messages.length; i++) {
      $.bootstrapGrowl(messages[i], {type: 'info'});
    }
  },
  onresize: function (handler) {
    var resizeTimerID = null;
    $(window).resize(function () {
      clearTimeout(resizeTimerID);
      resizeTimerID = setTimeout(handler, 100);
    });
    handler();
  }
};

window.materApp = angular.module('mater', ['ui', 'ui.bootstrap']);

materApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});

materApp.directive('inlineEdit', function ($timeout) {
  var template = (
    '<span>' +
      '<span ng-hide="editing"' +
            'ng-click="editing = true"' +
            'ng-class="{\'muted\': !value}"' +
            'class="editable" title="click to edit">' +
        '{[{value || "---"}]}' +
      '</span>' +
      '<form ng-show="editing"' +
            'ng-submit="editing = editForm.$invalid"' +
            'name="editForm"' +
            'class="form-inline">' +
        '<input class="{[{inputClass}]}" ng-model="value" {[{input-attrs}]}/>' +
      '</form>' +
    '</span>'
    );
  var get_template = function (input_attrs) {
    var attrs_list = [];
    for (var a in input_attrs) {
      attrs_list.push(a + '="' + input_attrs[a] + '"');
    }
    return template.replace(/\{\[\{input-attrs\}\]\}/, attrs_list.join(' '));
  }
  var link = function ($scope, elem, attr, ctrl) {
    $scope.$watch('editing', function (newval) {
      var input = elem.find('input');
      if (newval) {
        input.on('blur.inlineEdit', function (e) {
          var stopped = !$scope.$apply('stopEditing()');
          if (stopped) {
            input.off('.inlineEdit');
          } else {
            $timeout(function () {input[0].focus();});
          }
          return stopped;
        });
        $timeout(function () {input[0].focus();});
      }
    });
  };
  return {
    restrict: 'A',
    scope: {
      value: '=inlineEdit',
      inputClass: '@'
    },
    controller: function ($scope) {
      $scope.stopEditing = function () {
        if ($scope.editForm.$valid) {
          $scope.editing = false;
        }
        return $scope.editing;
      }
    },
    compile: function (elem, attrs) {
      var input_attrs = {
        type: attrs.type || 'text'
      };
      if (attrs.hasOwnProperty('required')) {
        input_attrs.required = '"required"';
      }
      if (attrs.hasOwnProperty('maxlength')) {
        input_attrs.maxlength = attrs.maxlength;
      }
      elem.replaceWith(get_template(input_attrs));
      return link;
    }
  };
});

materApp.directive('materMarquee', function () {
  var template = (
    '<div class="marquee-container">' +
        '<div class="marquee" id="strip1">{[{div1Msg}]}</div>' +
        '<div class="marquee" id="strip2" style="left: 100%">{[{div2Msg}]}</div>' +
        '<div class="marquee shadow pull-left"></div>' +
        '<div class="marquee shadow pull-right"></div>' +
    '</div>'
  );
  return {
    template: template,
    restrict: 'A',
    replace: true,
    scope: {
      getNextMsg: '&materMarquee',
      speed: '@'
    },
    controller: function ($scope) {
      var initMsg;
      if ($scope.speed === null)  {
        $scope.speed = 100;
      } else {
        $scope.speed = Math.max(50, $scope.speed);
        $scope.speed = Math.min(1000, $scope.speed);
      }
      initMsg = $scope.getNextMsg();
      $scope.div1Msg = initMsg;
      $scope.div2Msg = initMsg;
      $scope.currentDiv = 1;
      $scope.switchDiv = function () {
        var nextMsg;
        nextMsg = $scope.getNextMsg();
        if ($scope.currentDiv === 1) {
          $scope.currentDiv = 2;
          $scope.div1Msg = nextMsg;
        } else {
          $scope.currentDiv = 1;
          $scope.div2Msg = nextMsg;
        }
      };
    },
    link: function (scope, elem, attrs) {
      var pos = 0;
      var incrPos = function () {
        var nextMsg, positions;
        if (pos < 100) {
          pos += 0.25;
        }
        if (pos >= 100) {
          scope.$apply('switchDiv()');
          pos = 0;
        }
        positions = [0 - pos, 100 - pos];
        if (scope.currentDiv === 2) {
          positions.reverse();
        }
        return positions;
      }
      var ticker = function () {
        var positions = incrPos(),
            container, strip1, strip2;
        container = elem[0];
        strip1 = container.childNodes[0];
        strip2 = container.childNodes[1];
        strip1.style.left = positions[0] + '%';
        strip2.style.left = positions[1] + '%';
      };
      setTimeout(function () {
        scope.$apply('switchDiv()');
        ticker();
        setInterval(ticker, scope.speed / 4);
      }, 0);
    }
  };
});

// Copied from angular-ui
/**
 * Enhanced Select2 Dropmenus
 *
 * @AJAX Mode - When in this mode, your value will be an object (or array of objects) of the data used by Select2
 *     This change is so that you do not have to do an additional query yourself on top of Select2's own query
 * @params [options] {object} The configuration options passed to $.fn.select2(). Refer to the documentation
 */
materApp.directive('select2',function(){
  var options = {};
  return {
    require: '?ngModel',
    compile: function (tElm, tAttrs) {
      var watch,
        repeatOption,
        repeatAttr,
        isSelect = tElm.is('select'),
        isMultiple = (tAttrs.multiple !== undefined);

      // Enable watching of the options dataset if in use
      if (tElm.is('select')) {
        repeatOption = tElm.find('option[ng-repeat], option[data-ng-repeat]');

        if (repeatOption.length) {
              repeatAttr = repeatOption.attr('ng-repeat') || repeatOption.attr('data-ng-repeat');
          watch = jQuery.trim(repeatAttr.split('|')[0]).split(' ').pop();
        }
      }

      return function (scope, elm, attrs, controller) {
        // instance-specific options
        var opts = angular.extend({allowClear: true}, options, scope.$eval(attrs.select2));

        if (isSelect) {
          // Use <select multiple> instead
          delete opts.multiple;
          delete opts.initSelection;
        } else if (isMultiple) {
          opts.multiple = true;
        }

        if (controller) {
          // Watch the model for programmatic changes
          controller.$render = function () {
            if (isSelect) {
              elm.select2('val', controller.$modelValue);
            } else {
              if (isMultiple && !controller.$modelValue) {
                elm.select2('data', []);
              } else if (angular.isObject(controller.$modelValue)) {
                elm.select2('data', controller.$modelValue);
              } else {
                elm.select2('val', controller.$modelValue);
              }
            }
          };


          // Watch the options dataset for changes
          if (watch) {
            scope.$watch(watch, function (newVal, oldVal, scope) {
              if (!newVal) return;
              // Delayed so that the options have time to be rendered
              setTimeout(function () {
                elm.select2('val', controller.$viewValue);
                // Refresh angular to remove the superfluous option
                elm.trigger('change');
              });
            });
          }

          if (!isSelect) {
            // Set the view and model value and update the angular template manually for the ajax/multiple select2.
            elm.bind("change", function () {
              scope.$apply(function () {
                controller.$setViewValue(elm.select2('data'));
              });
            });

            if (opts.initSelection) {
              var initSelection = opts.initSelection;
              opts.initSelection = function (element, callback) {
                initSelection(element, function (value) {
                  controller.$setViewValue(value);
                  callback(value);
                });
              };
            }
          }
        }

        attrs.$observe('disabled', function (value) {
          elm.select2(value && 'disable' || 'enable');
        });

        if (attrs.ngMultiple) {
          scope.$watch(attrs.ngMultiple, function(newVal) {
            elm.select2(opts);
          });
        }

        // Set initial value since Angular doesn't
        elm.val(scope.$eval(attrs.ngModel));

        // Initialize the plugin late so that the injected DOM does not disrupt the template compiler
        setTimeout(function () {
          elm.select2(opts);
        });
      };
    }
  };
});

materApp.directive('onBlur', function() {
  return function( scope, elem, attrs ) {
    var onblur = attrs.onBlur;
    elem.on('blur.mater', function() {
      scope.$apply(onblur);
    });
  };
});

materApp.directive('blink', function($timeout) {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        controller: function($scope, $element) {
            function showElement() {
                $element.css("opacity", "1");
                $element.css("filter", "alpha(opacity=1");
                $timeout(hideElement, 1000);
            }
            function hideElement() {
                $element.css("opacity", "0");
                $element.css("filter", "alpha(opacity=0");
                $timeout(showElement, 1000);
            }
            showElement();
        },
        template: '<span ng-transclude></span>',
        replace: true
    };
});

// Utils
var formatSeconds = function(seconds) {
  var hours = Math.floor(seconds / 3600).toFixed();
  var minutes = Math.floor((seconds - hours * 3600) / 60);
  if (hours.length < 2) {
    hours = '0' + hours;
  }
  return hours + ':' + ('00' + minutes).slice(-2);
};

// For IE < 9
if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp */)
  {
    "use strict";

    if (this == null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var res = [];
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in t)
      {
        var val = t[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, t))
          res.push(val);
      }
    }

    return res;
  };
}
