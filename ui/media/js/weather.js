var mater = window.mater || {};

mater.weather = (function() {
  'use strict';


  var callYahooWeather = function (woeid, success) {
    var wsql = 'select * from weather.forecast where woeid=WOEID and u="c"',
        weatherYQL = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent(wsql) + '&format=json&callback=?';
    return $.getJSON(weatherYQL.replace('WOEID', woeid), success);
  };

  var weather = {
    cache: {},
    get: function (woeid, callback) {
      var cached = this.cache[woeid];
      if (cached && cached.ts && moment().diff(cached.ts, 'minutes') <= 10) {
        callback(cached);
        return;
      }
      delete this.cache[woeid];
      callYahooWeather(woeid, function (data, status) {
        data.ts = moment();
        weather.cache[woeid] = data;
        callback(data);
      })
    },
    iconNames: ['Thunderstorms', 'Thunderstorms', 'Thunderstorms', 'Thunderstorms',
      'Thunderstorms', 'Snow', 'Thunderstorms', 'Thunderstorms', 'Thunderstorms', 'Thunderstorms',
      'Thunderstorms', 'Thunderstorms', 'Thunderstorms', 'Snow', 'Snow', 'Snow', 'Snow',
      'Thunderstorms', 'Thunderstorms', 'Haze', 'Haze', 'Haze', 'Haze', 'Cloudy', 'Cloudy',
      'Cloudy', 'Cloudy', 'Cloudy Night', 'Mostly Cloudy', 'Cloudy Night', 'Mostly Cloudy', 'Moon',
      'Sunny', 'Moon', 'Sunny', 'Drizzle Snow', 'Sunny', 'Thunderstorms', 'Thunderstorms',
      'Thunderstorms', 'Drizzle', 'Snow', 'Snow', 'Snow', 'Cloudy', 'Drizzle', 'Snow',
      'Thunderstorms']
  };
  return weather;
})();
