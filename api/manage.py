#!/usr/bin/env python
import os
import sys


def patch_server():
    """ Patch the dev server to allow malformed http requests sent from
    the device from www.aniruddhagps.com
    """
    from django.core.servers.basehttp import WSGIRequestHandler
    import StringIO
    import urllib

    def parse_request(self):
        if self.raw_requestline.startswith('>'):
            raw_request = self.raw_requestline
            qs = urllib.quote(raw_request)
            self.raw_requestline = 'GET /?q=%s\r\n' % qs
        return real_parse_request(self)

    real_parse_request = WSGIRequestHandler.parse_request
    WSGIRequestHandler.parse_request = parse_request


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

    from django.core.management import execute_from_command_line

    patch_server()

    execute_from_command_line(sys.argv)
