var mater = window.mater;

var map;
var MARKERS = [];
var VIOLATION_MARKERS = [];
var DAY_TRACK = [];
var TRIP_TRACK = [];
var PLACE_ICONS = [];
DAY_TRACK_POLYLINE = null;
TRIP_TRACK_POLYLINE = null;
var show_map = true;
var panlockctrl;


var _Max = function (arr) { return Math.max.apply(Math, arr); }
  , _Min = function (arr) { return Math.min.apply(Math, arr); }


//{{ LabeledImageMarker
function LabeledImageMarker() {
  RichMarker.apply(this, arguments);
}
LabeledImageMarker.prototype = new RichMarker({});

LabeledImageMarker.prototype.fixAnchor_ = function() {
  if (this.getMap() == null) {
    return;
  }
  var wrapper = $(this.getContent());
  var img = wrapper.find('img');
  var txt = wrapper.find('.marker-label');
  var img_width = img.outerWidth();
  var img_height = img.outerHeight();
  var txt_width = txt.outerWidth();
  var txt_height = txt.outerHeight();
  if (txt_width > img_width) {
    var margins = (txt_width - img_width) / 2;
    img[0].style.marginLeft = margins + 'px';
    img[0].style.marginRight = margins + 'px';
  }
  this.setAnchor(new google.maps.Size(-1 * wrapper.outerWidth()/2, -1 * img_height/2));
}
LabeledImageMarker.prototype.updateLabelStyle = function() {
  var style,
      map = this.getMap();
  if (map == null) {
    return;
  }
  if (map.getMapTypeId() == google.maps.MapTypeId.HYBRID) {
    style = 'light';
  } else {
    style = '';
  }
  var wrapper = $(this.getContent());
  var txt = wrapper.find('.marker-label')[0];
  txt.setAttribute('class', 'marker-label' + ' ' + style);
}

LabeledImageMarker.prototype.onAdd = function() {
  RichMarker.prototype.onAdd.apply(this, arguments);
  this.fixAnchor_();
  this.updateLabelStyle();
}
LabeledImageMarker.prototype['onAdd'] = LabeledImageMarker.prototype.onAdd;
//}} LabeledImageMarker


//{{ PlaceOverlay
function PlaceOverlay () {
  this.paneName = 'mapPane';
  LabeledImageMarker.apply(this, arguments);
};
PlaceOverlay.prototype = new LabeledImageMarker({});
PlaceOverlay.prototype.fixAnchor_ = function () {};
//}} LabeledImageMarker

ViolationMarker = function (options) {
  var marker = this;
  var when = moment(options.when * 1000).format('hh:mmA DD/MM/YY');
  var details;
  var content;
  if (options.type == 1) {
    // Parking violation
    details = formatSeconds(options.details);
    content = 'Parked for ' + details + '<br>at ' + when;
    options.icon = {url: 'media/img/Map-Marker-Flag--Right-Azure.png'};
  } else if (options.type == 2) {
    details = options.details + ' km/h';
    content = 'Speed Violation: ' + details + '<br>at ' + when;
    options.icon = {url: 'media/img/Map-Marker-Flag--Right-Pink.png'};
  } else if (options.type == 3) {
    content = 'Left ' + (options.details || '') + '<br>at ' + when;
  }
  delete options.info;
  options.clickable = true;
  options.flat = true;
  google.maps.Marker.apply(this, arguments);

  this.addListener('mouseover', function () {
    if (typeof this.infoWindow != 'undefined' && this.infoCloseTimer) {
      clearTimeout(this.infoCloseTimer);
      delete this.infoCloseTimer;
    } else {
      this.infoWindow && this.infoWindow.close();
      this.infoWindow = new google.maps.InfoWindow({
        content: content,
        disableAutoPan: true,
        pixelOffset: {width: 0, height: 0}
      });
      this.infoWindow.addListener('closeclick', function () {
        this.close();
        delete marker.infoWindow;
      });
      this.infoWindow.open(map, this);
    }
  });
  this.addListener('mouseout', function () {
    var that = this;
    this.infoCloseTimer = setTimeout(function () {
      if (typeof that.infoWindow != 'undefined') {
        that.infoWindow.close();
        delete that.infoWindow;
      }
    }, 100);
  });
  this.addListener('click', function () {
    var pos = this.getPosition()
      , that = this;
    if (map) {
      map.setZoom(18);
      map.setCenter(pos);
      setTimeout(function () {
        that.setMap(map);
      }, 0)
    }
  });
};
ViolationMarker.prototype = new google.maps.Marker({});

var update_violations = function (violations) {
  var new_violations = []
    , v;
  for (var i = 0; i < violations.length; i++) {
    v = add_violation(
        violations[i][0],
        violations[i][1],
        violations[i][2],
        violations[i][3]
    )
    v._new = true;
    new_violations.push(v);
  }
  for (var i = 0; i < VIOLATION_MARKERS.length; i++) {
    v = VIOLATION_MARKERS[i];
    if (v._new) {
      v._new = false;
      delete v._new;
    } else {
      hide_violation(v);
    }
  }
  VIOLATION_MARKERS = new_violations;
};

var add_violation = function (position, when, details, type) {
  var marker, i, visible = map && (map.getZoom() >= 18 ? null : map);
  for (i=0; i < VIOLATION_MARKERS.length; i++) {
    marker = VIOLATION_MARKERS[i];
    if (Math.abs(marker.position.lat() - position[0]) < 0.00001 &&
        Math.abs(marker.position.lng() - position[1]) < 0.00001 &&
        marker.type == type) {
      return marker;
    }
  }
  var latLngPos = new google.maps.LatLng(position[0], position[1]);
  marker = new ViolationMarker({
    position: latLngPos,
    when: when,
    details: details,
    type: type,
    map: visible
  });
  VIOLATION_MARKERS.push(marker);
  return marker;
};

var hide_violation = function (marker) {
  if (typeof marker.infoWindow != 'undefined') {
    marker.infoWindow.close();
    delete marker.infoWindow;
  }
  marker.setMap(null);
};

var hide_violations = function () {
  var marker, i;
  for (i = 0; i < VIOLATION_MARKERS.length; i++) {
    marker = VIOLATION_MARKERS[i];
    hide_violation(marker);
  }
};

var show_violations = function () {
  var marker, i;
  for (i=0; i < VIOLATION_MARKERS.length; i++) {
    marker = VIOLATION_MARKERS[i];
    marker.setMap(map);
  }
};

var clear_violations = function () {
  hide_violations();
  VIOLATION_MARKERS = [];
};

// Places
var update_places = function (places) {
  var place, i, icon, opts, new_places = [];
  for (i = 0; i < places.length; i++) {
    place = places[i];
    icon = PLACE_ICONS[place.pk];
    if (icon instanceof PlaceOverlay) {
      icon._new = true;
    } else {
      opts = _get_marker_opts(
        place.centroid[0],
        place.centroid[1],
        '',  // place.name,
        0,
        'place-icon',
        place.icon_url
      );
      opts.anchor = RichMarkerPosition.BOTTOM_RIGHT;
      icon = new PlaceOverlay(opts);
    }
    icon.setMap(map);
    new_places[place.pk] = icon;
  }
  for (pk in PLACE_ICONS) {
    icon = PLACE_ICONS[pk];
    if (icon._new) {
      icon._new = false;
      delete icon._new;
    } else {
      icon.setVisible(false);   // This is required
      icon.setMap(null);
    }
  }
  PLACE_ICONS = new_places;
};

// AJAX updating
var show_tracks = function (track, loaded) {
  var latlng
    , day_path
    , trip_path
    , visible = map && (map.getZoom() >= 18 ? null : map);
  ;

  if (!track) {
    if (TRIP_TRACK_POLYLINE) {
      TRIP_TRACK_POLYLINE.setMap(visible);
    }
    return;
  }
  trip_path = [];
  for (var i = 0; i < track.length; i++) {
    latlng = new google.maps.LatLng(track[i][0], track[i][1]);
    trip_path.push(latlng);
  }

  if (TRIP_TRACK_POLYLINE) {
    if (trip_path.length >= 2) {
      TRIP_TRACK_POLYLINE.setPath(trip_path);
      TRIP_TRACK_POLYLINE.setMap(visible);
      TRIP_TRACK_POLYLINE.setOptions({
        strokeColor: loaded ? '#F06F40' : '#6040FF',
      });
    } else {
      TRIP_TRACK_POLYLINE.setMap(null);
    }
  } else if (trip_path.length >= 2) {
    TRIP_TRACK_POLYLINE = new google.maps.Polyline({
      path: trip_path,
      clickable: false,
      map: visible,
      strokeColor: loaded ? '#F06F40' : '#6040FF',
      strokeOpacity: 0.8,
      strokeWeight: 3,
      icons: [{
        icon: {
          path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
          fillOpacity: 1.0
        },
        repeat: '100px',
        offset: '15px'
      }],
      zIndex: 10
    });
  }
}

var hide_tracks = function () {
  if (DAY_TRACK_POLYLINE) {
    DAY_TRACK_POLYLINE.setMap(null);
  }
  if (TRIP_TRACK_POLYLINE) {
    TRIP_TRACK_POLYLINE.setMap(null);
  }
}

var _get_marker_opts = function (lat, lng, caption, rotate, wrapperClass, imgUrl) {
  var latlng = new google.maps.LatLng(lat, lng);
  var el = document.createElement('DIV');
  var img = document.createElement('IMG');
  var txt = document.createElement('DIV');
  wrapperClass = wrapperClass || '';
  wrapperClass += 'marker-wrapper';
  el.className = wrapperClass;
  txt.innerHTML = caption;
  txt.className = 'marker-label';
  img.src = imgUrl || 'media/img/car-halfsize-topview.png';
  el.appendChild(img);
  el.appendChild(txt);
  if (typeof rotate != 'undefined' && rotate > -360 && rotate < 360) {
    $(el).find('img').rotate(rotate);
  }
  var marker_opts = {
    'position': latlng,
    'anchor': RichMarkerPosition.MIDDLE,
    'content': el,
    'flat': true
  };
  return marker_opts;
}

var add_marker = function (pk, lat, lng, caption, rotation) {
  if (MARKERS[pk]) {
    hide_marker(pk);
    delete MARKERS[pk];
  }

  if (!lat || !lng) return;
  var marker_opts = _get_marker_opts(lat, lng, caption, rotation);
  var marker = new LabeledImageMarker(marker_opts);

  MARKERS[pk] = marker;
  show_marker(pk);
}

var update_marker = function (pk, lat, lng, caption, rotation) {
  var marker = MARKERS[pk];
  var marker_opts = _get_marker_opts(lat, lng, caption, rotation);
  if (marker) {
    marker.setOptions(marker_opts);
    marker.updateLabelStyle();
    marker.fixAnchor_();
  }
}

var show_marker = function (pk) {
  var marker = MARKERS[pk];
  if (marker && map) {
    marker.setMap(map);
    marker.setVisible(true);
    marker.fixAnchor_();
  }
}

var hide_marker = function (pk) {
  var marker = MARKERS[pk];
  if (marker && map) { marker.setVisible(false); }
}

var pan_to_marker = function (pk) {
  if (pk === 'all') {
    var lats = []
      , lngs = [];
    for (var i in MARKERS) {
      var position = MARKERS[i].getPosition();
      lats.push(position.lat());
      lngs.push(position.lng());
    }
    var sw = new google.maps.LatLng(_Min(lats), _Min(lngs))
      , ne = new google.maps.LatLng(_Max(lats), _Max(lngs))
      , bounds = new google.maps.LatLngBounds(sw, ne);
    map.fitBounds(bounds);
  } else {
    var marker = MARKERS[pk];
    if (marker && map) {
      map.panTo(marker.getPosition());
    }
  }
}

var resize_map = function () {
  if (map) {
    panlockctrl.dont_unlock = true;
    google.maps.event.trigger(map, 'resize');
    panlockctrl.recenter();
    panlockctrl.dont_unlock = false;
  }
};

var fetch_first_logs = function () {
  var jqxhr = $.getJSON(
    mater.urls.get_initial_logs,
    function (data, textStatus, xhr) {
      $('#assets-ctrl').scope().$apply(function ($scope) {
        $scope.update_asset_stats(data.assets);
      });
      mater.show_messages(data.messages);
    }
  )
  jqxhr.complete(function () {
    $('#assets-ctrl').scope().$apply(function ($scope) {
      $scope.update_last_time();
    });
  });
};

var get_angle_from_tracks = function(tracks) {
  var angle = 0;
  if (tracks && tracks.length >= 2) {
    var logs = tracks.slice(-2)
      , lat_diff = logs[1][0] - logs[0][0]
      , lng_diff = logs[1][1] - logs[0][1]
    ;
    angle = Math.atan2(lng_diff, lat_diff) * 180 / Math.PI;
  }
  return angle;
}

var update_map_center = function () {
  if (panlockctrl && panlockctrl.locked) {
    panlockctrl.recenter();
  }
}

var setMapCenterAndExtent = function (extent, mapTypeId) {
  mapTypeId = mapTypeId || google.maps.MapTypeId.ROADMAP;
  if (typeof panlockctrl != 'undefined') {
    panlockctrl.setCenterAndExtent(extent, mapTypeId);
  }
}


var maptypeid_changed = function () {
  var pk;
  for (pk in MARKERS) {
    MARKERS[pk].updateLabelStyle();
  }
  if (!panlockctrl.dont_unlock) {
    panlockctrl.unlock();
  }
};

var zoom_changed = function () {
  var mapTypeId;

  if (map.getZoom() >= 18) {
    hide_violations();
    hide_tracks();
    mapTypeId = google.maps.MapTypeId.HYBRID;
  } else {
    show_violations();
    show_tracks();
    mapTypeId = google.maps.MapTypeId.ROADMAP;
  }
  if (mapTypeId && map.getMapTypeId() != mapTypeId) {
    map.setMapTypeId(mapTypeId);
  }
};

// Map
var initialize = function () {
  var center;
  var asset;
  var selected_pk;
  $('#assets-ctrl').scope().$apply(function (scope) {
    selected_pk = scope.selected_pk;
    for (var i in scope.user_assets) {
      asset = scope.user_assets[i];
      break;
    }
  });
  if (asset) {
    center = new google.maps.LatLng(asset.y, asset.x);
  } else {
    center = new google.maps.LatLng(22.300, 70.7833);
  }
  var mapOptions = {
    zoom: 14,
    center: center,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    //mapTypeControl: false,
    panControl: false,
    rotateControl: false,
    streetViewControl: false
  };
  if (show_map) {
    map = new google.maps.Map(document.getElementById('map-container'),
        mapOptions);
    show_marker(selected_pk);
    if (TRIP_TRACK_POLYLINE) {
      TRIP_TRACK_POLYLINE.setMap(map);
    }
    if (DAY_TRACK_POLYLINE) {
      DAY_TRACK_POLYLINE.setMap(map);
    }
    map.addListener('maptypeid_changed', maptypeid_changed);
    map.addListener('zoom_changed', zoom_changed);
    panlockctrl = new mater.PanLockControl(map);
    panlockctrl.wrapperDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(panlockctrl.wrapperDiv);
    $('#assets-ctrl').scope().$apply('setMapCenterAndExtent()');
  }
}

$(document).ready(function () {
  fetch_first_logs();
  setInterval(fetch_first_logs, 15000);
});

mater.livemap = {
  'initialize': function () {
    if (show_map) {
      google.maps.event.addDomListener(window, 'load', initialize);
    }
  },
  'add_marker': add_marker,
  'update_marker': update_marker,
  'setMapCenterAndExtent': setMapCenterAndExtent,
  'show_marker': show_marker,
  'hide_marker': hide_marker,
  'update_violations': update_violations,
  'hide_violations': hide_violations,
  'clear_violations': clear_violations,
  'update_places': update_places,
  'resize_map': resize_map,
  'show_tracks': show_tracks,
  'hide_tracks': hide_tracks,
  'get_angle_from_tracks': get_angle_from_tracks,
  'update_map_center': update_map_center
};
