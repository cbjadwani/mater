import hmac
import uuid

from django.core.cache import cache

from common.models import Device


CACHE_KEY = 'API_TS:%s'


def _create_sig(token, params):
    params_str = ['%s=%s' % (key, val)
                    for key, val in sorted(params.items())]
    # `hmac` doesn't accept unicode
    if isinstance(token, unicode):
        token = token.encode('utf-8')
    sig = hmac.new(token, ''.join(params_str))
    return sig.hexdigest()


def _check_ts(dev_id, ts):
    ''' `ts` is in milliseconds (thousandths) seconds
    '''
    last_ts = cache.get(CACHE_KEY % dev_id)
    if last_ts:
        if ts == last_ts:
            raise ValueError('Repeated ts')
        if ts < last_ts:
            raise ValueError('Older ts')
    cache.set(CACHE_KEY % dev_id, ts, 60 * 60 * 24 * 30)


def check_auth(dev_id, params_dict):
    ''' `ts` is in milliseconds (thousandths) seconds

        returns a tuple of (device, ts) where ts is a float in milliseconds
    '''
    params = dict(params_dict)
    sig = params.pop('sig')
    ts = params['ts']
    ts = int(ts, 16)
    try:
        dev_id = int(dev_id, 16)
    except (TypeError, ValueError):
        dev_id = None
    device = Device.objects.get(pk=dev_id)
    token = device.token

    gen_sig = _create_sig(token, params)
    if sig != gen_sig:
        raise ValueError('Bad sig')
    _check_ts(dev_id, ts)

    return (device, ts / 1000.0)


def gen_token():
    token = uuid.uuid4()
    token = str(token).replace('-', '').upper()
    return token
