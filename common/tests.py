import datetime

from django.test import TestCase

from common.models import Asset, Device, DevLog, Place


class ModelsTests(TestCase):
    def setUp(self):
        self.asset = Asset.objects.create(name='test_asset')
        self.device = Device.objects.create(token='test_token')
        self.asset.install_device(self.device)
        polygon_json = ('[[22.2, 70.77], [22.2, 70.79],'
                        ' [22.3, 70.79], [22.3, 70.77]]')
        self.place = Place.objects.create(
            name='test_place', category=Place.CAT_LM,
            polygon_json=polygon_json)

    def test_pre_delete_place(self):
        log = DevLog.objects.create(
            asset=self.asset, device=self.device,
            timestamp=datetime.datetime.now(), latitude=22.3, longitude=70.78,
            speed=10, event=DevLog.EVENT_ENTER, place=self.place)
        self.place.delete()
        log = DevLog.objects.get(pk=log.pk)
        self.assertIsNone(log.place)
        self.assertEqual(log.event, '')
