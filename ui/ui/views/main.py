from collections import defaultdict
import datetime
import logging
import math
import simplejson as json

from django.core.cache import cache
from django.core.serializers import serialize
from django.db.models import F, Q, Sum, Count

from common.models import DevLog, Asset, Message, Place, Path, EventLog
from common import utils


logformat = '%(asctime)s %(name)s [%(levelname)s] %(message)s'
logging.basicConfig(level=logging.INFO, format=logformat)
logger = logging.getLogger('ui.views.main')


def _get_trips(asset, month_start):
    events = EventLog.objects.filter(
        asset=asset, end_log__timestamp__gte=month_start) \
        .exclude(end_log__asset_state=F('start_log__asset_state')) \
        .select_related('start_log__place', 'start_log__subplace',
                        'end_log__place', 'end_log__subplace')

    events = list(events.order_by('start_log_ts'))
    while events and events[0].end_log.asset_state == Asset.STATE_NONE:
        events.pop(0)
    loaded_trips = []
    unloaded_trips = []
    events.append(None)
    for e1, e2 in zip(events[:-1], events[1:]):
        if e1.end_log.asset_state != Asset.STATE_LOADED:
            unloaded_trips.append([e1.end_log, e2 and e2.end_log])
        else:
            loaded_trips.append([e1.end_log, e2 and e2.end_log])
    assert len(loaded_trips) >= len(unloaded_trips)
    return loaded_trips, unloaded_trips


def _get_average_efficiency(asset):
    cache_key = 'EFFICIENCY:%s' % asset.name
    cache_key = cache_key.replace(' ', '-')
    cached_data = cache.get(cache_key)
    if cached_data:
        cache_ts, efficiency_rows = cached_data
    else:
        cache_ts = 0
        efficiency_rows = []
    now = datetime.datetime.now()
    last_30_days = now.replace(hour=0, minute=0, second=0) - \
        datetime.timedelta(days=29)
    last_30_days = last_30_days - datetime.timedelta(seconds=60 * 60 * 5.5)
    if utils.get_ts(now) - cache_ts > 15 * 60:
        # update data
        new_rows = _asset_efficiency_report(
            asset, time_filter=[last_30_days, None])
    else:
        # filter out old data
        new_rows = [row for row in efficiency_rows
                    if row['exit'] > utils.get_ts(last_30_days)]
    if new_rows != efficiency_rows:
        # update the cache
        cache_ts = utils.get_ts(now)
        efficiency_rows = new_rows
        cached_data = cache_ts, efficiency_rows
        cache.set(cache_key, cached_data, None)
    efficiencies = [float(_get_efficiency_value(row))
                    for row in efficiency_rows]
    if not len(efficiencies):
        avg_efficiency = 60 * 60 * 4.5
    else:
        avg_efficiency = sum(efficiencies) / len(efficiencies)
    return avg_efficiency


def _get_asset_stats(asset, trip_num=None, loaded=None):
    result = {}
    logs = asset.dev_logs.all().annotate(
        num_end_events=Count('end_event_logs'))

    try:
        curr_log = logs.select_related('place', 'subplace').latest()
    except DevLog.DoesNotExist:
        return result

    # Distance travelled in calendar month
    logger.debug('Getting month meters')
    month_start = datetime.datetime.now().replace(
        day=1, hour=0, minute=0, second=0, microsecond=0)
    month_start -= datetime.timedelta(seconds=5.5 * 3600)
    loaded_trips, unloaded_trips = _get_trips(asset, month_start)
    if trip_num is not None:
        if loaded is None:
            return {}
        trips = loaded_trips if loaded else unloaded_trips
        if trip_num == 0 or trip_num > len(trips):
            return {}
        trip_num -= 1
        trip_start_log, trip_end_log = trips[trip_num]
        # trip start log should the log before the exit event
        try:
            l = logs.filter(timestamp__lt=trip_start_log.timestamp).latest()
        except:
            pass
        else:
            trip_start_log = l
        trip_logs = logs.filter(timestamp__gte=trip_start_log.timestamp)
        if trip_end_log is not None:
            trip_logs = trip_logs.filter(timestamp__lte=trip_end_log.timestamp)
        metrics_logs = trip_logs
    else:
        if not loaded_trips:
            event = EventLog.objects.filter(asset=asset) \
                .exclude(start_log__asset_state=F('end_log__asset_state'))[:1]
            if event:
                trip_start_log = event[0].end_log
            else:
                trip_start_log = logs[:250]
                if trip_start_log:
                    trip_start_log = trip_start_log[len(trip_start_log) - 1]
                else:
                    return {}
        else:
            if len(unloaded_trips) < len(loaded_trips):
                assert loaded_trips[-1][1] is None
                trip_start_log = loaded_trips[-1][0]
            else:
                assert unloaded_trips[-1][1] is None
                trip_start_log = unloaded_trips[-1][0]
        trip_end_log = None
        loaded = bool(trip_start_log.asset_state == Asset.STATE_LOADED)
        # trip start log should the log before the exit event
        try:
            l = logs.filter(timestamp__lt=trip_start_log.timestamp).latest()
        except:
            pass
        else:
            trip_start_log = l
        trip_logs = logs.filter(timestamp__gte=trip_start_log.timestamp)
        metrics_logs = logs.filter(timestamp__gte=month_start)

    result['efficiency'] = _get_average_efficiency(asset)
    result['aggregate_meters'] = metrics_logs.aggregate(
        dist=Sum('diff_distance'))['dist']

    #curr_log = logs[0]
    logger.debug('Getting trip points')
    trip_points = [l.float_point for l in trip_logs]
    result['start_place'] = trip_start_log.place_description
    result['start_ts'] = trip_start_log.get_ts()
    result['end_place'] = trip_end_log and trip_end_log.place_description
    result['end_ts'] = trip_end_log and trip_end_log.get_ts()
    result['trip_dist'] = utils.distance_from_latlng_list(trip_points)
    result['curr_place'] = curr_log.place_description
    result['curr_ts'] = curr_log.get_ts()
    result['curr_speed'] = curr_log.speed
    result['ignition'] = curr_log.ignition
    result['loaded'] = loaded
    result['num_trips'] = len(loaded_trips)
    if curr_log.duration > 30 * 60 or \
            (not curr_log.ignition and curr_log.duration > 120):
        result['status'] = 'Parked'
    else:
        now = datetime.datetime.now()
        duration = (now - curr_log.timestamp).total_seconds()
        if duration >= 5 * 60:
            result['status'] = 'Unreachable'
        elif curr_log.speed == 0 and curr_log.duration > 60:
            result['status'] = 'Stopped'
        else:
            result['status'] = 'On Road'

    result['stoppage'] = sum(int(l.duration)
                             for l in trip_logs[:len(trip_logs) - 2])
    result['y'] = curr_log.latitude.to_eng_string()
    result['x'] = curr_log.longitude.to_eng_string()

    if trip_start_log.subplace is not None and \
            trip_start_log.place.category != Place.CAT_DEPOT:
        start_place = trip_start_log.subplace
    else:
        start_place = trip_start_log.place
    violations = []
    logger.debug('Getting trace')
    if start_place is not None:
        trip_logs = list(trip_logs)
        res = utils.get_trace(asset, trip_points[::-1], trip_logs[::-1],
                              start_place, trip_num)
        trip_trace, route, trip_violations, nearest_place, nearest_dist = res
        nearest_place = utils.get_place_description(
            nearest_place[0], nearest_place[1], curr_log.asset_state)
        track = trip_trace
        violations = trip_violations
    else:
        track = [l.float_point for l in logs.all()[:250][::-1]]
        route = []
        nearest_place = ''
        nearest_dist = ''
    all_places = utils.get_all_places(None)
    places = []
    for p in all_places:
        icon_url = p.get_icon_url()
        if icon_url:
            places.append({
                'pk': p.pk,
                'centroid': p.get_centroid(),
                'name': p.name,
                'icon_url': icon_url
            })
    result['track'] = track
    result['route'] = route
    result['nearest_place'] = nearest_place
    result['nearest_dist'] = nearest_dist
    result['violations'] = violations
    result['places'] = places
    return result


def get_asset_stats(user, params=None):
    _s = datetime.datetime.now()

    assets = user.assets.all()
    if params and 'asset' in params:
        try:
            asset_pk = int(params['asset'])
            asset = user.assets.get(pk=asset_pk)
            trip_num = int(params['trip'])
            loaded = int(params['loaded'])
        except:
            return {}
        results = _get_asset_stats(asset, trip_num, loaded)
        return results
    asset_stats = {}

    for asset in assets:
        result = _get_asset_stats(asset)
        if result is not None:
            asset_stats[asset.pk] = result

    _e = datetime.datetime.now()
    _d = _e - _s
    logger.info('get_asset_stats: %s.%06ds' % (_d.seconds, _d.microseconds))
    return asset_stats


def get_messages(user):
    msgs = []
    delta = datetime.timedelta(seconds=30 * 60)
    fresh_threshold = datetime.datetime.now() - delta
    delivered = None
    for message in Message.objects.filter(user=user, delivered=False):
        log = message.log
        if not log.event or log.timestamp < fresh_threshold:
            continue
        msg = utils.short_notification(log)
        msgs.append(msg)
        delivered = True
    if delivered:
        Message.objects.filter(user=user, delivered=False) \
            .update(delivered=True)
    return  msgs


def _get_parkings(user, since=None, long_parking=30, ignition_off_parking=2):
    if since is None:
        last_month = datetime.datetime.now() - datetime.timedelta(days=60)
        since = last_month
    logs = DevLog.objects.filter(asset__in=user.assets.all(),
                                 timestamp__gte=since)
    return utils.get_parked_logs(
        logs, long_parking=long_parking,
        ignition_off_parking=ignition_off_parking)


def get_suggested_places(user):
    parkings = _get_parkings(user, long_parking=30, ignition_off_parking=30)
    parking_points = parkings.values_list('latitude', 'longitude')
    neighbours = utils.group_points(parking_points)

    point_in_poly = lambda p, polygon: utils.point_inside_polygon_inclusive(
        float(p[0]), float(p[1]), polygon)

    places_added = []
    place_parkings = defaultdict(list)
    db_places = Place.objects.all()
    pk = -1  # negative pk as they are not real, only place holder values
    for group in neighbours:
        for place in db_places:
            if all(point_in_poly(p, place.polygon) for p in group):
                break
        else:
            name = 'Suggested Place %d' % (pk * -1)
            poly = utils.get_bounding_box(group, inflate='0.00001')
            poly_json = json.dumps(poly, use_decimal=True)

            lats = [p[0] for p in poly]
            lngs = [p[1] for p in poly]

            place = Place(
                name=name, category='LM', polygon_json=poly_json,
                min_latitude=min(lats), min_longitude=min(lngs),
                max_latitude=max(lats), max_longitude=max(lngs))
            place.pk = pk
            pk -= 1
            places_added.append(place)
            poly = [(float(lat), float(lng)) for lat, lng in poly]
            for p in parkings:
                lat, lng = p.float_point
                if utils.point_inside_polygon_inclusive(lat, lng, poly):
                    place_parkings[place.pk].append({
                        'asset': p.asset.name,
                        'when': p.get_ts(),
                        'duration': p.duration
                    })

    try:
        places_added = serialize('json', places_added)
    except Exception:
        return {'error': 'SerializationError'}
    places_added = json.loads(places_added)
    return {'ok': True, 'added': places_added, 'parkings': place_parkings}


def get_suggested_routes(user):
    parkings = list(
        _get_parkings(user, long_parking=30, ignition_off_parking=10))
    places = {}
    for p in parkings:
        place, subplace = utils.get_place(p)
        if subplace is not None and place.category == Place.CAT_LM:
            place = subplace
        places[p] = place
    assets = defaultdict(list)
    for p in parkings:
        assets[p.asset].append(p)
    parkings = sum((assets[a] for a in assets), [])

    routes = set()
    for park_log1, park_log2 in zip(parkings[:-1], parkings[1:]):
        if not park_log1.asset == park_log2.asset:
            continue
        place1 = places[park_log1]
        place2 = places[park_log2]
        if place1 is None or place2 is None or place1 == place2:
            continue
        if (place1, place2) not in routes and (place2, place1) not in routes:
            routes.add((place1, place2))
    for place1, place2 in routes:
        if not Path.objects.filter(Q(place1=place1, place2=place2) |
                                   Q(place1=place2, place2=place1)).exists():
            points = get_suggested_route(
                user, place1, place2, parkings, places)
            if len(points) < 3:
                continue
            now = datetime.datetime.now()
            route_data = {
                'points': list(points),
                'place1_branches': [],
                'place2_branches': [],
            }
            route_json = json.dumps(route_data, use_decimal=True)
            Path.objects.create(
                place1=place1, place2=place2, map_route_json=route_json,
                default_travel_duration=now, default_stop_duration=now)
    merge_all_routes(user)
    return {'ok': True}


def get_suggested_route(user, place1, place2, parkings, places):
    trips = []
    trip_orders = [(place1, place2), (place2, place1)]

    for park_log1, park_log2 in zip(parkings[:-1], parkings[1:]):
        if park_log1.asset != park_log2.asset:
            continue
        trip_place1 = places[park_log1]
        trip_place2 = places[park_log2]
        trip_order = (trip_place1, trip_place2)
        if trip_order not in trip_orders:
            continue
        trip_logs = DevLog.objects.filter(
            timestamp__lte=park_log1.timestamp,
            timestamp__gte=park_log2.timestamp,
            asset=park_log1.asset)
        trip_points = trip_logs.values_list('latitude', 'longitude')
        if trip_order == trip_orders[1]:
            trip_points = reversed(trip_points)
        trip_points = list(trip_points)
        point_inside_place = lambda p, place: \
            utils.point_inside_polygon_inclusive(
                float(p[0]), float(p[1]), place.polygon)
        for i in range(len(trip_points) - 1, -1, -1):
            if point_inside_place(trip_points[i], place1):
                trip_points = trip_points[i:]
                break
        for i in range(len(trip_points)):
            if point_inside_place(trip_points[i], place2):
                trip_points = trip_points[:i + 1]
                break
        if len(trip_points) > 25:
            trip = [(float(lat), float(lng)) for (lat, lng) in trip_points]
            trips.append(trip)

    if len(trips) <= 1:
        return []
    approx_trip_lengths = []
    for t in trips:
        length = utils.distance_from_latlng_list(t)
        approx_trip_lengths.append(length)
    min_trip_length = min(approx_trip_lengths)
    ref_trip = trips.pop(approx_trip_lengths.index(min_trip_length))
    trips.insert(0, ref_trip)
    route_points = ref_trip[:]
    route_point_weights = [1] * len(route_points)
    dist2_threshold = (30 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    merge_dist2_threshold = (5 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    avg_insert_dist2_threshold = \
        (15 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2

    #if len(trips) == 1:
    #    return route_points
    i = 1
    while i < len(route_points) - 2:
        p = route_points.pop(i)
        p_wt = route_point_weights.pop(i)
        min_i = None
        min_dist2 = None
        min_point = None
        for i in range(1, len(route_points)):
            near_point = utils.point_nearest_line_segment_point(
                p, route_points[i - 1], route_points[i])
            dist2 = utils.dist2(near_point, p)
            if min_dist2 is None or min_dist2 > dist2:
                min_i = i
                min_dist2 = dist2
                min_point = near_point
        assert min_dist2 is not None
        if min_dist2 <= merge_dist2_threshold:
            p1_dist2 = utils.dist2(p, route_points[min_i - 1])
            p2_dist2 = utils.dist2(p, route_points[min_i])
            if p1_dist2 <= merge_dist2_threshold:
                route_point_weights[min_i - 1] += 1
            elif p2_dist2 <= merge_dist2_threshold:
                route_point_weights[min_i] += 1
            else:
                route_point_weights[min_i - 1] += 0.5
                route_point_weights[min_i] += 0.5
        else:
            p_normal = utils.point_normal_base(
                p, route_points[min_i - 1], route_points[min_i])
            if p_normal is not None:
                route_points.insert(min_i, p)
                route_point_weights.insert(min_i, p_wt)
            elif min_point == route_points[min_i - 1]:
                route_points.insert(min_i - 1, p)
                route_point_weights.insert(min_i - 1, p_wt)
            else:
                route_points.insert(min_i + 1, p)
                route_point_weights.insert(min_i + 1, p_wt)
            i += 1

    num_logs = 0
    while num_logs != sum(map(len, trips)):
        num_logs = sum(map(len, trips))
        for trip in trips:
            for p in trip[:]:
                min_i = None
                min_dist2 = None
                for i in range(1, len(route_points)):
                    dist2 = utils._point_dist_from_line_segment_sqr(
                        p, route_points[i - 1], route_points[i])
                    if min_dist2 is None or min_dist2 > dist2:
                        min_i = i
                        min_dist2 = dist2
                if min_dist2 is not None and min_dist2 < dist2_threshold:
                    p1_dist2 = utils.dist2(p, route_points[min_i - 1])
                    p2_dist2 = utils.dist2(p, route_points[min_i])
                    if p1_dist2 <= min_dist2:
                        if p1_dist2 > merge_dist2_threshold:
                            route_points.insert(min_i, p)
                            route_point_weights.insert(min_i, 1)
                        else:
                            wt = route_point_weights[min_i - 1]
                            rp = route_points[min_i - 1]
                            avg_pt = (
                                (p[0] + rp[0] * wt) / (wt + 1),
                                (p[1] + rp[1] * wt) / (wt + 1))
                            route_points[min_i - 1] = avg_pt
                            route_point_weights[min_i - 1] += 1
                    elif p2_dist2 <= min_dist2:
                        if p1_dist2 > merge_dist2_threshold:
                            route_points.insert(min_i, p)
                            route_point_weights.insert(min_i, 1)
                        else:
                            wt = route_point_weights[min_i]
                            rp = route_points[min_i]
                            avg_pt = (
                                (p[0] + rp[0] * wt) / (wt + 1),
                                (p[1] + rp[1] * wt) / (wt + 1))
                            route_points[min_i] = avg_pt
                            route_point_weights[min_i] += 1
                    else:
                        if min_dist2 <= merge_dist2_threshold:
                            route_point_weights[min_i - 1] += 0.5
                            route_point_weights[min_i] += 0.5
                        else:
                            wt = 1
                            if min_dist2 <= avg_insert_dist2_threshold:
                                wt += 1
                            route_points.insert(min_i, p)
                            route_point_weights.insert(min_i, wt)
                    trip.remove(p)
                else:
                    seg_len2 = utils.dist2(route_points[min_i - 1],
                                           route_points[min_i])
                    force_split_threshold = \
                        (300 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
                    if seg_len2 > force_split_threshold and \
                            min_dist2 < (dist2_threshold * 9):
                        route_points.insert(min_i, p)
                        route_point_weights.insert(min_i, 1)
                    trip.remove(p)

    max_log_len = (4500 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    dist2s = [utils.dist2(p1, p2) for p1, p2 in
              zip(route_points[:-1], route_points[1:])]
    if max(dist2s) > max_log_len:
        return []
    remove_small_segments(route_points)
    check_almost_straight(route_points)

    # remove loops
    i = 4
    while i <= len(route_points):
        p1, p2, p3, p4 = route_points[i - 4:i]
        p3_normal = utils.point_normal_base(p3, p1, p2)
        p2_normal = utils.point_normal_base(p2, p3, p4)
        if p2_normal is not None and p3_normal is not None:
            p2_avg = (p2[0] + p2_normal[0]) / 2, (p2[1] + p2_normal[1]) / 2
            p3_avg = (p3[0] + p3_normal[0]) / 2, (p3[1] + p3_normal[1]) / 2
            avg_pt = (p2_avg[0] + p3_avg[0]) / 2, (p2_avg[1] + p3_avg[1]) / 2
            route_points = \
                route_points[:i - 3] + [avg_pt] + route_points[i - 1:]
        else:
            i += 1

    route_points.reverse()
    smoothen_route(route_points)
    route_points.reverse()
    smoothen_route(route_points)

    remove_small_segments(route_points)
    check_almost_straight(route_points)
    return route_points


def merge_all_routes(user):
    places = Place.objects.all()  # will include filter for customer
    all_paths = list(Path.objects.filter(
        Q(place1__in=places) | Q(place2__in=places)))
    places_dict = defaultdict(list)
    for path in all_paths:
        places_dict[path.place1].append(path)
        places_dict[path.place2].append(path)

    for place, paths in places_dict.items():
        if len(paths) > 1:
            merge_routes(place, paths)


def check_almost_straight(route):
    vert_spike_width = (3 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    done = False
    while not done:
        done = True
        i = 0
        while i + 3 <= len(route):
            p1, p2, p3 = route[i:i + 3]
            p1_base = utils.point_normal_base(p1, p2, p3)
            p2_base = utils.point_normal_base(p2, p1, p3)
            p3_base = utils.point_normal_base(p3, p1, p2)
            if p1_base is not None and \
                    utils.dist2(p1, p1_base) <= vert_spike_width:
                route.pop(i)
                done = False
            elif p2_base is not None and \
                    utils.dist2(p2, p2_base) <= vert_spike_width:
                route.pop(i + 1)
                done = False
            elif p3_base is not None and \
                    utils.dist2(p3, p3_base) <= vert_spike_width:
                route.pop(i + 2)
                done = False
            else:
                i += 1


def remove_small_segments(route):
    min_log_length = (5 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    done = False
    while not done:
        done = True
        i = 0
        while i + 2 <= len(route):
            p1, p2 = route[i:i + 2]
            len_p1p2 = utils.dist2(p1, p2)
            if len_p1p2 <= min_log_length:
                p1 = (p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2
                route[i] = p1
                route.pop(i + 1)
                done = False
            else:
                i += 1


def smoothen_route(route):
    smooth_threshold = (5 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    smooth_max_threshold = (40 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2

    # remove spikes
    done = False
    while not done:
        done = True
        i = 0
        while i + 5 <= len(route):
            p1, p2, p3, p4, p5 = route[i:i + 5]
            p2_base = utils.point_normal_base(p2, p1, p5)
            p4_base = utils.point_normal_base(p4, p1, p5)
            if p2_base is not None and p4_base is not None:
                p2_dist2 = utils.dist2(p2, p2_base)
                p4_dist2 = utils.dist2(p4, p4_base)
                if p2_dist2 <= smooth_threshold:
                    p3_base = utils.point_normal_base(p3, p1, p5)
                    p3_dist2 = 0
                    if p3_base is not None:
                        p3_dist2 = utils.dist2(p3, p3_base)
                    if p4_dist2 <= smooth_threshold or \
                            (p3_dist2 <= smooth_max_threshold and
                                p4_dist2 <= smooth_max_threshold):
                        route.pop(i + 1)  # remove p2
                        route.pop(i + 1)  # remove p3
                        route.pop(i + 1)  # remove p4
                        i -= 1  # start again at this point
                        done = False
            i += 1
    # check small deviations over mostly straight paths
    min_stable_seg = (50 / utils.LATLNG_DIFF_TO_METERS_APPROX)
    done = False
    while not done:
        done = True
        i = 0
        while i + 4 <= len(route):
            p1, p2, p3, p4 = route[i:i + 4]
            l1 = math.sqrt(utils.dist2(p1, p2))
            l2 = math.sqrt(utils.dist2(p3, p4))
            if l1 >= min_stable_seg and l2 >= min_stable_seg and \
                    utils.dist2(p2, p3) <= smooth_max_threshold * 2:
                p23_avg = (p2[0] + p3[0]) / 2, (p2[1] + p3[1]) / 2
                p23_normal = utils.point_normal_base(p23_avg, p1, p4)
                if p23_normal is not None and \
                        utils.dist2(p23_avg, p23_normal) <= \
                        smooth_max_threshold * 4:
                    route[i + 1] = p23_avg
                    route.pop(i + 2)
                    done = False
                    i -= 1
            elif l1 >= min_stable_seg and \
                    utils.dist2(p2, p4) <= smooth_max_threshold * 2:
                route.pop(i + 1)  # remove p2
                route.pop(i + 1)  # remove p3
                done = False
                i -= 1
            else:
                #if l1 >= min_stable_seg and \
                #    utils.dist2(p2, p3) <= smooth_max_threshold * 2 and \
                #    utils.dist2(p3, p4) <= smooth_max_threshold * 2:
                #unitvect = (p2[0] - p1[0]) / l1, (p2[1] - p1[1]) / l1
                pass
            i += 1

    done = False
    while not done:
        done = True
        i = 0
        while i + 4 <= len(route):
            p1, p2, p3, p4 = route[i:i + 4]
            p2_normal = utils.point_normal_base(p2, p1, p3)
            #p3_normal = utils.point_normal_base(p3, p2, p4)
            if p2_normal is None:
                if utils.point_normal_base(p2, p3, p4) is not None:
                    # remove spike
                    route.pop(i + 1)
                    i -= 1
                    done = False
            #elif p3_normal is not None:
            #    p2_wt = route_point_weights[i + 1]
            #    p3_wt = route_point_weights[i + 2]
            #    p2_avg = (
            #        (p2[0] * p2_wt * 2 + p2_normal[0]) / (p2_wt * 2 + 1),
            #        (p2[1] * p2_wt * 2 + p2_normal[1]) / (p2_wt * 2 + 1))
            #    p3_avg = (
            #        (p3[0] * p3_wt * 2 + p3_normal[0]) / (p3_wt * 2 + 1),
            #        (p3[1] * p3_wt * 2 + p3_normal[1]) / (p3_wt * 2 + 1))
            #    route[i + 1] = p2_avg
            #    route[i + 2] = p3_avg
            i += 1


def merge_routes(place, paths):
    routes = []
    for path in paths:
        route_data = json.loads(path.map_route_json)
        if route_data:
            routes.append(route_data['points'])
        else:
            routes.append([])
    for route, path in zip(routes, paths):
        if path.place2 == place:
            route.reverse()
    proximity_threshold = (45 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    merge_threshold = (5 / utils.LATLNG_DIFF_TO_METERS_APPROX) ** 2
    # merge pairs of routes

    def get_proximate_path_index(p, route):
        i = 0
        for p1, p2 in zip(route[:-1], route[1:]):
            i += 1
            dist2 = utils._point_dist_from_line_segment_sqr(p, p1, p2)
            if 0.0 < dist2 < proximity_threshold:
                return i
        return None

    def get_latest_merge_point(route, other_route):
        for i, p in reversed(list(enumerate(other_route))):
            if get_proximate_path_index(p, route) is not None:
                return i
        return None

    def get_merge_points():
        merge_points = [None] * len(routes)
        for route_index, route in enumerate(routes):
            route_merge_points = [None] * len(routes)
            for i, other_route in enumerate(routes):
                if i == route_index:
                    continue
                merge_point = get_latest_merge_point(route, other_route)
                route_merge_points[i] = merge_point
            merge_points[route_index] = route_merge_points
        return merge_points

    merge_points = get_merge_points()

    for x in range(10):
        for route_index in range(len(routes)):
            route = routes[route_index]

            # collect common paths of other routes
            for other_route_index in range(len(routes)):
                if other_route_index == route_index:
                    continue
                merge_point = merge_points[route_index][other_route_index]
                if merge_point is None:
                    continue
                other_route = routes[other_route_index]
                other_route = other_route[:merge_point]

                for p_i, p in enumerate(other_route):
                    if p in route:
                        continue
                    min_i = None
                    min_dist2 = None
                    min_point = None
                    for i in range(1, len(route)):
                        nearest_pt = utils.point_nearest_line_segment_point(
                            p, route[i - 1], route[i])
                        dist2 = utils.dist2(p, nearest_pt)
                        if min_dist2 is None or dist2 < min_dist2:
                            min_i = i
                            min_dist2 = dist2
                            min_point = nearest_pt
                    if min_dist2 is None:
                        continue
                    p1 = route[min_i - 1]
                    p2 = route[min_i]
                    if min_dist2 == 0.0:
                        continue
                    elif min_point == p1:
                        if min_dist2 <= merge_threshold:
                            route[min_i - 1] = p1
                        else:
                            avg_pt = ((p[0] + p1[0] * 2) / 3,
                                      (p[1] + p1[1] * 2) / 3)
                            route[min_i - 1] = avg_pt
                    elif min_point == p2:
                        if min_dist2 <= merge_threshold:
                            route[min_i] = p2
                        else:
                            avg_pt = ((p[0] + p2[0] * 2) / 3,
                                      (p[1] + p2[1] * 2) / 3)
                            route[min_i] = avg_pt
                    else:
                        force = min(merge_threshold / min_dist2, 1.0)
                        avg_pt = (min_point[0] * force + p[0] * (1 - force),
                                  min_point[1] * force + p[1] * (1 - force))
                        routes[other_route_index][p_i] = avg_pt
                        diff = (p[0] - min_point[0],
                                p[1] - min_point[1])
                        seg_len = utils.dist2(p1, p2)
                        multiple = seg_len / merge_threshold
                        avg_pt1 = ((diff[0] / multiple + p1[0]),
                                   (diff[1] / multiple + p1[1]))
                        route[min_i - 1] = avg_pt1
                        avg_pt2 = ((diff[0] / multiple + p2[0]),
                                   (diff[1] / multiple + p2[1]))
                        route[min_i] = avg_pt2

    new_routes = []
    for route_index in range(len(routes)):
        route = routes[route_index][:]

        # collect common paths of other routes
        for other_route_index in range(len(routes)):
            if other_route_index == route_index:
                continue
            merge_point = merge_points[route_index][other_route_index]
            if merge_point is None:
                continue
            other_route = routes[other_route_index]
            other_route = other_route[:merge_point]

            p_i = 1
            while p_i < len(route):
                p1 = route[p_i - 1]
                p2 = route[p_i]
                i = 0
                while i < len(other_route):
                    p = other_route[i]
                    p_normal = utils.point_normal_base(p, p1, p2)
                    if p_normal is None:
                        pass
                    elif p_normal in [p1, p2]:
                        other_route.pop(i)
                        i -= 1
                    else:
                        dist2 = utils.dist2(p, p_normal)
                        if dist2 < utils.dist2(p1, p2) / 2 or \
                                dist2 < merge_threshold:
                            route.insert(p_i, p)
                            p_i += 1
                            other_route.pop(i)
                            i -= 1
                    i += 1
                p_i += 1
        # fix start point
        for i in range(len(route) - 1, -1, -1):
            if utils.point_inside_polygon_inclusive(
                    route[i][0], route[i][1], place.polygon):
                break
        route = route[i:]
        # remove very small segments
        remove_small_segments(route)
        # remove perpendicular segments
        check_almost_straight(route)

        route.reverse()
        smoothen_route(route)
        route.reverse()
        smoothen_route(route)

        remove_small_segments(route)
        check_almost_straight(route)
        new_routes.append(route)
    routes = new_routes

    merge_points = get_merge_points()

    for route, path, route_branches in zip(routes, paths, merge_points):
        if path.place2 == place:
            route.reverse()
        route_data = json.loads(path.map_route_json)
        if not route_data:
            # turn empty list to dict
            route_data = {}
        route_data['points'] = route
        branch_points = sorted(filter(None, route_branches))
        if path.place1 == place:
            route_data['place1_branches'] = branch_points
            route_data.setdefault('place2_branches', [])
        else:
            route_data['place2_branches'] = branch_points
            route_data.setdefault('place1_branches', [])
        path.map_route_json = json.dumps(route_data)
        path.save()


def _get_entry_exit_logs(asset, places):
    enter_logs = asset.dev_logs.filter(
        event=DevLog.EVENT_ENTER, place__in=places).select_related('place')
    enter_logs = enter_logs.order_by('timestamp')
    if not enter_logs:
        #return 'no enter logs'
        return []
    first_entry = enter_logs[0]
    exit_logs = asset.dev_logs.filter(
        event=DevLog.EVENT_EXIT, place__in=places,
        timestamp__gt=first_entry.timestamp).select_related('place')
    exit_logs = exit_logs.order_by('timestamp')
    logs = list(enter_logs) + list(exit_logs)
    if len(logs) < 2:
        return []
    logs.sort(key=lambda l: l.timestamp)
    return zip(logs[:-1], logs[1:])


def _get_sub_entry_exit_logs(asset, subplaces):
    enter_logs = asset.dev_logs.filter(
        event=DevLog.EVENT_SUB_ENTER,
        subplace__in=subplaces).select_related('subplace')
    enter_logs = enter_logs.order_by('timestamp')
    if not enter_logs:
        #return 'no enter logs'
        return []
    first_entry = enter_logs[0]
    exit_logs = asset.dev_logs.filter(
        event=DevLog.EVENT_SUB_EXIT, subplace__in=subplaces,
        timestamp__gt=first_entry.timestamp).select_related('subplace')
    exit_logs = exit_logs.order_by('timestamp')
    logs = list(enter_logs) + list(exit_logs)
    if len(logs) < 2:
        return []
    logs.sort(key=lambda l: l.timestamp)
    return zip(logs[:-1], logs[1:])


def get_entry_exit_logs(asset, places):
    _places = places[:]
    places = []
    subplaces = []
    for place in _places:
        if place.is_subplace():
            subplaces.append(place)
        else:
            places.append(place)
    logs = _get_entry_exit_logs(asset, places)
    for l1, l2 in logs:
        if l1.event == DevLog.EVENT_ENTER and \
                l2.event == DevLog.EVENT_EXIT and \
                l1.place == l2.place and \
                l1.subplace == l2.subplace and \
                l1.timestamp < l2.timestamp:
            yield l1, l2
    logs = _get_sub_entry_exit_logs(asset, subplaces)
    for l1, l2 in logs:
        if l1.event == DevLog.EVENT_SUB_ENTER and \
                l2.event == DevLog.EVENT_SUB_EXIT and \
                l1.place == l2.place and \
                l1.subplace == l2.subplace and \
                l1.timestamp < l2.timestamp:
            yield l1, l2


def get_trip_report_data(user):
    data = {}
    # trip num, start, loaded/unloaded, start place, end place, distance,
    # time, parked, avg. speed
    since = datetime.datetime.now() - datetime.timedelta(days=365)
    for asset in user.assets.all():
        logs = asset.dev_logs.all()
        trips = []
        loaded_trips, unloaded_trips = _get_trips(asset, since)
        if loaded_trips and loaded_trips[-1][1] is None:
            loaded_trips.pop()
        if unloaded_trips and unloaded_trips[-1][1] is None:
            unloaded_trips.pop()
        all_start_ts = min(t[0] for t in loaded_trips + unloaded_trips)
        all_end__ts = max(t[1] for t in loaded_trips + unloaded_trips)
        try:
            l = logs.filter(timestamp__lt=all_start_ts.timestamp)\
                    .latest()
        except:
            pass
        else:
            all_start_ts = l
        all_logs = list(logs.filter(timestamp__gte=all_start_ts,
                                    timestamp__lte=all_end__ts)
                        .select_related('place', 'subplace')
                        )
        for i, (trip_start_log, trip_end_log) in enumerate(loaded_trips):
            for l in all_logs:
                if l.timestamp < trip_start_log.timestamp:
                    trip_start_log = l
            start_ts = trip_start_log.timestamp
            end_ts = trip_end_log.timestamp
            #trip_logs = logs.filter(timestamp__gte=trip_start_log.timestamp)
            trip_logs = filter(lambda l: start_ts <= l.timestamp < end_ts, all_logs)
            if trip_start_log.place and \
                    trip_start_log.place.category == Place.CAT_DEPOT:
                start_place = trip_start_log.place.name
            else:
                start_place = trip_start_log.place_description
            if trip_end_log.place and \
                    trip_end_log.place.category == Place.CAT_DEPOT:
                end_place = trip_end_log.place.name
            else:
                end_place = trip_end_log.place_description

            distance = 0
            parked = 0
            for l in trip_logs:
                distance += l.diff_distance
                parked += l.duration
            total_time = trip_end_log.get_ts() - trip_start_log.get_ts()
            avg_speed = None
            if total_time - parked > 0:
                avg_speed = distance * 3.6 / (total_time - parked)
            trips.append({
                'trip_num': i + 1,
                'start_ts': trip_start_log.get_ts(),
                'end_ts': trip_end_log.get_ts(),
                'state': 'L',
                'start_place': start_place,
                'end_place': end_place,
                'distance': distance,
                'parked': parked,
                'total_time': total_time,
                'avg_speed': avg_speed,
            })
        for i, (trip_start_log, trip_end_log) in enumerate(unloaded_trips):
            for l in all_logs:
                if l.timestamp < trip_start_log.timestamp:
                    trip_start_log = l
            start_ts = trip_start_log.timestamp
            end_ts = trip_end_log.timestamp
            #trip_logs = logs.filter(timestamp__gte=trip_start_log.timestamp)
            trip_logs = filter(lambda l: start_ts <= l.timestamp < end_ts, all_logs)
            if trip_start_log.place and \
                    trip_start_log.place.category == Place.CAT_DEPOT:
                start_place = trip_start_log.place.name
            else:
                start_place = trip_start_log.place_description
            if trip_end_log.place and \
                    trip_end_log.place.category == Place.CAT_DEPOT:
                end_place = trip_end_log.place.name
            else:
                end_place = trip_end_log.place_description

            distance = 0
            parked = 0
            for l in trip_logs:
                distance += l.diff_distance
                parked += l.duration
            total_time = trip_end_log.get_ts() - trip_start_log.get_ts()
            avg_speed = None
            if total_time - parked > 0:
                avg_speed = distance * 3.6 / (total_time - parked)
            trips.append({
                'trip_num': i + 1,
                'start_ts': trip_start_log.get_ts(),
                'end_ts': trip_end_log.get_ts(),
                'state': 'U',
                'start_place': start_place,
                'end_place': end_place,
                'distance': distance,
                'parked': parked,
                'total_time': total_time,
                'avg_speed': avg_speed,
            })
        data[asset.name] = trips
    return data


def get_place_report_data(user):
    assets = {}
    report = []
    places = Place.objects.filter(
        category__in=[Place.CAT_STOP, Place.CAT_DEPOT])
    assets = {a.pk: a.name for a in user.assets.all()}
    place_names = []
    for asset in user.assets.all():
        for l1, l2 in get_entry_exit_logs(asset, places):
            place_name = l1.place_description
            if place_name not in place_names:
                place_names.append(place_name)
            report.append([
                place_names.index(place_name),
                asset.pk,
                utils.get_ts(l1.first_ts or l1.timestamp),
                utils.get_ts(l2.first_ts or l2.timestamp),
            ])
    report.sort(key=lambda line: line[1])
    return {
        'places': dict(enumerate(place_names)),
        'assets': assets,
        'report': report,
    }


def get_asset_lifelog_report_data(user):
    assets = {a.pk: {'name': a.name, 'report': []} for a in user.assets.all()}
    event_logs = EventLog.objects.filter(asset__in=user.assets.all()) \
        .select_related('start_log__place', 'start_log__subplace',
                        'end_log__place', 'end_log__subplace') \
        .order_by('start_log__timestamp')
    for e in event_logs:
        assets[e.asset_id]['report'].append([
            utils.get_ts(e.start_log.first_ts or e.start_log.timestamp),
            utils.get_ts(e.end_log.first_ts or e.end_log.timestamp),
            e.event_description,
            e.event_details,
            e.distance / 1000.0,
        ])
    return assets


def select_user_asset(user, asset_id):
    asset = user.assets.get(pk=asset_id)
    profile = user.profile
    profile.selected_asset = asset
    profile.save()


def _supply_efficiency_report_row(logs):
    '''
        logs:   list of all logs starting from Depot entry event to
                Depot exit event.
                Must be in _ascending_ timestamp order.
        :returns:
            enter: ts,   # depot parking entry ts
            parked: time_driven = 0, time_total, dist = 0
            token: time_driven, time_total, dist
            gantry: time_driven, time_total, dist
            billing: time_driven, time_total, dist
            exit: ts
    '''
    ret = {}
    ret['internalTravel'] = 0
    logs_iter = iter(logs)
    # enter
    try:
        log = next(logs_iter)
    except StopIteration:
        logger.error('No logs')
        return {}
    else:
        if log.event != DevLog.EVENT_ENTER:
            logger.error('Depot enter log not found')
        ts = log.first_ts or log.timestamp
        ret['enter'] = utils.get_ts(ts)

    # Internal travel - before entering
    time_total_acc = log.duration
    for log in logs_iter:
        if log.event == DevLog.EVENT_SUB_ENTER:
            ret['internalTravel'] += time_total_acc
            break
        time_total_acc += log.diff_time
    else:
        logger.error('No parking entry log')

    # parking - after entering
    time_total_acc = log.diff_time
    for log in logs_iter:
        if log.subplace is not None and \
                log.subplace.category == Place.CAT_DEPOT_PARK and \
                log.event != DevLog.EVENT_SUB_EXIT:
            time_total_acc += log.diff_time
        else:
            ret['parked'] = [time_total_acc, time_total_acc, 0]
            break
    else:
        logger.error('Error find parked')
        return {}
    # token
    time_parked_acc = log.duration
    time_total_acc = log.diff_time
    dist_acc = log.diff_distance
    for log in logs_iter:
        if log.event == DevLog.EVENT_SUB_EXIT or \
                (log.subplace is not None and
                 log.subplace.category != Place.CAT_DEPOT_BILL):
            ret['token'] = [time_total_acc, time_parked_acc, dist_acc]
            break
        time_parked_acc += log.duration
        time_total_acc += log.diff_time
        dist_acc += log.diff_distance
    else:
        logger.error('Error find token')
        return {}
    # loading
    time_parked_acc = log.duration
    time_total_acc = log.diff_time
    dist_acc = log.diff_distance
    for log in logs_iter:
        if log.asset_state == Asset.STATE_LOADED and \
                log.event == DevLog.EVENT_SUB_EXIT:
            ret['gantry'] = [time_total_acc, time_parked_acc, dist_acc]
            break
        time_parked_acc += log.duration
        time_total_acc += log.diff_time
        dist_acc += log.diff_distance
    else:
        logger.error('Error find gantry data')
        return {}
    # billing
    time_parked_acc = log.duration
    time_total_acc = log.diff_time
    dist_acc = log.diff_distance
    for log in logs_iter:
        if log.event == DevLog.EVENT_SUB_EXIT or \
                (log.subplace is not None and
                    log.subplace.category != Place.CAT_DEPOT_BILL):
            ret['billing'] = [time_total_acc, time_parked_acc, dist_acc]
            break
        time_parked_acc += log.duration
        time_total_acc += log.diff_time
        dist_acc += log.diff_distance
    else:
        logger.error('Error find billing data')
        return {}
    # exit
    time_parked_acc = log.duration
    time_total_acc = log.diff_time
    dist_acc = log.diff_distance
    for log in logs_iter:
        if log.event == DevLog.EVENT_EXIT:
            # Only add time taken to travel out of area.
            time_total_acc += (log.diff_time - log.duration)
            ret['internalTravel'] += time_total_acc - time_parked_acc
            ret['billing'][0] += time_parked_acc
            ret['billing'][1] += time_parked_acc
            break
        time_parked_acc += log.duration
        time_total_acc += log.diff_time
        dist_acc += log.diff_distance
    else:
        logger.error('Error finding Depot exit logs')
        return {}
    ts = log.first_ts or log.timestamp
    ret['exit'] = utils.get_ts(ts)
    return ret


def _get_efficiency_value(row):
    row = row.copy()
    place_duration = row['exit'] - row['enter']
    total_time = 0.0
    # Use only parking time for token and add travelling to `internalTravel`
    row['internalTravel'] += row['token'][0] - row['token'][1]

    total_time += row['parked'][0]
    total_time += row['token'][1]
    total_time += row['gantry'][0]
    total_time += row['billing'][0]
    total_time += row['internalTravel']

    row['internalTravel'] += place_duration - total_time
    row['efficiency'] = place_duration - row['parked'][0]
    return row['efficiency']


def _asset_efficiency_report(asset, time_filter=None):
    report_rows = []
    logs = DevLog.objects.filter(
        asset=asset, place__category=Place.CAT_DEPOT)
    if time_filter and time_filter[0]:
        logs = logs.filter(timestamp__gte=time_filter[0])
    if time_filter and time_filter[1]:
        logs = logs.filter(timestamp__lte=time_filter[1])
    logs = logs.order_by('-timestamp')   # descending order
    logs = logs.select_related('place', 'subplace')
    row_logs = []
    for log in logs:
        if row_logs:
            row_logs.append(log)
            if log.event == DevLog.EVENT_ENTER:
                row_logs.reverse()
                report_row = _supply_efficiency_report_row(row_logs)
                if report_row:
                    report_row['asset'] = asset.name
                    report_rows.append(report_row)
                row_logs = []
        elif log.event == DevLog.EVENT_EXIT:
            row_logs = [log]
    return report_rows


def supply_efficiency_report(user, time_filter=None):
    '''
    data:
        asset: -> [rows]:
            enter: ts,   # depot parking entry ts
            parked: time_driven = 0, time_total, dist = 0
            token: time_driven, time_total, dist
            gantry: time_driven, time_total, dist
            billing: time_driven, time_total, dist
            exit: ts
    '''
    assets = user.assets.all()
    assets_dict = {}
    report_rows = []
    report = {}
    for asset in assets:
        assets_dict[asset.pk] = asset.name
        rows = _asset_efficiency_report(asset, time_filter)
        report_rows.extend(rows)
    report['rows'] = report_rows
    report['assets'] = assets_dict
    return report
