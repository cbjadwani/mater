from collections import namedtuple
import datetime
from decimal import Decimal
import httplib
import logging
import simplejson as json
import texttable

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import force_escape, linebreaksbr
from django.views.decorators.http import require_http_methods


from common.models import DevLog, Device, Asset, Message, Place, EventLog, \
    AssetStateTransition
from common import utils
from api.auth import check_auth


MIN_LOG_DISTANCE = 5  # in meters
STOPPED_MIN_LOG_DISTANCE = 300  # in meters
MIN_LOG_SPEED = 2 / 3.6  # in meters per second
MAX_IGNORE_DISTANCE = 50  # in meters
MAX_LOG_DISTANCE = 500  # in meters
PARKING_DURATION = 5 * 60  # in seconds
LONG_PARKING_DURATION = 30 * 60  # in seconds
CONTINUOUS_MIN_SPEED = 10.0  # in km/h
PLACE_EXIT_DIST_THRESHOLD = 300  # in meters
PARKING_VIOLATION_THRESHOLD = 15 * 60   # seconds
SPEED_THRESHOLD = 55
MIN_DATA_PARAMS = (
    'dev_id counter lat lng ts tot_dist asset_status '
    'alarm_code gps_speed gps_avg_speed')
TYP_DATA_PARAMS = MIN_DATA_PARAMS + ' ' + (
    'digital_IO ADC0 ADC1 ADC2 ADC3 ADC4 door_count temperature '
    'acc_max_x acc_min_x acc_max_y acc_min_y acc_max_z acc_min_z '
    'cell_id cell_lac primary_tower')
CONFIG_PARAMS = ('vehicle_Id n1 n2 n3 password sos_no apn apn_user apn_pass '
                 'x7 x8 x9 x10 x11 x12 x13 x14 x15 x16 x17 x18 x19 x20 x21 '
                 'x22 server_url port ctrl_sleep sleep_delay x27 post_format '
                 'post_rate stealth_mode snooze_interval '
                 'snooze_sms_listen_time sleep_source_select io_port_control')
AGPSMinData = namedtuple('AGPSMinData', MIN_DATA_PARAMS)
AGPSTypData = namedtuple('AGPSTypData', TYP_DATA_PARAMS)
AGPSConfig = namedtuple('AGPSConfig', CONFIG_PARAMS)


logformat = '%(asctime)s %(name)s [%(levelname)s] %(message)s'
logging.basicConfig(level=logging.INFO, format=logformat)
logger = logging.getLogger('MaterUI')


def _logs_are_non_continuous(oldlog, newlog, dist):
    # TODO: user diff_distance, diff_time
    if dist is not None:
        duration = (newlog.timestamp - oldlog.timestamp).total_seconds()
        speed = (dist / duration) if duration else 0     # m/s
        speed = float(speed) * 3.6  # km/h
        if dist > MAX_LOG_DISTANCE and duration > PARKING_DURATION and \
                speed < CONTINUOUS_MIN_SPEED:
            return True
    return False


def add_log(asset, device, lat, lng, ts, speed, ignition=None):
    if not asset == device.current_asset:
        raise Exception
    diff_time = None
    diff_distance = None
    latest_log = None
    did_not_move = False
    asset_state = Asset.STATE_NONE
    try:
        latest_log = DevLog.objects.filter(device=device, asset=asset).latest()
    except DevLog.DoesNotExist:
        pass
    else:
        asset_state = latest_log.asset_state
        diff_time = (ts - latest_log.timestamp).total_seconds()
        if diff_time <= 0:
            return
        diff_distance = utils.distance_from_latlng(
            latest_log.latitude, latest_log.longitude, lat, lng)
        min_speed = min(speed / 3.6,      # meters per second
                        diff_distance / diff_time)
        min_log_distance = STOPPED_MIN_LOG_DISTANCE \
            if not (ignition or latest_log.ignition) else MIN_LOG_DISTANCE
        if diff_distance < min_log_distance or \
                (min_speed < MIN_LOG_SPEED and
                 diff_distance < MAX_IGNORE_DISTANCE):
            did_not_move = True

    if did_not_move:
        prev_duration = latest_log.duration
        latest_log.speed = 0
        if not latest_log.first_ts:
            latest_log.first_ts = latest_log.timestamp
        latest_log.ignition = ignition and latest_log.ignition
        if latest_log.diff_time is None:
            latest_log.diff_time = 0
        latest_log.diff_time += (ts - latest_log.timestamp).total_seconds()
        latest_log.timestamp = ts
        if prev_duration <= PARKING_VIOLATION_THRESHOLD < latest_log.duration:
            _check_violations(latest_log, None)
            if str(DevLog.VIOLAT_PARK) in latest_log.violations:
                #_send_notifications(latest_log)
                #_add_event_log(latest_log)
                pass
        if latest_log.place is None:
            save_state = latest_log.asset_state
            _check_asset_transition(latest_log, None)
            if latest_log.asset_state != save_state:
                _add_event_log(latest_log)
        latest_log.save()
    else:
        new_log = DevLog(
            device=device, asset=asset, timestamp=ts,
            latitude=lat, longitude=lng, speed=speed, ignition=ignition,
            asset_state=asset_state, diff_time=diff_time,
            diff_distance=diff_distance)
        _save_new_log(new_log, latest_log, diff_distance)


def _save_new_log(new_log, latest_log, distance_from_last_log):
    if _logs_are_non_continuous(latest_log, new_log, distance_from_last_log):
        parking_duration = datetime.timedelta(seconds=PARKING_DURATION)
        t1 = new_log.timestamp - parking_duration
        seconds = distance_from_last_log * 3.6 / CONTINUOUS_MIN_SPEED
        t2 = latest_log.timestamp + datetime.timedelta(seconds=seconds)
        new_log.first_ts = min(t1, t2)
    latest_place = (None, None)
    if latest_log:
        latest_place = (latest_log.place, latest_log.subplace)
    current_place = utils._get_place(new_log.float_point)
    if latest_place != current_place:
        # place changed
        if current_place[0] != latest_place[0]:
            if current_place[0] is not None:
                # enter
                new_log.place = current_place[0]
                new_log.subplace = current_place[1]
                new_log.event = DevLog.EVENT_ENTER
                if latest_place[0] is not None and \
                        latest_place[0].category != Place.CAT_LM:
                    # enter directly from one place to another, no exit event
                    latest_log = _create_exit_log(new_log, latest_log)
            else:
                # exit
                place_dist = new_log.approx_dist_from_place(latest_place[0])
                if latest_place[0].category == Place.CAT_LM:
                    if place_dist < PLACE_EXIT_DIST_THRESHOLD:
                        new_log.place = latest_place[0]
                        new_log.subplace = latest_place[1]
                elif latest_log.event != DevLog.EVENT_EXIT:
                    new_log.place = latest_place[0]
                    new_log.subplace = latest_place[1]
                    if place_dist > PLACE_EXIT_DIST_THRESHOLD:
                        new_log.event = DevLog.EVENT_EXIT
        # subplace changed
        elif current_place[1] != latest_place[1]:
            new_log.place = latest_place[0]
            if current_place[1] is not None:
                # enter
                new_log.subplace = current_place[1]
                new_log.event = DevLog.EVENT_SUB_ENTER
                if latest_place[1] is not None and \
                        latest_place[1].category != Place.CAT_LM:
                    # enter directly from one place to another, no exit event
                    latest_log = _create_exit_log(new_log, latest_log)
            else:
                # exit
                place_dist = new_log.approx_dist_from_place(latest_place[1])
                if latest_place[1].category == Place.CAT_LM:
                    if place_dist < PLACE_EXIT_DIST_THRESHOLD:
                        new_log.subplace = latest_place[1]
                elif latest_log.event != DevLog.EVENT_SUB_EXIT:
                    new_log.subplace = latest_place[1]
                    if place_dist > PLACE_EXIT_DIST_THRESHOLD:
                        new_log.event = DevLog.EVENT_SUB_EXIT
        else:
            assert False
    else:
        new_log.place = latest_place[0]
        new_log.subplace = latest_place[1]

    # handle transitions
    if new_log.event:
        _check_asset_transition(new_log, latest_log)

    # violations
    _check_violations(new_log, latest_log)

    new_log.save()
    if new_log.event or \
            (latest_log and latest_log.violations != new_log.violations):
        _add_event_log(new_log)

    if new_log.event:
        _send_notifications(new_log)


def _create_exit_log(new_log, latest_log):
    latest_place = (None, None)
    if latest_log:
        latest_place = (latest_log.place, latest_log.subplace)
    exit_log = DevLog()
    for f in new_log._meta.fields:
        val = getattr(new_log, f.name)
        setattr(exit_log, f.name, val)
    exit_log.id = None
    exit_log.place = latest_place[0]
    exit_log.subplace = latest_place[1]
    exit_log.timestamp -= datetime.timedelta(seconds=1)    # HACK!
    if exit_log.diff_time:
        exit_log.diff_time -= 1
    if exit_log.subplace is not None:
        exit_log.event = DevLog.EVENT_SUB_EXIT
    else:
        exit_log.event = DevLog.EVENT_EXIT
    new_log.diff_time = 1

    _check_asset_transition(exit_log, latest_log)
    _check_violations(exit_log, latest_log)
    exit_log.save()
    _add_event_log(exit_log)
    new_log.asset_state = exit_log.asset_state

    if exit_log.event:
        _send_notifications(exit_log)
    return exit_log


def _check_asset_transition(new_log, latest_log):
    stay_duration = 0
    if new_log.event in [DevLog.EVENT_EXIT, DevLog.EVENT_SUB_EXIT]:
        enter_log = utils.get_enter_log(new_log)
        if enter_log:
            enter_ts = (enter_log.first_ts or enter_log.timestamp)
            exit_ts = (new_log.first_ts or new_log.timestamp)
            stay_duration = (exit_ts - enter_ts).total_seconds()
    elif new_log.place is None:
        if new_log.first_ts is None:
            return
        stay_duration = new_log.duration
    if new_log.place and new_log.place.category == Place.CAT_CITY and \
            new_log.subplace is None:
        place = None
    else:
        place = new_log.subplace or new_log.place
    if place is not None:
        attrs = place.attrs or ''
        place_attrs = [attr.strip() for attr in attrs.split(';')]
    else:
        place_attrs = ['*NO PLACE*']

    transition = AssetStateTransition.objects.filter(
        asset_type_id=new_log.asset.type_id,
        place_attr__in=place_attrs,
        initial_state=new_log.asset_state or Asset.STATE_NONE,
        trigger_duration__lte=stay_duration)[:1]

    if transition:
        transition = transition[0]
        new_log.asset_state = transition.final_state


def _check_violations(new_log, latest_log):
    violations = []
    speed_threshold = new_log.asset.speed_threshold or SPEED_THRESHOLD
    if new_log.speed > speed_threshold:
        violations.append(DevLog.VIOLAT_OVERSPEED)
    if new_log.subplace is not None and new_log.place.category == Place.CAT_LM:
        new_log_place = new_log.subplace
    else:
        new_log_place = new_log.place
    if new_log_place is None or \
            new_log_place.category not in [Place.CAT_STOP, Place.CAT_DEPOT]:
        if new_log.duration > PARKING_VIOLATION_THRESHOLD:
            violations.append(DevLog.VIOLAT_PARK)
    new_log.violations = ','.join(map(str, violations))


def _send_notifications(new_log):
    asset = new_log.asset
    for user in asset.users.all():
        Message.objects.create(user=user, log=new_log)

    msg = utils.short_notification(new_log)
    if msg is None:
        return
    subject = '[Mater] %s' % msg
    timezonediff = datetime.timedelta(seconds=5.5 * 60 * 60)
    ts = new_log.timestamp + timezonediff
    timestr = ts.strftime('%I:%M%p %d/%m/%y')
    body = '%s at %s' % (msg, timestr)
    other_stats = _get_notification_stat(new_log)
    body += '\n\n%s' % other_stats
    recipients = asset.users.values_list('email', flat=True)
    recipients = filter(None, recipients)
    mail = EmailMultiAlternatives(
        subject, body, settings.DEFAULT_FROM_EMAIL, recipients)
    html_body = ('<pre style="font-family: Courier, Courier New">%s</pre>' %
                 linebreaksbr(force_escape(body)))
    mail.attach_alternative(html_body, 'text/html')
    mail.send(fail_silently=True)
    logger.info('Sent Email %r to %r' % (subject, recipients))


def _get_notification_stat(new_log):
    asset = new_log.asset

    def get_logs(since):
        enter_event = DevLog.EVENT_ENTER
        exit_event = DevLog.EVENT_EXIT
        if new_log.subplace:
            enter_event = DevLog.EVENT_SUB_ENTER
            exit_event = DevLog.EVENT_SUB_EXIT
        enter_logs = asset.dev_logs.filter(
            event=enter_event, place=new_log.place,
            timestamp__gte=since)
        if new_log.subplace is not None:
            enter_logs = enter_logs.filter(subplace=new_log.subplace)
        enter_logs = enter_logs.order_by('timestamp')
        if not enter_logs:
            #return 'no enter logs'
            return
        first_entry = enter_logs[0]
        exit_logs = asset.dev_logs.filter(
            event=exit_event, place=new_log.place,
            timestamp__gt=first_entry.timestamp)
        if new_log.subplace is not None:
            exit_logs = exit_logs.filter(subplace=new_log.subplace)
        exit_logs = exit_logs.order_by('timestamp')
        logs = list(enter_logs) + list(exit_logs)
        if len(logs) < 2:
            return
        logs.sort(key=lambda l: l.timestamp)
        for l1, l2 in zip(logs[:-1], logs[1:]):
            if l1.event == enter_event and \
                    l2.event == exit_event and \
                    l1.timestamp < l2.timestamp:
                yield l1, l2

    if new_log.event in [DevLog.EVENT_EXIT, DevLog.EVENT_SUB_EXIT]:
        today = datetime.datetime.now().replace(
            hour=0, minute=0, second=0, microsecond=0)
        week = today - datetime.timedelta(days=6)
        month = today - datetime.timedelta(days=29)
        alltime = datetime.datetime.fromordinal(1)
        sums = {
            today: [0, 0],
            week: [0, 0],
            month: [0, 0],
            alltime: [0, 0],
        }
        ts = asset.current_installation.timestamp
        for enter_log, exit_log in get_logs(ts):
            stay = (exit_log.timestamp - enter_log.timestamp).total_seconds()
            if stay < 0:
                return ''
            for key in sums:
                if enter_log.timestamp >= key:
                    sums[key][0] += stay
                    sums[key][1] += 1
        table = texttable.Texttable()
        table.set_deco(texttable.Texttable.HEADER)
        table.header(['When', 'Number\nof Entries', 'Average Stay\n(HH:MM)'])
        table.set_cols_align(['l', 'r', 'r'])
        labels = 'Today', 'Last 7 Days', 'Last 30 Days', 'All Time'
        keys = today, week, month, alltime
        for label, key in zip(labels, keys):
            total_stay, count_entries = sums[key]
            if count_entries:
                average_stay = total_stay // count_entries
                average_stay = datetime.timedelta(seconds=average_stay)
                average_stay = utils.strftimedelta('%H:%M', average_stay)
            else:
                average_stay = '--:--'
            table.add_row([label, count_entries, average_stay])
        text_table = table.draw()
        return text_table
    elif new_log.event in [DevLog.EVENT_ENTER, DevLog.EVENT_SUB_ENTER]:
        last_month = datetime.datetime.now() - datetime.timedelta(days=30)
        stays = []
        for enter_log, exit_log in get_logs(last_month):
            stay = (exit_log.timestamp - enter_log.timestamp).total_seconds()
            if stay < 0:
                return ''
            stays.append(stay)
        if not stays:
            return ''
        trunc_mean = utils.ftrunc_mean(stays)
        trunc_mean = datetime.timedelta(seconds=trunc_mean)
        timezonediff = datetime.timedelta(seconds=5.5 * 60 * 60)
        expected_ttl = new_log.timestamp + trunc_mean + timezonediff
        return 'Expected to leave at %s' % expected_ttl.strftime(
            '%I:%M%p %d/%m/%Y')
    else:
        assert False


def _add_event_log(new_log):
    asset = new_log.asset
    try:
        latest_event_log = EventLog.objects.filter(asset=asset).latest()
    except EventLog.DoesNotExist:
        EventLog.objects.create(
            asset=asset, start_log=new_log, end_log=new_log, distance=0,
            start_log_ts=new_log.timestamp)
        return

    start_log = latest_event_log.end_log
    end_log = new_log
    if start_log == end_log:  # Hack !
        return
    logs = asset.dev_logs.filter(
        timestamp__gte=start_log.timestamp, timestamp__lte=end_log.timestamp)
    distance = utils.distance_from_latlng_list([l.float_point for l in logs])
    if latest_event_log.start_log == latest_event_log.end_log:
        latest_event_log.end_log = new_log
        latest_event_log.distance = distance
        latest_event_log.save()
        return
    if start_log.event == DevLog.EVENT_SUB_EXIT and \
            end_log.event == DevLog.EVENT_EXIT and \
            start_log.place_id == end_log.place_id:
        return
    if start_log.event == DevLog.EVENT_ENTER and \
            start_log.place == new_log.place:
        if new_log.event == DevLog.EVENT_EXIT:
            if not any(l.speed == 0 for l in logs) and \
                    len(set(l.place_id for l in logs)) == 1:
                return
        replace_old_event = False
        if new_log.event == DevLog.EVENT_SUB_ENTER:
            replace_old_event = True
        elif not new_log.event and \
                str(DevLog.VIOLAT_PARK) in new_log.violations:
            replace_old_event = True
        if replace_old_event:
            latest_event_log.end_log = new_log
            latest_event_log.distance += distance
            latest_event_log.save()
            return
    EventLog.objects.create(
        asset=asset, start_log=start_log, end_log=end_log, distance=distance,
        start_log_ts=start_log.timestamp)


def root(request):
    raw_data = request.GET.get('q')
    if raw_data:
        raw_data = raw_data.strip()
    if raw_data is None or \
            not (raw_data.startswith('>') and raw_data.endswith('<')):
        return HttpResponse(content='404 NOT FOUND', status=httplib.NOT_FOUND)
    try:
        data = raw_data[1:-1].split('|')
        token = data[0]
        device = Device.objects.get(token=token)
    except Exception:
        #import  traceback
        #traceback.print_exc()
        return HttpResponse(content='404 NOT FOUND', status=httplib.NOT_FOUND)
    try:
        try:
            data = AGPSMinData(*data)
        except Exception:
            try:
                data = AGPSTypData(*data)
            except Exception:
                data = AGPSConfig(*data)
        if not isinstance(data, (AGPSMinData, AGPSTypData)):
            raise Exception('This is to skip creating new log')
        lat = round(Decimal(data.lat), 5)
        lng = round(Decimal(data.lng), 5)
        ts = datetime.datetime.strptime(data.ts, '%d%m%y%H%M%S')
        speed_knots = data.gps_speed
        speed = int(round(float(speed_knots) * 1.852))
        ignition = None
        if hasattr(data, 'digital_IO'):
            # 3rd bit of digital_IO is complementary of ignition status
            digital_IO = int(data.digital_IO)
            if device.token == '353451048517989':
                ignition = bool(digital_IO & 0x0010)
            else:
                ignition = not bool(digital_IO & 0x0004)
        if hasattr(data, 'acc_max_x'):
            if data.acc_max_x == '0' and data.acc_max_y == '0' and \
                    data.acc_max_z == '0' and speed <= 15:
                raise Exception("Don't add log")
    except Exception:
        pass
    else:
        add_log(device.current_asset, device, lat, lng, ts, speed,
                ignition=ignition)

    response = None
    # Commands to be sent to the device are put in
    # ``device.command`` with one command per line/request
    if device.command:
        # If the device has sent us the config, the next command
        # is probably a JSON dict of the config fields to update
        commands = device.command.splitlines()
        response = commands.pop(0).strip()
        if isinstance(data, AGPSConfig):
            try:
                data = data._asdict()
                data.update(json.loads(response))
                data = data.values()
                data = '|'.join(data[0:1] + data[4:])
            except Exception:
                pass
            else:
                response = ':>F|07|02|%s|G;' % data
        device.command = '\n'.join(c.strip() for c in commands)
        device.save()
    if not response:
        response = ':AGPRSOKB;'
    return HttpResponse(content=response, status=httplib.OK)


def log(request, dev_id):
    params = request.REQUEST
    try:
        (device, ts) = check_auth(dev_id, params)
    except Device.DoesNotExist:
        raise Http404
    except (KeyError, ValueError) as e:
        #import traceback
        #traceback.print_exc()
        return HttpResponse(content=str(e), status=httplib.BAD_REQUEST)

    try:
        lat = Decimal(params['y'])
        lon = Decimal(params['x'])
        speed = int(round(float(params['s'])))
    except KeyError:
        return HttpResponse(status=httplib.BAD_REQUEST)

    if not (-90.0 <= lat <= 90.0) or \
            not (-180.0 <= lon <= 180.0) or \
            not (0 <= speed <= 150):
        return HttpResponse(status=httplib.BAD_REQUEST)

    asset = device.current_asset
    ts = datetime.datetime.utcfromtimestamp(ts)
    add_log(asset, device, lat, lon, ts, speed)
    return HttpResponse(content='OK', status=httplib.OK)


def stopped(request, dev_id):
    params = request.REQUEST
    try:
        (device, ts) = check_auth(dev_id, params)
    except Device.DoesNotExist:
        raise Http404
    except (KeyError, ValueError):
        return HttpResponse(status=httplib.BAD_REQUEST)

    asset = device.current_asset
    ts = datetime.datetime.utcfromtimestamp(ts)

    try:
        latest_log = device.logs.latest()
    except DevLog.DoesNotExist:
        latest_log = None
    if latest_log is None or latest_log.asset != asset:
        return HttpResponse(status=httplib.BAD_REQUEST)

    lat = latest_log.latitude
    lon = latest_log.longitude
    speed = latest_log.speed
    DevLog.objects.create(
        device=device, asset=asset, timestamp=ts, latitude=lat, longitude=lon,
        speed=speed)

    return HttpResponse(content='OK', status=httplib.OK)


def convert_nmea_coords(value):
    value = float(value)
    m, s = divmod(value, 100)
    return Decimal(m + s / 60)


@require_http_methods(["GET", "POST"])
def opengts(request):
    query = dict(request.REQUEST)
    try:
        params = query.pop('gprmc')
        dev_id = query.pop('dev')
        dev_id = int(dev_id, 16)
        asset_id = query.pop('acct')
        asset_id = int(asset_id)
    except (KeyError, ValueError, TypeError) as e:
        return HttpResponse(content=str(e), status=httplib.BAD_REQUEST)
    device = get_object_or_404(Device, pk=dev_id)
    asset = get_object_or_404(Asset, pk=asset_id)
    if asset.current_device != device:
        return HttpResponse(
            content='Bad asset/device', status=httplib.BAD_REQUEST)

    #print params.split(',')[1:], query

    # HHMMSS, ok, lat, N/S, lon, W/E, speed, bearing, time, ?, cksum
    try:
        t, ok, lat, lat_h, lon, lon_h, s, b, d, _, c = params.split(',')[1:]
        ts = datetime.datetime.strptime(t + d, '%H%M%S%d%m%y')
        lat = convert_nmea_coords(lat)
        if lat_h == 'S':
            lat *= -1
        elif lat_h != 'N':
            return HttpResponse(status=httplib.BAD_REQUEST)
        lon = convert_nmea_coords(lon)
        if lon_h == 'W':
            lon *= -1
        elif lon_h != 'E':
            return HttpResponse(status=httplib.BAD_REQUEST)
        speed = int(round(float(s) * 1.852))   # speed is received in 'knots'
        #print ts, lat, lon, speed
    except Exception as e:
        import traceback
        traceback.print_exc()
        return HttpResponse(content=str(e), status=httplib.BAD_REQUEST)

    add_log(asset, device, lat, lon, ts, speed)

    return HttpResponse(content='OK', status=httplib.OK)
