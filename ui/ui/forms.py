from django import forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.signals import user_logged_out
from django.utils.importlib import import_module
from django.utils.translation import ugettext as _


class SingleSessionAuthForm(AuthenticationForm):
    remember_me = forms.BooleanField(
        label='Remember me', initial=True, required=False)
    close_other_sessions = forms.BooleanField(
        label='Log me out from other browser and login here', initial=False,
        required=False)

    error_messages = {
        'invalid_login': _("Please enter a correct username and password. "
                           "Note that both fields are case-sensitive."),
        'no_cookies': _("Your Web browser doesn't appear to have cookies "
                        "enabled. Cookies are required for logging in."),
        'inactive': _("This account is inactive."),
        'multiple_login': _("You have not logged out from other browser, "
                            "multiple logins are not allowed.")
    }

    def clean(self):
        cd = self.cleaned_data
        username = cd.get('username')
        password = cd.get('password')
        remember_me = cd.get('remember_me', True)

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'])
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])

            if username != 'demo':
                self._check_other_session()
            if not remember_me:
                request_session = self.request.session
                request_session.set_expiry(0)
                request_session['forget_me'] = True
        self.check_for_test_cookie()
        return self.cleaned_data

    def _check_other_session(self):
        close_other_sessions = \
            self.cleaned_data.get('close_other_sessions', False)
        profile_session_key = self.user_cache.profile.current_session
        request_session = self.request.session
        if profile_session_key is not None and \
                profile_session_key != request_session.session_key:
            profile_session = self._get_session(profile_session_key)
            forget_me = profile_session.get('forget_me', False)
            if close_other_sessions or forget_me:
                self._flush_session(profile_session)
            else:
                raise forms.ValidationError(
                    self.error_messages['multiple_login'])

    def _get_session(self, session_key):
        engine = import_module(settings.SESSION_ENGINE)
        return engine.SessionStore(session_key)

    def _flush_session(self, session):
        user = self.request.user
        user_logged_out.send(sender=user.__class__, request=self.request,
                             user=user)
        session.flush()
